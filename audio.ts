module GameAudio
{
	var imaIndexTable = [
		-1, -1, -1, -1, 2, 4, 6, 8,
		-1, -1, -1, -1, 2, 4, 6, 8,
	];
	var imaStepTable = [
		7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 
		19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 
		50, 55, 60, 66, 73, 80, 88, 97, 107, 118, 
		130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
		337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
		876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 
		2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
		5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899, 
		15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767 	
	];
	
	function imaDecode(data: Uint8Array, blockSize: number, channels: number): Float32Array[]
	{
		var out = [];
		var j = [0, 0];
		var predictor = [0, 0], stepIndex = [0, 0], step: number;
		for (var ch = 0; ch < channels; ch++)
		{
			out.push(new Float32Array(imaLength(data.length, blockSize, channels)));
		}
		for (var i = 0; i < data.length; i++)
		{
			if ((i % blockSize) == 0)
			{
				for (var ch = 0; ch < channels; ch++)
				{
					predictor[ch] = data[i] | (data[i+1] << 8);
					if (predictor[ch] & 0x8000)
						predictor[ch] = -(0x10000 - predictor[ch]);
					stepIndex[ch] = data[i + 2];
					i += 4;
				}
			}
			
			var ch = (i >> 2) % channels;
			
			// do it once
			var nibble = data[i];
			var code = nibble & 0xf;
			step = imaStepTable[stepIndex[ch]];
			var diff = step >> 3;
			if (code & 1)
				diff += step >> 2;
			if (code & 2)
				diff += step >> 1;
			if (code & 4)
				diff += step;
			if (code & 8)
			{
				predictor[ch] -= diff;
				if (predictor[ch] < -32768) predictor[ch] = -32768;
			}
			else
			{
				predictor[ch] += diff;
				if (predictor[ch] > 32767) predictor[ch] = 32767;
			}
			
			out[ch][j[ch]++] = predictor[ch] / 32768;
			stepIndex[ch] += imaIndexTable[code];
			if (stepIndex[ch] < 0) stepIndex[ch] = 0;
			if (stepIndex[ch] > 88) stepIndex[ch] = 88;
			
			// do it again
			var code = (nibble >> 4) & 0xf;
			step = imaStepTable[stepIndex[ch]];
			var diff = step >> 3;
			if (code & 1)
				diff += step >> 2;
			if (code & 2)
				diff += step >> 1;
			if (code & 4)
				diff += step;
			if (code & 8)
			{
				predictor[ch] -= diff;
				if (predictor[ch] < -32768) predictor[ch] = -32768;
			}
			else
			{
				predictor[ch] += diff;
				if (predictor[ch] > 32767) predictor[ch] = 32767;
			}
			
			out[ch][j[ch]++] = predictor[ch] / 32768;
			stepIndex[ch] += imaIndexTable[code];
			if (stepIndex[ch] < 0) stepIndex[ch] = 0;
			if (stepIndex[ch] > 88) stepIndex[ch] = 88;
		}
		return out;
	}
	
	function imaLength(byteLength: number, blockSize: number, channels: number)
	{
		var numblocks = Math.floor((byteLength + blockSize - 1) / blockSize);
		return numblocks * ((blockSize - channels * 4) * 2 + channels) / channels;
	}
	
	export class Audio
	{
		context: AudioContext;
		constructor()
		{
			this.context = new AudioContext();
		}
		getBufferFromIMA(data: Uint8Array, rate: number, channels: number): AudioBuffer
		{
			var blocksize = 512;
			if (channels > 1 || rate > 22050)
				blocksize = 1024;
			var buf: any = this.context.createBuffer(channels, imaLength(data.length, blocksize, channels), rate);
			var decoded: Float32Array[] = imaDecode(data, blocksize, channels);
			for (var ch = 0; ch < channels; ch++)
				buf.copyToChannel(decoded[ch], ch);
			return buf;
		}
		// left is negative, right is positive
		playBuffer(buf: AudioBuffer, pan: number, volume: number): AudioNode
		{
			var panner = this.context.createPanner();
			panner.panningModel = 'equalpower';
			panner.setPosition(pan, 0, 1-Math.abs(pan));
			var gain = this.context.createGain();
			gain.gain.value = volume;
			var node = this.context.createBufferSource();
			node.buffer = buf;
			node.connect(gain);
			gain.connect(panner);
			panner.connect(this.context.destination);
			node.start();
			
			return node;
		}
	}
}