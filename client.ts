/// <reference path="b64.d.ts" />
module WSClient
{
	function dumpbuffer(data: ArrayBuffer) : string {
		var _data : Uint8Array = new Uint8Array(data);
		var s : string = '';
		
		for (var i = 0; i < _data.length; i++)
		{
			var hex : string = _data[i].toString(16);
			if (hex.length < 2)
				hex = "0"+hex;
			s += hex.toUpperCase() + " ";
		}
		
		return s;
	}
	
	function ipv4ToByteArray(ip: string): number[] {
		var result = [];
		var octets = ip.split('.');
		for (var i = 0; i < octets.length; i++)
		{
			var octet = parseInt(octets[i], 10);
			result.push(octet);
		}
		return result.length == 4 ? result : null;
	};
	
	export class WSClient
	{
		game: Game.Client;
		debug: boolean;
		id: number;
		ackid: number;
		synid: number;
		importantLastTime: number;
		importantSeq: number;
		importantWindow: {[seq: number]: Uint8Array};
		timestamp: number;
		xorkey: number;
		ws: WebSocket;
		playerCls: number;
		playerName: string;
		wolname: string;
		gauntlet: boolean;
		lastTime: number;
		gameServer: string;
		gameServerPort: number;
		nextXferId: number;
		xfers: PlayerXfer[];
		constructor(url: string, gameServer: string, gameServerPort: number, game: Game.Client, name: string, playerCls = Game.PlayerClass.WARRIOR, gauntlet = false) {
			this.lastTime = 0;
			this.debug = false;
			this.ackid = 0;
			this.synid = 1;
			this.importantLastTime = 0;
			this.importantSeq = 0;
			this.importantWindow = {};
			this.timestamp = 0;
			this.xorkey = null;
			this.nextXferId = 0;
			this.xfers = [];
			this.playerCls = playerCls;
			this.playerName = name;
			this.wolname = 'noxjs';
			this.gauntlet = gauntlet;
			this.gameServer = gameServer;
			this.gameServerPort = gameServerPort;
			if (ipv4ToByteArray(this.gameServer) === null)
			{
				window.alert('Bad IPv4 address');
				return;
			}
			
			this.game = game;
			this.ws = new WebSocket(url, ["game-protocol"]);
			this.ws.binaryType = "arraybuffer";
			this.ws.onopen = ev => this.onopen(ev);
		}
		onopen(ev: Event) {
			this.ws.onmessage = ev => this.onconnectmessage(ev);
			
			// first tell WSProxy which server to connect to
			this.ws.send(new Uint8Array(ipv4ToByteArray(this.gameServer).concat([this.gameServerPort >> 8, this.gameServerPort & 0xff])));
			
			// and send the join game packet
			var wolname = this.wolname + '\0';
			this.ws.send(new Uint8Array([
				 0x00,0x00,0x0E,0x00,0x5A,0x00,0x42,0x00,0x6F,0x00,0x74,0x00,0x00,0x00,0x00,0x1A,0x00,0x00
	            ,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xB0,0x48,0x69,0x00,0x00
	            ,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x5A,0x01,0x83,0x01,0xB0
	            ,0x90,0x69,0x00,0x00,0x00,0x00,0x0A,0x34,0x34,0x37,0x35,0x33,0x33,0x30,0x31,0x37
	            ,0x31,0x31,0x36,0x38,0x30,0x32,0x35,0x33,0x35,0x32,0x30,0x38,0x39,0x00,0x9A
	            ,0x03,0x01,0x00,0x00,0x00,0x00,0x00,
				wolname.charCodeAt(0),
				wolname.charCodeAt(1),
				wolname.charCodeAt(2),
				wolname.charCodeAt(3),
				wolname.charCodeAt(4),
				wolname.charCodeAt(5),
				,0x00,0x00,0x50
	            ,0xB4,0x00,0x00
			]));
		}
		onconnectmessage(ev: MessageEvent) {
			var _data : ArrayBuffer = ev.data;
			var data : Uint8Array = new Uint8Array(_data);
			
			if (data.length != 3 || data[2] != 0x14)
			{
				console.log(dumpbuffer(data));
				window.alert("Connect failed!");
				return;
			}
			
			this.ws.onmessage = ev => this.onmessage(ev); 
			this.ws.send(new Uint8Array([0xFF, 0x00, 0x00]));
		}
		onmessage(ev: MessageEvent) {
			var _data : ArrayBuffer = ev.data;
			var data : Uint8Array = new Uint8Array(_data);
			
			// TODO measure jitter and report to user
			this.lastTime = Date.now();
			
			if (this.xorkey !== null)
			{
				for (var i = 0; i < data.length; i++)
					data[i] ^= this.xorkey;
			}
			
			if (data.length < 3)
			{
				console.log("Error: packet too short");
				return;
			}
			
			if (this.debug)
			{
				console.log("RX: " + dumpbuffer(_data));
			}
			
			if (data[0] & 0x80)
			{
				if (this.ackid != data[1])
					return;
				this.ackid++;
			}
			else
			{
				if (this.synid < data[1])
					this.synid = data[1];
			}
			
			// "fix" for delayed ACKs
			if (this.id)
				this.ws.send(new Uint8Array([]));			
			
			var i = 2;
			while (i < data.length)
			{
				var opc : string = data[i].toString(16).toUpperCase();
				if (opc.length < 2)
					opc = "0" + opc;
				var handler : (data: Uint8Array) => number = this["onpacket" + opc];
				if (handler)
				{
					i += 1;
					var result = handler.apply(this, [data.subarray(i)]);
					if (result < 0)
						break;
					i += result;
				}
				else
				{
					console.log("RX: " + dumpbuffer(_data));
					console.log("Error: missing handler for " + opc + " at " + i.toString(10));	
					break;
				}
			}
			
			if (!this.importantWindow[this.importantSeq] && !_.isEmpty(this.importantWindow))
			{
				// check for timeout
				if (Date.now() - this.importantLastTime > 10000)
				{
					// move seq number forward until we hit something
					while (!this.importantWindow[this.importantSeq])
						this.importantSeq++;
				}
			}
			// process important messages in-order
			for (; this.importantWindow[this.importantSeq]; this.importantSeq++)
			{
				data = this.importantWindow[this.importantSeq];
				for (var i = 0; i < data.length;)
				{
					var opc : string = data[i].toString(16).toUpperCase();
					if (opc.length < 2)
						opc = "0" + opc;
					var handler : (data: Uint8Array) => number = this["onpacket" + opc];
					if (handler)
					{
						i += 1;
						var result = handler.apply(this, [data.subarray(i)]);
						if (result < 0)
							break;
						i += result;
					}
					else
					{
						console.log("RX: " + dumpbuffer(new Uint8Array(data.subarray(3, 3 + length)).buffer));
						console.log("Error: missing handler for " + opc + " at " + i.toString(10));	
						break;
					}
				}
				delete this.importantWindow[this.importantSeq];
				this.importantLastTime = Date.now();
			}
		}
		send(data: Uint8Array, reliable = false, outofband = false, encrypt = true) {
			if (!outofband)
			{
				var _data = new Uint8Array(data.byteLength + 2);
				_data.set(data, 2);
				data = _data;
				
				if (reliable)
				{
					data[0] = 0x80 | this.id;
					data[1] = this.synid;
					this.synid++;
				}
				else
				{
					data[0] = 0x00 | this.id;
					data[1] = this.ackid;
				}				
			}
			if (this.debug)
				console.log("TX: " + dumpbuffer(data.buffer));
			if (encrypt)
			{
				for (var i = 0; i < data.length; i++)
					data[i] ^= this.xorkey;
			}

			this.ws.send(data);
		}
		onpacket04(data: Uint8Array) : number {
			var buf = [0x1F, 0x01, 0x20];
			var i: number;
			for (i = 0; i < 32 && i < this.playerName.length; i++)
			{
				buf.push(this.playerName.charCodeAt(i) & 0xFF);
				buf.push(this.playerName.charCodeAt(i) >> 8);
			}
			for (; i < 33; i++)
			{
				buf.push(0);
				buf.push(0);
			}
			
			var wolnameBuf = [];
			for (i = 0; i < 9 && i < this.wolname.length; i++)
				wolnameBuf.push(this.wolname.charCodeAt(i));
			for (; i < 10; i++)
				wolnameBuf.push(0);
			
			this.send(new Uint8Array(buf.concat(this.playerCls, 0x00, 0x73, 0x4D, 0x22, 0xDA, 0x9A, 0x6E, 0xDA, 
              0x9A, 0x6E, 0xDA, 0x9A, 0x6E, 0xDA, 0x9A, 0x6E, 0x0C, 0x07, 0x13, 0x17, 0x06, 0x00, 0x00, 0x00, 
              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 
              0x00, 0x71, 0x34, 0x4C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0xD4, 0x48, 
              0x00, 0x01, 0x00, 0x00, 0x00).concat(wolnameBuf).concat(0x00, 
              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, this.gauntlet ? 0x80 : 0x01)), true);
			this.send(new Uint8Array([0xAD]), true);
			this.onpacket04 = null;
			return 0;
		}
		onpacket1F(data: Uint8Array) : number {
			this.id = data[2];
			this.xorkey = data[6];
			
			this.send(new Uint8Array([0xFF, 0x00, 0x00]), false, true);
			this.onpacket1F = null;
			return 7;
		}
		onpacket27(data: Uint8Array) : number {
			// MSG_TIMESTAMP
			var partialts = data[0] | (data[1] << 8);
			var overflow = ((this.timestamp & 0xFFFF) >= 0xC000) && (partialts < 0x4000);
			if (!overflow && partialts < (this.timestamp & 0xFFFF))
			{
				console.log("ignoring out of order packet", this.timestamp, partialts);
				return -1;
			}
			this.timestamp = (this.timestamp & 0xFFFF0000) | partialts;
			if (overflow)
				this.timestamp += 0x10000;
			return 2;
		}
		onpacket28(data: Uint8Array) : number {  
			// MSG_FULL_TIMESTAMP
			var timestamp = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
			this.timestamp = timestamp;
			return 4;
		}
		onpacket2B(data: Uint8Array) : number {
			// MSG_USE_MAP
			var name: string = '';
			for (var i = 0; ; i++)
			{
				var ch = data[i];
				if (ch == 0)
					break;
				name += String.fromCharCode(ch);
			}
			
			this.game.onnewmap(name);
			return 40;
		}
		onpacket2C(data: Uint8Array) : number {
			// MSG_JOIN_DATA
			var extent = data[0] | (data[1] << 8);
			this.game.onjoindata(extent);
			return 6;
		}
		onpacket2D(data: Uint8Array) : number {
			// MSG_NEW_PLAYER
			var extent = data[0] | (data[1] << 8);
			var name: string = '';
			for (var i = 2; ; i += 2)
			{
				var ch = data[i + 0] | (data[i + 1] << 8);
				if (ch == 0)
					break;
				name += String.fromCharCode(ch);
			}
			var wolname: string = '';
			for (var i = 118; ; i++)
			{
				var ch = data[i];
				if (ch == 0)
					break;
				wolname += String.fromCharCode(ch);
			}
			var cls = data[68];
			var color1 = (data[70] << 16) | (data[71] << 8) | data[72];
			var color2 = (data[73] << 16) | (data[74] << 8) | data[75];
			var color3 = (data[76] << 16) | (data[77] << 8) | data[78];
			var color4 = (data[79] << 16) | (data[80] << 8) | data[81];
			var color5 = (data[82] << 16) | (data[83] << 8) | data[84];
			var respawnArmor = data[103] | (data[104] << 8) | (data[105] << 16) | (data[106] << 24);
			var respawnWeapon = data[107] | (data[108] << 8) | (data[109] << 16) | (data[110] << 24);
			this.game.onnewplayer(extent, cls, name, wolname, respawnArmor, respawnWeapon);
			console.log(dumpbuffer(data));
			return 128;
		}
		onpacket2E(data: Uint8Array) : number {
			// MSG_PLAYER_QUIT
			var id = data[0] | (data[1] << 8);
			this.game.onplayerquit(id);
			return 2;
		}
		onpacket2F(data: Uint8Array) : number {
			// MSG_SIMPLE_OBJ
			return 8;
		}
		onpacket31(data: Uint8Array) : number {
			// MSG_DESTROY_OBJECT
			var id = data[0] | (data[1] << 8);
			this.game.onobjectdestroy(id);
			return 2;
		}
		onpacket32(data: Uint8Array) : number {
			// MSG_OBJECT_OUT_OF_SIGHT
			var id = data[0] | (data[1] << 8);
			this.game.onobjectdestroy(id);
			return 2;
		}
		onpacket33(data: Uint8Array) : number {
			// MSG_OBJECT_IN_SHADOWS
			var id = data[0] | (data[1] << 8);
			this.game.onobjectdestroy(id);
			return 2;
		}
		onpacket34(data: Uint8Array) : number {
			return 2;
		}
		onpacket35(data: Uint8Array) : number {
			return 2;
		}
		onpacket36(data: Uint8Array) : number {
			return 2;
		}
		onpacket37(data: Uint8Array) : number {
			// MSG_ENABLE_OBJECT
			return 2;
		}
		onpacket38(data: Uint8Array) : number {
			// MSG_DISABLE_OBJECT
			return 2;
		}
		onpacket39(data: Uint8Array) : number {
			// MSG_DRAW_FRAME
			var id = data[0] | (data[1] << 8);
			this.game.onreportframe(id, data[2]);
			return 3;
		}
		onpacket3A(data: Uint8Array) : number {
			// MSG_DESTROY_WALL
			var id = data[0] | (data[1] << 8);
			this.game.onwalldestroy(id);
			return 2;
		}
		onpacket3B(data: Uint8Array) : number {
			// MSG_OPEN_WALL
			var id = data[0] | (data[1] << 8);
			this.game.onwallopen(id);
			return 2;
		}
		onpacket3C(data: Uint8Array) : number {
			// MSG_CLOSE_WALL
			var id = data[0] | (data[1] << 8);
			this.game.onwallclose(id);
			return 2;
		}
		onpacket3D(data: Uint8Array) : number {
			this.game.onaddwall(data[0], data[1], data[2], data[3], data[4]);
			return 5;
		}
		onpacket3E(data: Uint8Array) : number {
			this.game.onremovewall(data[0], data[1]);
			return 2;
		}
		onpacket42(data: Uint8Array) : number {
			// MSG_REPORT_PLAYER_HEALTH_DELTA
			var extent = data[0] | (data[1] << 8);
			var delta = data[2] | (data[3] << 8);
			if (delta & 0x8000) // negative
				delta = -((delta ^ 0xFFFF) + 1);
			this.game.onhealthdelta(extent, delta);
			return 4;
		}
		onpacket43(data: Uint8Array) : number {
			var current = data[0] | (data[1] << 8);			
			this.game.onhealth(current);
			return 2;
		}
		onpacket44(data: Uint8Array) : number {
			return 6;
		}
		onpacket45(data: Uint8Array) : number {
			return 4;
		}
		onpacket47(data: Uint8Array) : number {
			// MSG_REPORT_STAMINA
			this.game.onstamina(data[0]);
			return 1;
		}
		onpacket48(data: Uint8Array) : number {
			// MSG_REPORT_STATS
			return 13;
		}
		onpacket49(data: Uint8Array) : number {
			// MSG_REPORT_ARMOR_VALUE
			return 4;
		}
		onpacket4A(data: Uint8Array) : number {
			// MSG_REPORT_GOLD
			// uint32
			return 4;
		}
		onpacket4B(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			this.game.onpickup(id, type);
			return 4;
		}
		onpacket4C(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			this.game.onpickup(id, type);
			this.game.onreportmodifier(id, data[4], data[5], data[6], data[7]);
			return 8;
		}
		onpacket4D(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			this.game.ondrop(id, type);
			return 4;
		}
		onpacket4E(data: Uint8Array) : number {
			return 10;
		}
		onpacket4F(data: Uint8Array) : number {
			// MSG_REPORT_MUNDANE_ARMOR_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onarmor(id, bitmask, true);
			return 6;
		}
		onpacket50(data: Uint8Array) : number {
			// MSG_REPORT_MUNDANE_WEAPON_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onweapon(id, bitmask, true);
			return 6;
		}
		onpacket51(data: Uint8Array) : number {
			// MSG_REPORT_MODIFIABLE_WEAPON_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onweapon(id, bitmask, true, {material: data[6], effectiveness: data[7], primary: data[8], secondary: data[9]});
			return 10;
		}
		onpacket52(data: Uint8Array) : number {
			// MSG_REPORT_MODIFIABLE_ARMOR_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onarmor(id, bitmask, true, {material: data[6], effectiveness: data[7], primary: data[8], secondary: data[9]});
			return 10;
		}
		onpacket53(data: Uint8Array) : number {
			// MSG_REPORT_ARMOR_DEQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onarmor(id, bitmask, false);
			return 6;
		}
		onpacket54(data: Uint8Array) : number {
			// MSG_REPORT_WEAPON_DEQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onweapon(id, bitmask, false);
			return 6;
		}
		onpacket56(data: Uint8Array) : number {
			// MSG_REPORT_FLAGBALL_WINNER
			var id = data[0] | (data[1] << 8);
			// data[2] ???
			var team = data[3] | (data[4] << 8) | (data[5] << 16) | (data[6] << 24);
			return 7;
		}
		onpacket57(data: Uint8Array) : number {
			// MSG_REPORT_FLAG_WINNER
			var id = data[0] | (data[1] << 8);
			// data[2] 1 = time limit reached
			var team = data[3] | (data[4] << 8) | (data[5] << 16) | (data[6] << 24);
			return 7;
		}
		onpacket58(data: Uint8Array) : number {
			// MSG_REPORT_DEATHMATCH_WINNER
			var id = data[0] | (data[1] << 8);
			// data[2] 1 = time limit reached
			var team = data[3] | (data[4] << 8) | (data[5] << 16) | (data[6] << 24);
			return 7;
		}
		onpacket59(data: Uint8Array) : number {
			// MSG_REPORT_DEATHMATCH_TEAM_WINNER
			var id = data[0] | (data[1] << 8);
			// data[2] 1 = time limit reached
			var team = data[3] | (data[4] << 8) | (data[5] << 16) | (data[6] << 24);
			return 7;
		}
		onpacket5A(data: Uint8Array) : number {
			// MSG_REPORT_ENCHANTMENT
			var id = data[0] | (data[1] << 8);
			var enchant = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onenchantment(id, enchant);
			return 6;
		}
		onpacket5B(data: Uint8Array) : number {
			// MSG_REPORT_ITEM_ENCHANTMENT
			this.game.onitemenchantment(data[0]);
			return 1;
		}
		onpacket5E(data: Uint8Array) : number {
			// MSG_REPORT_Z_PLUS
			var id = data[0] | (data[1] << 8);
			this.game.onreportz(id, data[2]);
			return 3;
		}
		onpacket5F(data: Uint8Array) : number {
			// MSG_REPORT_Z_MINUS
			var id = data[0] | (data[1] << 8);
			this.game.onreportz(id, -data[2]);
			return 3;
		}
		onpacket60(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			this.game.onequip(id);
			return 2;
		}
		onpacket61(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			this.game.ondequip(id);
			return 2;
		}
		onpacket64(data: Uint8Array) : number {
			// MSG_REPORT_CHARGES
			var extent: number = data[0] | (data[1] << 8);
			this.game.oncharges(extent, data[2], data[3]);
			return 4;
		}
		onpacket65(data: Uint8Array) : number {
			// MSG_REPORT_X_STATUS
			var extent: number = data[0] | (data[1] << 8);
			var x = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onreportxstatus(extent, x);
			return 6;
		}
		onpacket67(data: Uint8Array) : number {
			// MSG_REPORT_MODIFIER
			var extent: number = data[0] | (data[1] << 8);
			this.game.onreportmodifier(extent, data[2], data[3], data[4], data[5]);
			return 6;
		}
		onpacket68(data: Uint8Array) : number {
			// MSG_REPORT_STAT_MODIFIER
			return 7;
		}
		onpacket6A(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			var status = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onclientstatus(extent, status);
			return 6;
		}
		onpacket6B(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			var frame = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onreportframe(extent, frame);
			return 6;
		}
		onpacket7E(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			var id: number = data[4] | (data[5] << 8);
			var type: number = data[6] | (data[7] << 8);
			var orientation: number = data[8];
			var frames: number = data[9] | (data[10] << 8);
			this.game.onfxsummon(id, x, y, type, orientation, frames);
			return 11;
		}
		onpacket7F(data: Uint8Array) : number {
			var id: number = data[0] | (data[1] << 8);
			this.game.onfxsummoncancel(id);
			return 2;
		}
		onpacket80(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.onfxshield(extent, data[2]);
			return 3;
		}
		onpacket81(data: Uint8Array) : number {
			// MSG_FX_BLUE_SPARKS
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxbluesparks(x, y);
			return 4;
		}
		onpacket82(data: Uint8Array) : number {
			// MSG_FX_YELLOW_SPARKS
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxyellowsparks(x, y);
			return 4;
		}
		onpacket83(data: Uint8Array) : number {
			// MSG_FX_CYAN_SPARKS
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxcyansparks(x, y);
			return 4;
		}
		onpacket84(data: Uint8Array) : number {
			// MSG_FX_VIOLET_SPARKS
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxvioletsparks(x, y);
			return 4;
		}
		onpacket85(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxexplosion(x, y);
			return 4;
		}
		onpacket86(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxlesserexplosion(x, y);
			return 4;
		}
		onpacket87(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxcounterspell(x, y);
			return 4;
		}
		onpacket88(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxthinexplosion(x, y);
			return 4;
		}
		onpacket89(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxteleport(x, y);
			return 4;
		}
		onpacket8A(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxsmokeblast(x, y);
			return 4;
		}
		onpacket8B(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxdamagepoof(x, y);
			return 4;
		}
		onpacket8C(data: Uint8Array) : number {
			return 8;
		}
		onpacket8D(data: Uint8Array) : number {
			return 8;
		}
		onpacket8E(data: Uint8Array) : number {
			return 8;
		}
		onpacket8F(data: Uint8Array) : number {
			return 8;
		}
		onpacket90(data: Uint8Array) : number {
			return 8;
		}
		onpacket91(data: Uint8Array) : number {
			return 8;
		}
		onpacket93(data: Uint8Array) : number {
			// MSG_FX_SPARK_EXPLOSION
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxsparkexplosion(x, y, data[4] / 255);
			return 5;
		}
		onpacket94(data: Uint8Array) : number {
			var x1: number = data[0] | (data[1] << 8);
			var y1: number = data[2] | (data[3] << 8);
			var x2: number = data[4] | (data[5] << 8);
			var y2: number = data[6] | (data[7] << 8);
			this.game.onfxdeathray(x1, y1, x2, y2);
			return 8;
		}
		onpacket95(data: Uint8Array) : number {
			// MSG_FX_SENTRY_RAY
            var x1 = data[0] | (data[1] << 8);
            var y1 = data[2] | (data[3] << 8);
            var x2 = data[4] | (data[5] << 8);
            var y2 = data[6] | (data[7] << 8);
			this.game.onfxsentryray(x1, y1, x2, y2);
			return 8;
		}
		onpacket96(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxricochet(x, y);
			return 4;
		}
		onpacket97(data: Uint8Array) : number {
			// MSG_FX_JIGGLE
			this.game.onfxjiggle(data[0] / 255);
			return 1;
		}
		onpacket9A(data: Uint8Array) : number {
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			this.game.onfxwhiteflash(x, y);
			return 4;
		}
		onpacket9E(data: Uint8Array) : number {
			// duration spell
			var type: number = data[0];
			var extent1: number = data[2] | (data[3] << 8);
			var extent2: number = data[4] | (data[5] << 8);
			this.game.onfxduration(type, extent1, extent2);
			return 6;
		}
		onpacket9F(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.onfxdeltaz(extent, data[2], data[3], data[4]);
			return 5;
		}
		onpacketA2(data: Uint8Array) : number {
			// MSG_FX_VAMPIRISM
			return 10;
		}
		onpacketA3(data: Uint8Array) : number {
			// MSG_FX_MANA_BOMB_CANCEL
			var x: number = data[0] | (data[1] << 8);
			var y: number = data[2] | (data[3] << 8);
			return 4;
		}
		onpacketA4(data: Uint8Array) : number {
			var i : number = 0;
			var prevloc : number[] = null;
			
			while (true)
			{
				if (data[i] == 0x00)
				{
					if (prevloc == null)
						break;
					prevloc = null;
					i++;
					continue;
				}
				if (data[i] != 0xFF)
				{
					console.log("Unknown byte in A4: " + dumpbuffer(data.buffer.slice(i)));
					break;
				}
				i++;
				
				var extent = data[i] | (data[i+1] << 8);
				i += 2;
				var type = data[i] | (data[i+1] << 8);
				i += 2;
				
				var loc : number[] = Array<number>(2);
				if (prevloc === null)
				{
					loc[0] = data[i] | (data[i+1] << 8);
					i += 2;
					loc[1] = data[i] | (data[i+1] << 8);
					i += 2;
				}
				else
				{
					loc[0] = prevloc[0];
					if (data[i] & 0x80)
						loc[0] -= (data[i] ^ 0xFF) + 1;
					else
						loc[0] += data[i];
					i++;
					loc[1] = prevloc[1];
					if (data[i] & 0x80)
						loc[1] -= (data[i] ^ 0xFF) + 1;
					else
						loc[1] += data[i];
					i++;
				}
				prevloc = loc;
				
				this.game.onupdate(extent, type, loc[0], loc[1], data.subarray(i));
				
				// FIXME this needs to be expanded a bit
				if (type == 0x000 || type == 0x2C9)
				{
					// NewPlayer
					var t = data[i];
					i++;
					if (t & 0x80)
						i++; // frame used for Slave animation
					i++;
				}
				else if (Game.theThings.objects[type].update == 'MonsterUpdate')
				{
					if (data[i] & 0x80)
					i++;
					i++;
				}
				else if (type == 0x20E)
				{
					// HarpoonBolt
					i++;
					i++;
				}
				// FIXME
				else if (Game.theThings.objects[type].update == 'ProjectileUpdate' &&
					Game.theThings.objects[type].name != 'SpiderSpit' &&
					Game.theThings.objects[type].name != 'ThrowingStone' &&
					Game.theThings.objects[type].name != 'ImpShot')
				{
					if (data[i] & 0x80)
						i++;
					i++;
				}
				else if (type == 0x49B)
				{
					// FanChakramInMotion
					i++;
				}
				else if (type == 0x499)
				{
					// FanChakramInMotion
					i++;
				}
			}

			return i + 2;
		}
		onpacketA6(data: Uint8Array) : number {
			// MSG_AUDIO_EVENT
			var id = (data[1] | (data[2] << 8)) & 0x3ff;
			this.game.onaudio(data[0], id, data[2] >> 2);
			return 3;
		}
		onpacketA7(data: Uint8Array) : number {
			// MSG_AUDIO_PLAYER_EVENT
			var id = (data[1] | (data[2] << 8)) & 0x3ff;
			this.game.onaudioplayer(data[0], id, data[2] >> 2);
			return 3;
		}
		onpacketA8(data: Uint8Array) : number {
			// MSG_TEXT_MESSAGE
			var extent: number = data[0] | (data[1] << 8);
			var flags: number = data[2];
			var x: number = data[3] | (data[4] << 8);
			var y: number = data[5] | (data[6] << 8);
			var length: number = data[7] | (data[8] << 8);
			var msg: string = '';
			if (flags & 4) // unicode
			{
				for (var i = 10; i < 10 + length * 2; i += 2)
				{
					var ch = data[i + 0] | (data[i + 1] << 8);
					if (ch == 0)
						break;
					msg += String.fromCharCode(ch);
				}
				this.game.onchat(extent, msg, x, y);
				return 10 + length * 2;
			}
			else if (flags & 2) // ascii
			{
				for (var i = 10; i < 10 + length; i++)
				{
					var ch = data[i];
					if (ch == 0)
						break;
					msg += String.fromCharCode(ch);
				}
				this.game.onchat(extent, msg, x, y);
				return 10 + length;
			}
		}
		onpacketA9(data: Uint8Array) : number {
			// MSG_INFORM
			switch (data[0])
			{
			case 0:
				var strid = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var str = new Array('plyrspel.c:SpellOK', 'plyrspel.c:SpellInUse', 'execspel.c:UnseenTarget', 'plyrspel.c:TooManySpells', 'summon.c:CreatureControlFailed',
					'glyph.c:DuplicateGlyphSpell', 'Spell.c:DuplicateGlyphSpell', 'glyph.c:CantCastGlyph', 'plyrspel.c:BadTarget', 'plyrspel.c:BadSkill', 'plyrspel.c:Illegal', 
					'plyrspel.c:NotEnoughManaCast', 'spell.c:NotEnoughManaGlyph', 'plyrspel.c:SpellRestrictedByFlag', 'plyrspel.c:SpellNotStartedWarCry', 'spell.c:SpellCancelledByWarCry', 
					'plyrspel.c:SpellRestrictedByBall', 'plyrspel.c:SpellRestrictedByCrown')[strid];
				this.game.oninformmsg(str);
				return 5;
			case 1:
				var spell = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				this.game.oninformmsg('plyrspel.c:SpellCastSuccess', spell);
				return 5;
			case 2:
				var strid = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var str = new Array('ability.c:AbilityOK', 'ability.c:AbilityInUse', 'ability.c:AbilityNotReady', 'ability.c:BadSkill', 'ability.c:Illegal', 'ability.c:AbilityRestrictedByFlag', 'ability.c:AbilityRestrictedWhileJumping', 'player.c:TooHeavy')[strid];
				this.game.oninformmsg(str);
				return 5;
			case 3:
				var extent = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				this.game.oninformmsg('netserv.c:PlayerTimeout', Game.theWorld.players[extent].name);
				return 5;
			case 4:
				var extent = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				this.game.oninformmsg('objcoll.c:FlagRetrieveNotice', Game.theWorld.players[extent].name);
				Game.playaudiobyname('FlagRespawn', 0, 1.0);
				return 5;
			case 5:
				var extent = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var team = data[5] | (data[6] << 8) | (data[7] << 16) | (data[8] << 24);
				this.game.oninformmsg('objcoll.c:FlagCaptureNotice', Game.theWorld.players[extent].name, Game.TeamData[team].name);
				Game.playaudiobyname('FlagCapture', 0, 1.0);
				return 9;
			case 6:
				var extent = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var team = data[5] | (data[6] << 8) | (data[7] << 16) | (data[8] << 24);
				this.game.oninformmsg('objcoll.c:FlagPickupNotice', Game.theWorld.players[extent].name, Game.TeamData[team].name);
				Game.playaudiobyname('FlagPickup', 0, 1.0);
				return 9;
			case 7:
				var extent = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var team = data[5] | (data[6] << 8) | (data[7] << 16) | (data[8] << 24);
				this.game.oninformmsg('drop.c:FlagDropNotice', Game.theWorld.players[extent].name, Game.TeamData[team].name);
				Game.playaudiobyname('FlagDrop', 0, 1.0);
				return 9;
			case 8:
				var team = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				this.game.oninformmsg('update.c:FlagRespawnNotice', Game.TeamData[team].name);
				Game.playaudiobyname('FlagRespawn', 0, 1.0);
				return 5;
			case 9:
				var extent = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var team = data[5] | (data[6] << 8) | (data[7] << 16) | (data[8] << 24);
				this.game.oninformmsg('objcoll.c:FlagBallNotice', Game.theWorld.players[extent].name, Game.TeamData[team].name);
				return 9;
			case 10:
				return 9;
			case 11:
				return 9;
			case 12:
				var status = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				this.game.oninformmsg('Netserv.c:InObservationMode');
				if (status != 0)
				{
					this.game.oninformmsg('Netserv.c:PressJump');
				}
				return 5;
			case 13:
				var status = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var str = new Array('atckexec.c:PlayerStunned', 'atckexec.c:PlayerConfused', 'atckexec.c:PlayerPoisoned', 'player.c:TooHeavy')[status];
				this.game.oninformmsg(str);
				return 5;
			case 14:
				var attacker1: number = data[1] | (data[2] << 8);
				var attacker2: number = data[3] | (data[4] << 8);
				var victim: number = data[5] | (data[6] << 8);
				this.game.oninformkill(attacker1, attacker2, victim);
				return 10;
			case 15:
				var str: string = '';
				for (var i = 2; ; i++)
				{
					var ch = data[i];
					if (ch == 0)
						break;
					str += String.fromCharCode(ch);
				}
				this.game.oninformmsg(str);
				return i + 1;
			case 16:
				var team = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				this.game.oninformmsg('pickup.c:WrongTeam', Game.TeamData[team].name);
				return 5;
			case 21:
				return 5;
			default:
				return 1; 
			}
		}
		onpacketAA(data: Uint8Array) : number {
			// MSG_IMPORTANT
			this.sendackimportant();
			return 0;
		}
		onpacketAB(data: Uint8Array) : number {
			// MSG_IMPORTANT_ACK
			// XXX we currently use this to measure RTT
			var x = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
			this.game.onimportantack(x);
			return 4;
		}
		onpacketAE(data: Uint8Array) : number {
			// MSG_OUTGOING_CLIENT
			return 2;
		}
		onpacketAF(data: Uint8Array) : number {
			// MSG_GAME_SETTINGS
			return 19;
		}
		onpacketB0(data: Uint8Array) : number {
			// MSG_GAME_SETTINGS_2
			return 48;
		}
		onpacketB1(data: Uint8Array) : number {
			// MSG_UPDATE_GUI_GAME_SETTINGS
			return 59;
		}
		onpacketB2(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.ondoorangle(extent, data[2]);
			return 3;
		}
		onpacketB3(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.onobeliskcharge(extent, data[2]);
			return 3;
		}
		onpacketB4(data: Uint8Array) : number {
			// MSG_PENTAGRAM_ACTIVATE
			return 3;
		}
		onpacketB5(data: Uint8Array) : number {
			// MSG_CLIENT_PREDICT_LINEAR
			return 13;
		}
		onpacketC2(data: Uint8Array) : number {
			// MSG_XFER
			if (data[0] == 0)
			{
				var xfer = this.newxfer((name: string, data: Uint8Array) => {
					if (name == 'SAVEDATA')
						localStorage.setItem('savedata', base64js.fromByteArray(data));
				});
				return xfer.onpacket00(this, data.subarray(1)) + 1;
			}
			else
			{
				var token = data[0].toString(16).toUpperCase();
				if (token.length < 2)
					token = "0" + token;
				for (var i = 0; i < this.xfers.length; i++)
				{
					var xfer = this.xfers[i];
					var bytes: number = xfer["onpacket"+token].apply(xfer, [this, data.subarray(1)]);
					if (bytes != -1)
						return bytes + 1;
				}
				console.log("Unhandled XFER pkt", dumpbuffer(data));
			}
		}
		onpacketC3(data: Uint8Array) : number {
			// MSG_PLAYER_OBJ
			return 11;
		}
		onpacketC4(data: Uint8Array) : number {
			var plr, team;
			switch (data[0])
			{
			case 0:
				team = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				// skip 9 bytes
				var length = data[14];
				// skip 2 bytes (0x01 0x01)
				var teamName = '';
				return 17 + length * 2;
			case 1: // join team
				team = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
				var extent = data[5] | (data[6] << 8);
				var type = data[7] | (data[8] << 8);
				this.game.onteamjoin(team, extent, type);
				return 9;
			case 2:
				return 5;
			case 3:
				return 9;
			case 5:
				return 5;
			case 8:
				return 9;
			case 9:
				return 1;
			case 12:
				// uint16 player extent
				// byte health percentage (0x64 == 100%)
				return 4;
			default:
				console.log("Unknown TEAM_MSG packet", data[0].toString(10), dumpbuffer(data));
				return 1;
			}
		}
		onpacketC7(data: Uint8Array) : number {
			// MSG_SERVER_QUIT
			this.send(new Uint8Array([0xC8]));
			this.ws.onmessage = ev => { return; };
			this.ws.close();
			return 0;
		}
		onpacketCC(data: Uint8Array) : number {
			// MSG_SEQ_IMPORTANT
			var seq = data[0] | (data[1] << 8);
			if (seq < this.importantSeq)
			{
				// ignore since we already have this message
				console.log("Dropped important", seq);
				return 3 + length;
			}
				
			var length = data[2];
			this.importantWindow[seq] = new Uint8Array(data.buffer, data.byteOffset + 3, length);
			return 3 + length;
		}
		onpacketCD(data: Uint8Array) : number {
			// MSG_REPORT_ABILITY_REWARD
			return 2;
		}
		onpacketCE(data: Uint8Array) : number {
			// MSG_REPORT_ABILITY_STATE
			var ability: number = data[0];
			var available: boolean = data[1] != 0;
			this.game.onabilitystate(ability, available);
			return 2;
		}
		onpacketCF(data: Uint8Array) : number {
			// MSG_REPORT_ACTIVE_ABILITIES
			var ability: number = data[0];
			var active: boolean = data[1] != 0;
			return 2;
		}
		onpacketD2(data: Uint8Array) : number {
			// MSG_INTERESTING_ID
			var extent = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			return 6;
		}
		onpacketD3(data: Uint8Array) : number {
			// MSG_TIMER_STATUS
			return 12;
		}
		onpacketD7(data: Uint8Array) : number {
			// MSG_REPORT_ALL_LATENCY
			return 4;
		}
		onpacketD8(data: Uint8Array) : number {
			// MSG_REPORT_FLAG_STATUS
			// data[0] is status
			// data[1] is team
			// data[2] is 0, 1, 2
			// data[4] is flag holder extent
			var extent = data[3] | (data[4] << 8);
			this.game.onflagstatus(data[0], data[1], data[2], extent);
			return 5;
		}
		onpacketD9(data: Uint8Array) : number {
			// MSG_REPORT_BALL_STATUS
			// data[0] is 0 (home), 1 (no team), 2 (team 1), 4 (team 2)
			// data[1] is ball holder extent
			var extent = data[1] | (data[2] << 8);
			this.game.onballstatus(data[0], extent);
			return 3;
		}
		onpacketDD(data: Uint8Array) : number {
			// MSG_REPORT_TOTAL_HEALTH
			var extent = data[0] | (data[1] << 8);
			var current = data[2] | (data[3] << 8);
			var total = data[4] | (data[5] << 8);
			this.game.onhealth(current, total);
			return 6;
		}
		onpacketDE(data: Uint8Array) : number {
			// MSG_REPORT_TOTAL_MANA
			var extent = data[0] | (data[1] << 8);
			var current = data[1] | (data[2] << 8);
			var total = data[3] | (data[4] << 8);
			this.game.onhealth(current, total);
			return 6;
		}
		onpacketDF(data: Uint8Array) : number {
			// MSG_REPORT_SPELL_STAT
			return 5;
		}
		onpacketE4(data: Uint8Array) : number {
			// MSG_FADE_BEGIN
			return 2;
		}
		onpacketE8(data: Uint8Array) : number {
			// MSG_PLAYER_DIED
			var extent = data[0] | (data[1] << 8);
			this.game.onplayerdied(extent);
			return 2;
		}
		onpacketE9(data: Uint8Array) : number {
			// MSG_PLAYER_RESPAWN
			return 8;
		}
		onpacketEA(data: Uint8Array) : number {
			// MSG_FORGET_DRAWABLES
			var timestamp = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
			this.game.onforgetdrawables(timestamp);
			return 4;
		}
		onpacketEB(data: Uint8Array) : number {
			// MSG_RESET_ABILITIES
			this.game.onabilityreset(data[0]);
			return 1;
		}
		onpacketEC(data: Uint8Array) : number {
			// MSG_RATE_CHANGE
			return 1;
		}
		onpacketEF(data: Uint8Array) : number {
			// MSG_STAT_MULTIPLIERS
			return 16;
		}
		onpacketF0(data: Uint8Array) : number {
			// MSG_GAUNTLET
			switch (data[0]) // 0-33
			{
			case 0:
				var savedataEncoded: string = localStorage.getItem('savedata');
				var savedata: Uint8Array;
				if (savedataEncoded === null)
					savedata = new Uint8Array([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
				else
					savedata = base64js.toByteArray(savedataEncoded);
				var xfer = this.newxfer((name: string, data: Uint8Array) => { return; }, savedata);
				xfer.start(this, "SAVE_SERVER");
				return 1;
			case 1:
				// player joined the Quest
				// uint16 player extent
				return 3;
			case 4:
				// byte
				// uint16 (player extent)
				return 4;
			case 14:
				// not important, just intro screen
				// uint16
				// byte
				// NUL-terminated string (length 0x20)
				// NUL-terminated string (length 0x20)
				return 0x44;
			case 15:
				// unlock door
				// uint16 (door extent)
                var extent = data[1] | (data[2] << 8);
				this.game.onunlockdoor(extent);
				return 3;
			case 16:
				// monster generated fx
				// uint16 x1
				// uint16 y1
				// uint16 x2
				// uint16 y2
				// uint16 ??
				return 11;
			case 22:
				// number of silver keys player has
				// byte
				// uint16 player extent
				return 4;
			case 23:
				// number of gold keys player has
				// byte
				// uint16 player extent
				return 4;
			case 24:
				// key sharing mode
				// byte (1 = on)
				return 2;
			case 25:
				// generator destroyed fs
				// uint16 x
				// uint16 y
				// byte ?? 
				var x = data[1] | (data[2] << 8);
				var y = data[3] | (data[4] << 8);
				this.game.onfxgeneratordestroyed(x, y, data[5]);
				return 6;
			case 26:
				// generator damaged fs
				// uint16 x
				// uint16 y
				var x = data[1] | (data[2] << 8);
				var y = data[3] | (data[4] << 8);
				this.game.onfxgeneratordamaged(x, y);
				return 5;
			case 33:
				// key message
				// char fmt[49] (null-terminated)
				// byte key
				var fmtstring = '';
				for (var i = 1; ; i++)
				{
					var ch = data[i];
					if (ch == 0)
						break;
					fmtstring += String.fromCharCode(ch);
				}
				var keystring = new Array("objcoll.c:NullKey", "objcoll.c:SilverKey", "objcoll.c:GoldKey", "objcoll.c:RubyKey", "objcoll.c:SaphhireKey")[data[50]];
				this.game.oninformmsg(fmtstring, Game.getstring(keystring));
				return 51;
			default:
				console.log("Unknown MSG_GAUNTLET type", data[0], dumpbuffer(data));
				return -1;
			}
		}
		sendackimportant() : void {
			this.send(new Uint8Array([0xAB,
				this.timestamp & 0xff, (this.timestamp >> 8) & 0xff,
				(this.timestamp >> 16) & 0xff, (this.timestamp >> 24) & 0xff]));
		}
		newxfer(cb: (name: string, data: Uint8Array) => void, data?: Uint8Array) : PlayerXfer {
			var id = this.nextXferId++;
			var xfer = new PlayerXfer(id, cb, data);
			this.xfers.push(xfer);
			return xfer;
		}
	}
	
	class PlayerXfer
	{
		cb: (string, Uint8Array) => void;
		id: number;
		pktid: number;
		remoteid: number;
		transmit: boolean;
		data: Uint8Array;
		idx: number;
		pending: number;
		name: string;
		constructor(id: number, cb: (string, Uint8Array) => void, data?: Uint8Array)
		{
			this.cb = cb;
			this.id = id;
			this.pktid = 1;
			this.data = data;
			this.transmit = this.data !== undefined;
			this.idx = 0;
		}
		start(client: WSClient, name: string)
		{
			if (!this.transmit)
				throw 'Invalid';
			
			this.name = name;
			var data = new Uint8Array(0x8C);
			data[0] = 0xC2;
			data[1] = 0x00;
			data[2] = 0x03;
			data[3] = 0x00;
			data[4] = this.data.length & 0xff;
			data[5] = (this.data.length >> 8) & 0xff;
			data[6] = (this.data.length >> 16) & 0xff;
			data[7] = (this.data.length >> 24) & 0xff;
			for (var i = 0; i < 0x80; i++)
			{
				if (i < name.length)
					data[8 + i] = name.charCodeAt(i);
				else
					data[8 + i] = 0;
			}
			data[0x88] = this.id;
			data[0x89] = 0x00;
			data[0x8A] = 0x00;
			data[0x8B] = 0x00;
			
			client.send(data);
		}
		onpacket00(client: WSClient, data: Uint8Array): number
		{
			if (this.transmit)
				throw 'Invalid';
			var length = 0;
			length |= data[2];
			length |= data[3] << 8;
			length |= data[4] << 16;
			length |= data[5] << 24;
			this.data = new Uint8Array(length);
			
			var name = '';
			for (var i = 0; i < 0x80; i++)
			{
				if (data[6 + i] == 0) break;
				name += String.fromCharCode(data[6 + i]);
			}
			this.name = name;
			
			var remoteid = data[0x86];
			this.remoteid = remoteid;
			
			client.send(new Uint8Array([0xC2, 0x01, this.id, remoteid]));
			return 0x8A;
		}
		onpacket01(client: WSClient, data: Uint8Array): number
		{
			if (!this.transmit)
				throw 'Invalid';
			if (data[1] != this.id)
				return -1;
			this.remoteid = data[0];
			this.sendpacket(client);
			return 2;
		}
		onpacket02(client: WSClient, data: Uint8Array): number
		{
			if (this.transmit)
				throw 'Invalid';
			if (data[0] != this.id)
				return -1;
			var pktid = data[2] | (data[3] << 8);
			if (pktid != this.pktid)
				throw 'Dropped a packet';
			var length = data[4] | (data[5] << 8);
			this.data.set(data.subarray(6, 6 + length), this.idx);
			this.idx += length;
			this.pktid += 1;
			client.send(new Uint8Array[0xC2, 0x03, this.remoteid, 0x00, data[2], data[3]]);
			if (this.idx == this.data.length)
			{
				this.senddone(client);
			}
			return 6 + length;
		}
		onpacket03(client: WSClient, data: Uint8Array): number
		{
			if (!this.transmit)
				throw 'Invalid';
			if (data[0] != this.id)
				return -1;
			var pktid = data[2] | (data[3] << 8);
			if (pktid == this.pktid)
			{
				this.pktid += 1;
				this.idx += this.pending;
				
				if (this.idx < this.data.length)
					this.sendpacket(client);
				else
					this.senddone(client);
			}
			return 4;
		}
		onpacket04(client: WSClient, data: Uint8Array): number
		{
			if (data[0] != this.id)
				return -1;
			return 1;
		}
		senddone(client: WSClient)
		{
			client.send(new Uint8Array([0xC2, 0x04, this.remoteid]));
			this.cb(this.name, this.data);
		}
		sendpacket(client: WSClient)
		{
			var toSend = Math.min(512, this.data.length - this.idx);
			var data = new Uint8Array(8 + toSend);
			data[0] = 0xC2;
			data[1] = 0x02;
			data[2] = this.remoteid & 0xff;
			data[3] = 0x00;
			data[4] = this.pktid & 0xff;
			data[5] = this.pktid >> 8;
			data[6] = toSend & 0xff;
			data[7] = toSend >> 8;
			this.pending = toSend;
			data.set(this.data.subarray(this.idx, this.idx + toSend), 8);
			client.send(data);
		}
	}
}