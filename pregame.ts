/// <reference path="game.ts" />
/// <reference path="jquery.d.ts" />
module PreGame
{
	export class Modal
	{
		callback: () => void;
		$modal: JQuery;
		constructor(callback: () => void, selector: string)
		{
			this.callback = callback;
			this.$modal = $(selector);
		}
		finish()
		{
			this.$modal.addClass('hide');
			this.callback();
		}
		show()
		{
			this.$modal.removeClass('hide');
		}
	}
	
	export class ModalCharacter extends Modal
	{
		$name: JQuery;
		$submit: JQuery;
		constructor(options: Game.IOptions, callback: () => void)
		{
			super(callback, '#modal_character');
			
			this.$name = $('#character_name', this.$modal);
			this.$submit = $('#character_submit', this.$modal);
			
			// read default values from localStorage
			var val: string;
			val = localStorage.getItem('playerName');
			if (val)
				this.$name.val(val);
			
			this.$submit.click((evt: JQueryEventObject) => {
				if (this.$name.val())
				{
					options.playerName = this.$name.val();
					localStorage.setItem('playerName', options.playerName);
				}
				this.finish();
			});
		}
	}
	
	export class ModalInstructions extends Modal
	{
		constructor(callback: () => void)
		{
			super(callback, '#modal_instructions');
			
			this.$modal.click((evt: JQueryEventObject) => {
				this.finish();
			});
		}
	}
}