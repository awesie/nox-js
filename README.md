# Nox-JS #

JavaScript client for Nox

### Build requirements

* TypeScript 1.5
* Visual Studio Code (recommended)

### License

* All included TypeScript code (not including pure module definitions) are licensed under GPLv3.