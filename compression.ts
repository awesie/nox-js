module Compression
{
	var _output = new Uint8Array(256*1024*1024);
	
	export function decompress(input: Uint8Array) : Uint8Array
	{
		var output: Uint8Array = _output,
			outputSize = 0,
			flg, bd, hc,
			offset = 0, outputOffset = 0,
			inputSize = input.length;
		
		offset += 4; // magic
		flg = input[offset++];
		bd = input[offset++];
		if (flg & 0x80)
		{
			outputSize = input[offset] | (input[offset+1] << 8) | (input[offset+2] << 16);
			offset += 8; // content-size
		}
		hc = input[offset++];
		
		while (offset < inputSize)
		{
			var blockSize = input[offset] | (input[offset+1] << 8) | (input[offset+2] << 16) | (input[offset+3] << 24);
			offset += 4;
			var blockEnd = offset + blockSize;
			
			if (blockSize & 0x80000000)
			{
				// uncompressed block
				blockSize &= 0x7fffffff;
				for (var i = 0; i < blockSize; i++)
					output[outputOffset++] = input[offset++];
				continue;
			}
			
			// compressed block
			while (offset < blockEnd)
			{
				var token = input[offset++];
				var litLength = token >> 4;
				var matchLength = token & 0xf;
				if (litLength == 0xf)
				{
					do {
						token = input[offset++];
						litLength += token;
					} while (token == 0xff);
				}
				for (var i = 0; i < litLength; i++)
				{
					output[outputOffset++] = input[offset++];
				}
				var matchOffset = input[offset] | (input[offset+1] << 8);
				offset += 2;
				if (matchLength == 0xf)
				{
					do {
						token = input[offset++];
						matchLength += token;
					} while (token == 0xff);
				}
				matchLength += 4;
				var start = outputOffset - matchOffset;
				for (var i = 0; i < matchLength; i++)
					output[outputOffset++] = output[start++];
			}
			break;
		}
		
		return output.subarray(0, outputOffset);
	}
}