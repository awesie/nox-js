/// <reference path="game.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="loader.ts" />
/// <reference path="pixi.d.ts" />
/// <reference path="pregame.ts" />

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}

var renderer = new PIXI.CanvasRenderer(1024, 768);

PIXI.ticker.shared.autoStart = false;
PIXI.ticker.shared.stop();
$('#game').append(renderer.view);

var server = getQueryVariable('server');
if (!server)
	server = 'america';

var servers = {
	europe: "ws://81.4.107.152:7681",
	america: "ws://185.35.78.52:7681"
};

// default game options
var options: Game.IOptions = {
    gameServer: '192.3.191.120',
    gameServerPort: 18590,
    proxyServer: servers[server],
    playerName: 'Jack',
    playerClass: Game.PlayerClass.WARRIOR,
    gauntletMode: false
};

if (getQueryVariable('quest'))
{
    options.gameServerPort = 18591;
    options.gauntletMode = true;
}

function hideGame()
{
    var $game = $('#game'), $chat = $('#chat');
    $game.addClass('hide');
    $chat.addClass('hide'); 
}

function showGame()
{ 
    var $game = $('#game'), $chat = $('#chat');
    $game.removeClass('hide');
    $chat.removeClass('hide'); 
}

showGame();
Loader.waitForAppCache(renderer, () => {
    hideGame();
    var modal: PreGame.Modal = new PreGame.ModalInstructions(() =>
    {
        var modal: PreGame.Modal = new PreGame.ModalCharacter(options, () =>
        {
            showGame();
            Game.main(options, renderer, <HTMLDivElement>$('#chatlog').get(0), <HTMLInputElement>$('#chatinput').get(0));
        });
        modal.show();
    });
    modal.show();
});