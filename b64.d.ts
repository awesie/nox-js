declare module base64js
{
	export function toByteArray(encoded: string): Uint8Array;
	export function fromByteArray(data: Uint8Array): string;
}