/// <reference path="assets.ts" />
/// <reference path="audio.ts" />
/// <reference path="client.ts" />
/// <reference path="compression.ts" />
/// <reference path="pixi.d.ts" />
/// <reference path="underscore.d.ts" />
/// <reference path="visibility.ts" />

declare function sprintf(fmt: string, ...args: any[]): string;
declare function vsprintf(fmt: string, args: any[]): string;

module Game
{
	export var theAudio: GameAudio.Audio;
	export var theChat: Chat;
	export var theClient: Client;
	export var theAssets: Assets.AssetLoader;
	export var thePlayer: Player;
	export var theModifiers: ModifierDb;
	export var theStrings: JStrings;
	export var theThings: ThingDb;
	export var theWorld: World;
	
	var imagesCache: { [id: number]: PIXI.Texture };
	var imagesIdx: Uint32Array[];
	var imagesPkg: Uint8Array[];
	
	export var audioCache: { [id: string]: AudioBuffer };
	export var audioIdx: JAudioIdx;
	export var audioPkg: Uint8Array[];
	
	interface JAudioIdx
	{
		pkgid: number;
		offset: number;
		length: number;
		rate: number;
		channels: number;
	}
	
	export function connect(options: IOptions)
	{
		thePlayer = null;
		theWorld = new World();
		theClient = new Client(options);
		
		window.addEventListener('beforeunload', (ev) => {
			theClient.sendquit();
		});
	}
	
	export function main(options: IOptions, renderer: PIXI.CanvasRenderer, chatDiv: HTMLDivElement, chatInput: HTMLInputElement)
	{
		audioCache = {};
		imagesCache = {};
		theAudio = new GameAudio.Audio();
		theAssets = new Assets.AssetLoader("assets");
		theAssets.onerror = (url: string) => {
			console.log("Error loading " + url);
		};
		theAssets.load("Estate.json", (url, response) => true);
		theAssets.load("Strings.json", (url, response) => {
			theStrings = JSON.parse(response);
			// canonicalize key capitalization
			var keys = _.keys(theStrings.messages);
			keys.forEach((k: string) => {
				var value = theStrings.messages[k];
				delete theStrings.messages[k];
				theStrings.messages[k.toLowerCase()] = value;
			});
			return false;
		});
		theAssets.load("ModifierDb.json", (url, response) => {
			var moddb: JModifierDb = JSON.parse(response);
			theModifiers = new ModifierDb(moddb);
			return false;
		});
		theAssets.load("ThingDb.json", (url, response) => {
			var thingdb: JThingDb = JSON.parse(response);
			theThings = new ThingDb(thingdb);
			return false;
		});
		imagesIdx = [];
		imagesPkg = [];
		for (var i = 0; i < 140; i++)
		{
			((i) => {
				theAssets.load("images/"+i.toString()+".idx3", (url, response) => {
					var blob: ArrayBuffer = response;
					imagesIdx[i] = new Uint32Array(blob);
					return false;
				}, true);
				theAssets.load("images/"+i.toString()+".pkg3", (url, response) => {
					var blob: ArrayBuffer = response;
					imagesPkg[i] = new Uint8Array(blob);
					return false;
				}, true);
			})(i);
		}
		audioPkg = [];
		theAssets.load("audio.json", (url, response) => {
			audioIdx = JSON.parse(response);
			return false;
		});
		for (var i = 0; i < 22; i++)
		{
			((i) => {
				theAssets.load("audio/audio"+i.toString()+".pkg", (url, response) => {
					var blob: ArrayBuffer = response;
					audioPkg[i] = new Uint8Array(blob);
					return false;
				}, true);
			})(i);
		}
		theAssets.wait(() => {
			if (!theAssets.errors)
			{
				theChat = new Chat(chatDiv, chatInput);
				connect(options);
				theWorld.render(renderer);
			}
		});
	}
	
	function createobject(extent: number, type: number) : Object
	{
		var thing = theThings.objects[type];
		switch (thing.update ? thing.update.split(' ')[0] : '')
		{
		case "HarpoonUpdate":
			return new HarpoonObject(extent, type);
		case "PlayerUpdate":
			return new PlayerObject(extent, type);
		case "ProjectileUpdate":
			return new ProjectileObject(extent, type); 
		case 'FlagUpdate':
		case "WeaponArmorUpdate":
			return new WeaponArmorObject(extent, type);
		case 'OpenUpdate':
		case 'BreakUpdate':
		case 'TrapDoorUpdate':
			return new StateObject(extent, type);
		case 'MonsterUpdate':
			return new MonsterObject(extent, type);
		case 'MonsterGeneratorUpdate':
			return new MonsterGeneratorObject(extent, type);
		case 'FistUpdate':
		case 'MeteorUpdate':
			return new FistMeteorObject(extent, type);
		case 'PixieUpdate':
			return new PixieObject(extent, type);
		case 'BoulderUpdate':
			return new BoulderObject(extent, type);
		case 'DoorUpdate':
			return new DoorObject(extent, type);
		case 'TriggerUpdate':
			return new TriggerObject(extent, type);
		default:
			return new Object(extent, type);
		}
	}
	
	export function playaudio(id: string, pan: number, volume: number): AudioNode
	{
		if (id.indexOf('.') >= 0)
			id = id.substring(0, id.indexOf('.'));
		if (audioCache[id])
			return theAudio.playBuffer(audioCache[id], pan, volume);
		
		var idx: JAudioIdx = audioIdx[id];
		var data: Uint8Array = audioPkg[idx.pkgid].subarray(idx.offset, idx.offset + idx.length);
		var uncomp: Uint8Array = Compression.decompress(data);
		audioCache[id] = theAudio.getBufferFromIMA(uncomp, idx.rate, idx.channels);
		return theAudio.playBuffer(audioCache[id], pan, volume);
	}
	
	export function playaudiobyname(name: string, pan: number, volume: number): AudioNode
	{
		var aud = theThings.audioByName[name];
		var r = Math.min((Math.random() * (aud.sounds.length-1)) | 0, (aud.sounds.length - 1));
		return playaudio(aud.sounds[r], pan, aud.volume * volume);
	}
	
	export function getstring(id: string): string
	{
		if (!id || !theStrings.messages[id.toLowerCase()])
			return '';
		return theStrings.messages[id.toLowerCase()][Math.floor(Math.random() * Game.theStrings.messages[id.toLowerCase()].length)];
	}
	
	function getarmorcolors(color: number[], armor: number, material?: number, effectiveness?: number, primary?: number, secondary?: number)
	{
		var mod = theModifiers.armors[PlayerArmorToObjectName[armor].name];
		for (var i = 0; i < mod.colors.length; i++)
		{
			if (mod.colors[i] !== null)
				color[i] = mod.colors[i];
			else
				color[i] = 0;
		}
		if (material)
			color[mod.material] = theModifiers.modifiers[material].color;
		if (effectiveness)
			color[mod.effectiveness] = theModifiers.modifiers[effectiveness].color;
		if (primary)
			color[mod.primary] = theModifiers.modifiers[primary].color;
		if (secondary)
			color[mod.secondary] = theModifiers.modifiers[secondary].color;
	}
	
	function getweaponcolors(color: number[], armor: number, material?: number, effectiveness?: number, primary?: number, secondary?: number)
	{
		var mod = theModifiers.weapons[PlayerWeaponToObjectName[armor].name];
		for (var i = 0; i < mod.colors.length; i++)
			if (mod.colors[i] !== null)
				color[i] = mod.colors[i];
		if (material)
			color[mod.material] = theModifiers.modifiers[material].color;
		if (effectiveness)
			color[mod.effectiveness] = theModifiers.modifiers[effectiveness].color;
		if (primary)
			color[mod.primary] = theModifiers.modifiers[primary].color;
		if (secondary)
			color[mod.secondary] = theModifiers.modifiers[secondary].color;
	}
	
	function getuncomp(id: number): Uint8Array
	{		
		var subdir = Math.floor(id / 1000);
		var offset = imagesIdx[subdir][(id % 1000) * 3];
		var length = imagesIdx[subdir][(id % 1000) * 3 + 1] & 0x7fffffff;
		var coords = imagesIdx[subdir][(id % 1000) * 3 + 2];
		
		return Compression.decompress(imagesPkg[subdir].subarray(offset, offset + length));
	}
	
	function drawtile(data: number[] | Uint8Array, id: number)
	{
		var uncomp = getuncomp(id);
		var uncompOffset = 0;
		for (var y = 0; y <= 45; y++)
		{
			var xstart = 22 - y;
			var xend = 22 + y;
			if (y > 22)
			{
				xstart = 22 - (45 - y)
				xend = 22 + (45 - y);
			}
			for (var x = xstart; x <= xend; )
			{
				var pixel = uncomp[uncompOffset++];
				pixel |= uncomp[uncompOffset++] << 8;
				data[(46 * y + x) * 4 + 0] = ((pixel << 1) >> 8) & 0xff;
				data[(46 * y + x) * 4 + 1] = ((pixel << 6) >> 8) & 0xff;
				data[(46 * y + x) * 4 + 2] = ((pixel << 11) >> 8) & 0xff;
				data[(46 * y + x) * 4 + 3] = 0xff;
				x++;
			}
		}
	}
	
	function drawedge(data: number[], tileid: number, edgeid: number)
	{
		var tmp = new Uint8Array(46 * 46 * 4);
		drawtile(tmp, tileid);
		
		var uncomp = getuncomp(edgeid);
		var start = uncomp[0];
		var end = uncomp[1];
		
		var uncompOffset = 2;
		for (var y = start; y <= end; y++)
		{
			var xstart = 22 - y;
			var xend = 22 + y;
			if (y > 22)
			{
				xstart = 22 - (45 - y)
				xend = 22 + (45 - y);
			}
			
			for (var x = xstart; x <= xend; )
			{
				var token = uncomp[uncompOffset++];
				var length = uncomp[uncompOffset++];
				switch (token)
				{
				case 1:
					x += length;
					break;
				case 2:
					for (var i = 0; i < length; ++i)
					{
						var pixel = uncomp[uncompOffset++];
						pixel |= uncomp[uncompOffset++] << 8;
						data[(46 * y + x) * 4 + 0] = ((pixel << 1) >> 8) & 0xff;
						data[(46 * y + x) * 4 + 1] = ((pixel << 6) >> 8) & 0xff;
						data[(46 * y + x) * 4 + 2] = ((pixel << 11) >> 8) & 0xff;
						data[(46 * y + x) * 4 + 3] = 0xff;
						x++;
					}
					break;
				case 3:
					for (var i = 0; i < length; ++i)
					{
						data[(46 * y + x) * 4 + 0] = tmp[(46 * y + x) * 4 + 0];
						data[(46 * y + x) * 4 + 1] = tmp[(46 * y + x) * 4 + 1];
						data[(46 * y + x) * 4 + 2] = tmp[(46 * y + x) * 4 + 2];
						data[(46 * y + x) * 4 + 3] = tmp[(46 * y + x) * 4 + 3];
						x++;
					}
					break;
				}
			}
		}
	}
	
	function drawimage(imageData: ImageData, id: number, colors: number[])
	{
		var destWidth = imageData.width,
			destHeight = imageData.height,
			destData = imageData.data;

		var uncomp = getuncomp(id);
		var width = uncomp[0] | (uncomp[1] << 8);
		var height = uncomp[4] | (uncomp[5] << 8);
		var startX = uncomp[8] | (uncomp[9] << 8);
		var startY = uncomp[12] | (uncomp[13] << 8);
		
		var uncompOffset = 17; // skip a byte
		for (var y = startY; y < height + startY; y++)
		{
			for (var x = startX; x < width + startX;)
			{
				var color;
				var token = uncomp[uncompOffset++];
				var length = uncomp[uncompOffset++];
				
				switch (token & 0xF)
				{
					case 1:
						x += length;
						break;
					case 2:
						for (var i = 0; i < length; i++)
						{
							var pixel = uncomp[uncompOffset++];
							pixel |= uncomp[uncompOffset++] << 8;
							destData[(destWidth * y + x) * 4 + 0] = ((pixel << 1) >> 8) & 0xff;
							destData[(destWidth * y + x) * 4 + 1] = ((pixel << 6) >> 8) & 0xff;
							destData[(destWidth * y + x) * 4 + 2] = ((pixel << 11) >> 8) & 0xff;
							destData[(destWidth * y + x) * 4 + 3] = 0xff;
							x++;
						}
						break;
					case 4:
						color = colors[(token >> 4) - 1];
                        for (var i = 0; i < length; i++) {
                            var intensity = uncomp[uncompOffset++] / 255.0;
                            destData[(destWidth * y + x) * 4 + 0] = ((color >> 16) & 0xff) * intensity;
                            destData[(destWidth * y + x) * 4 + 1] = ((color >> 8) & 0xff) * intensity;
                            destData[(destWidth * y + x) * 4 + 2] = ((color >> 0) & 0xff) * intensity;
                            destData[(destWidth * y + x) * 4 + 3] = 0xff;
                            x++;
                        }
						break;
					case 5:
						for (var i = 0; i < length; i++)
						{
							var pixel = uncomp[uncompOffset++];
							pixel |= uncomp[uncompOffset++] << 8;
							destData[(destWidth * y + x) * 4 + 0] = ((pixel >> 12) & 0xf) << 4;
							destData[(destWidth * y + x) * 4 + 1] = ((pixel >> 8) & 0xf) << 4;
							destData[(destWidth * y + x) * 4 + 2] = ((pixel >> 4) & 0xf) << 4;
							destData[(destWidth * y + x) * 4 + 3] = ((pixel >> 0) & 0xf) << 4;
							x++;
						}
						break;
					default:
						console.log("Unknown", (token & 0xff));
						break;
				}
			}
		}
	}
	
	export function getimage(id: number, oncomplete?: (id: number) => void) : PIXI.Texture
	{
		if (imagesCache[id])
			return imagesCache[id];
		var subdir = Math.floor(id / 1000);
		var offset = imagesIdx[subdir][(id % 1000) * 3];
		var length = imagesIdx[subdir][(id % 1000) * 3 + 1];
		var coords = imagesIdx[subdir][(id % 1000) * 3 + 2];
		if (length & 0x80000000)
		{	
			var uncomp = getuncomp(id);
			var width = uncomp[0] | (uncomp[1] << 8);
			var height = uncomp[4] | (uncomp[5] << 8);
			var startX = uncomp[8] | (uncomp[9] << 8);
			var startY = uncomp[12] | (uncomp[13] << 8);
			if (width == 0 || height == 0)
				return PIXI.Texture.EMPTY;
			var buffer = new PIXI.CanvasBuffer(width + startX, height + startY);
			var imagedata = buffer.context.createImageData(width + startX, height + startY);
			drawimage(imagedata, id, [0, 0, 0, 0, 0, 0]);
			buffer.context.putImageData(imagedata, 0, 0);
			imagesCache[id] = PIXI.Texture.fromCanvas(buffer.canvas);
			return imagesCache[id];
		}
		var realWidth = coords & 0xffff;
		var realHeight = coords >> 16;
		var blob = new Blob([imagesPkg[subdir].subarray(offset, offset + length)], { type: 'image/png' });
		var src = URL.createObjectURL(blob);
		imagesCache[id] = PIXI.Texture.fromImage(src);
		imagesCache[id].baseTexture.addListener('error', () => {
			URL.revokeObjectURL(src);
			if (oncomplete)
				oncomplete(id);
		});
		imagesCache[id].baseTexture.addListener('loaded', () => {
			URL.revokeObjectURL(src);
			if (oncomplete)
				oncomplete(id);
		});
		imagesCache[id].addListener('update', () => {
			var crop = imagesCache[id].crop;
			crop.x = crop.width - realWidth;
			crop.width = realWidth;
			crop.y = crop.height - realHeight;
			crop.height = realHeight;
			imagesCache[id].removeAllListeners('update');
		});
		return imagesCache[id];
	}
	
	function getimagepath(id: number) : string
	{
		var subdir = Math.floor(id / 1000).toString(10);
		return theAssets.base + "/images/" + subdir + "/" + id.toString(10) + ".png"; 
	}
	
	export enum SparkColor
	{
		Red,
		White,
		LightBlue,
		Orange,
		Green,
		Violet,
		LightViolet,
		Yellow,
		Blue
	}
	
	export enum WallFlag
	{
		Shadow = 0x01,
		NonBlocking = 0x02,
		Short = 0x04
	}
	
	export enum Action
	{
		Move = 0x01,
		Jump = 0x02,
		Attack = 0x04
	}
	
	export enum PlayerClass
	{
		WARRIOR,
		WIZARD,
		CONJURER
	}
	
	export enum ThingFlags
	{
		AIRBORNE = 0x1,
		ALLOW_OVERLAP = 0x2,
		BELOW = 0x4,
		DANGEROUS = 0x8,
		EDIT_VISIBLE = 0x10,
		FLICKER = 0x20,
		IMMOBILE = 0x40,
		MISSILE_HIT = 0x80,
		NO_AUTO_DROP = 0x100,
		NO_COLLIDE = 0x200,
		NO_COLLIDE_OWNER = 0x400,
		NO_PUSH_CHARACTERS = 0x800,
		NONE = 0x1000,
		OWNER_VISIBLE = 0x2000,
		RESPAWN = 0x4000,
		SHADOW = 0x8000,
		SHORT = 0x10000,
		SIGHT_DESTROY = 0x20000,
	}
	
	export enum ThingClass
	{
		MISSILE = 0x000001,
		MONSTER = 0x000002,
		PLAYER = 0x000004,
		OBSTACLE = 0x000008,
		FOOD = 0x000010,
		EXIT = 0x000020,
		KEY = 0x000040,
		DOOR = 0x000080,
		INFO_BOOK = 0x000100,
		TRIGGER = 0x000200,
		TRANSPORTER = 0x000400,
		HOLE = 0x000800,
		WAND = 0x001000,
		FIRE = 0x002000,
		ELEVATOR = 0x004000,
		ELEVATOR_SHAFT = 0x008000,
		DANGEROUS = 0x010000,
		MONSTERGENERATOR = 0x020000,
		READABLE = 0x040000,
		LIGHT = 0x080000,
		SIMPLE = 0x100000,
		COMPLEX = 0x200000,
		IMMOBILE = 0x400000,
		VISIBLE_ENABLE = 0x800000,
		WEAPON = 0x1000000,
		ARMOR = 0x2000000,
		NOT_STACKABLE = 0x4000000,
		TREASURE = 0x8000000,
		FLAG = 0x10000000,
		CLIENT_PERSIST = 0x20000000,
		CLIENT_PREDICT = 0x40000000,
		PICKUP = 0x80000000,
	}
	
	export enum WallFacing
	{
		UP,
		DOWN,
		CROSS,
		TNORTHWEST,
		TNORTHEAST,
		TSOUTHEAST,
		TSOUTHWEST,
		ARROWDOWN,
		ARROWLEFT,
		ARROWUP,
		ARROWRIGHT,
		WINDOWUP,
		WINDOWDOWN,
		RIGHTHALFARROWUP,
		LETHALFARROWUP		
	}
	
	export enum Enchant
	{
		ENCHANT_INVISIBLE,
		ENCHANT_MOONGLOW,
		ENCHANT_BLINDED,
		ENCHANT_CONFUSED,
		ENCHANT_SLOWED,
		ENCHANT_HELD,
		ENCHANT_DETECTING,
		ENCHANT_ETHEREAL,
		ENCHANT_RUN,
		ENCHANT_HASTED,
		ENCHANT_VILLAIN,
		ENCHANT_AFRAID,
		ENCHANT_BURNING,
		ENCHANT_VAMPIRISM,
		ENCHANT_ANCHORED,
		ENCHANT_LIGHT,
		ENCHANT_DEATH,
		ENCHANT_PROTECT_FROM_FIRE,
		ENCHANT_PROTECT_FROM_POISON,
		ENCHANT_PROTECT_FROM_MAGIC,
		ENCHANT_PROTECT_FROM_ELECTRICITY,
		ENCHANT_INFRAVISION,
		ENCHANT_SHOCK,
		ENCHANT_INVULNERABLE,
		ENCHANT_TELEKINESIS,
		ENCHANT_FREEZE,
		ENCHANT_SHIELD,
		ENCHANT_REFLECTIVE_SHIELD,
		ENCHANT_CHARMING,
		ENCHANT_ANTI_MAGIC,
		ENCHANT_CROWN,
		ENCHANT_SNEAK
	}
	
	export enum ItemEnchant
	{
		FireProtect,
		PoisonProtect,
		LightningProtect,
		Brilliance,
		Speed,
		Regeneration
	}
	
	export enum PlayerArmor
	{
        NO_ARMOR = 0,
        ALL_CLOTH_ARMOR = 0x4C0F,
        ALL_LEATHER_ARMOR = 0x2090D0,
        ALL_TORSO_ARMOR = 0x3CC02,
        ALL_HELM_ARMOR = 0x0FC0000,
        ALL_ARM_ARMOR = 0x3000,
        ALL_FEET_ARMOR = 0x1C1,
        ALL_LEG_ARMOR = 0x23C,
        ALL_HAND_ARMOR = 0x3000000,
        STREET_PANTS = 0x4,
        MEDIEVAL_PANTS = 0x8,
        STREET_SNEAKERS = 0x1,
        LEATHER_BOOTS = 0x40,
        LEATHER_ARMORED_BOOTS = 0x80,
        PLATE_BOOTS = 0x100,
        LEATHER_LEGGINGS = 0x10,
        CHAIN_LEGGINGS = 0x20,
        PLATE_LEGGINGS = 0x200,
        STREET_SHIRT = 0x400,
        MEDIEVAL_SHIRT = 0x800,
        WIZARD_ROBE = 0x4000,
        LEATHER_TUNIC = 0x8000,
        CHAIN_TUNIC = 0x10000,
        PLATE_BREAST = 0x20000,
        LEATHER_ARMBANDS = 0x1000,
        PLATE_ARMS = 0x2000,
        MEDIEVAL_CLOAK = 0x2,
        ROUND_SHIELD = 0x1000000,
        KITE_SHIELD = 0x2000000,
        CHAIN_COIF = 0x40000,
        WIZARD_HELM = 0x80000,
        CONJURER_HELM = 0x100000,
        LEATHER_HELM = 0x200000,
        PLATE_HELM = 0x400000,
        ORNATE_HELM = 0x800000
    }
	
	export enum PlayerStance
    {
        IDLE,
        DIE,
        DEAD,
        JUMP,
        WALK,
        WALK_AND_DRAG,
        RUN,
        RUNNING_JUMP,
        PICKUP,
        DODGE_LEFT,
        DODGE_RIGHT,
        ELECTROCUTED,
        FALL,
        TRIP,
        GET_UP,
        LAUGH,
        POINT,
        SIT,
        SLEEP,
        TALK,
        TAUNT,
        CAST_SPELL,
        CONCENTRATE,
        PUNCH_LEFT,
        PUNCH_RIGHT,
        PUNCH_RIGHT_HOOK,
        MACE_STRIKE,
        SWORD_STRIKE,
        LONG_SWORD_STRIKE,
        STAFF_STRIKE,
        STAFF_BLOCK,
        STAFF_SPELL_BLAST,
        STAFF_THRUST,
        SHOOT_BOW,
        SHOOT_CROSSBOW,
        AXE_STRIKE,
        GREAT_SWORD_PARRY,
        GREAT_SWORD_STRIKE,
        GREAT_SWORD_IDLE,
        HAMMER_STRIKE,
        RAISE_SHIELD,
        RECOIL_FORWARD,
        RECOIL_BACKWARD,
        RECOIL_SHIELD,
        CHAKRAM_STRIKE,
        BERSERKER_CHARGE,
        WARCRY,
        GREAT_SWORD_BLOCK_LEFT,
        GREAT_SWORD_BLOCK_RIGHT,
        GREAT_SWORD_BLOCK_DOWN,
        ELECTRIC_ZAP,
        DUST,
        RECOIL,
        SNEAK,
        HARPOONTHROW
    }
	
	export enum PlayerWeapon
    {
        NO_WEAPONS = 0x0,
        ALL_WEAPONS = 0x7FFFFFE,
        ALL_MAGICAL_STAVES = 0x47F0000,
        ALL_STAVES = 0x7FF8000,
        ALL_RANGED_WEAPONS = 0x47F00FE,
        FLAG = 0x1,
        QUIVER = 0x2,
        BOW = 0x4,
        CROSSBOW = 0x8,
        ARROW = 0x10,
        BOLT = 0x20,
        CHAKRAM = 0x40,
        SHURIKEN = 0x80,
        SWORD = 0x100,
        LONG_SWORD = 0x200,
        GREAT_SWORD = 0x400,
        MACE = 0x800,
        AXE = 0x1000,
        OGRE_AXE = 0x2000,
        HAMMER = 0x4000,
        STAFF = 0x8000,
        STAFF_SULPHOROUS_FLARE = 0x10000,
        STAFF_SULPHOROUS_SHOWER = 0x20000,
        STAFF_LIGHTNING = 0x40000,
        STAFF_FIREBALL = 0x80000,
        STAFF_TRIPLE_FIREBALL = 0x100000,
        STAFF_FORCE_OF_NATURE = 0x200000,
        STAFF_DEATH_RAY = 0x400000,
        STAFF_OBLIVION_HALBERD = 0x800000,
        STAFF_OBLIVION_HEART = 0x1000000,
        STAFF_OBLIVION_WIERDLING = 0x2000000,
        STAFF_OBLIVION_ORB = 0x4000000
    }
	
	export enum PlayerAbility
	{
		ABILITY_BERSERKER_CHARGE = 1,
		ABILITY_WARCRY,
		ABILITY_HARPOON,
		ABILITY_TREAD_LIGHTLY,
		ABILITY_EYE_OF_THE_WOLF
	}
	
	var ModelArmor = {};
	ModelArmor[PlayerArmor.CHAIN_COIF] = 14344;
	ModelArmor[PlayerArmor.CHAIN_LEGGINGS] = 14309;
	ModelArmor[PlayerArmor.CHAIN_TUNIC] = 14343;
	ModelArmor[PlayerArmor.CONJURER_HELM] = 14365;
	ModelArmor[PlayerArmor.KITE_SHIELD] = 14374;
	ModelArmor[PlayerArmor.LEATHER_ARMBANDS] = 14345;
	ModelArmor[PlayerArmor.LEATHER_ARMORED_BOOTS] = 14347;
	ModelArmor[PlayerArmor.LEATHER_BOOTS] = 14346;
	ModelArmor[PlayerArmor.LEATHER_HELM] = 14358;
	ModelArmor[PlayerArmor.LEATHER_LEGGINGS] = 14359;
	ModelArmor[PlayerArmor.LEATHER_TUNIC] = 14357;
	ModelArmor[PlayerArmor.MEDIEVAL_CLOAK] = 14307;
	ModelArmor[PlayerArmor.MEDIEVAL_PANTS] = 14354;
	ModelArmor[PlayerArmor.MEDIEVAL_SHIRT] = 14353;
	ModelArmor[PlayerArmor.ORNATE_HELM] = 14364;
	ModelArmor[PlayerArmor.PLATE_ARMS] = 14362;
	ModelArmor[PlayerArmor.PLATE_BOOTS] = 14356;
	ModelArmor[PlayerArmor.PLATE_BREAST] = 14360;
	ModelArmor[PlayerArmor.PLATE_HELM] = 14361;
	ModelArmor[PlayerArmor.PLATE_LEGGINGS] = 14363;
	ModelArmor[PlayerArmor.ROUND_SHIELD] = 14373;
	ModelArmor[PlayerArmor.STREET_PANTS] = 14351;
	ModelArmor[PlayerArmor.STREET_SHIRT] = 14352;
	ModelArmor[PlayerArmor.STREET_SNEAKERS] = 14350;
	ModelArmor[PlayerArmor.WIZARD_HELM] = 14372;
	ModelArmor[PlayerArmor.WIZARD_ROBE] = 14371;
	
	var ModelWeapon = {};
	ModelWeapon[PlayerWeapon.FLAG] = null;
	ModelWeapon[PlayerWeapon.QUIVER] = 14348;
	ModelWeapon[PlayerWeapon.BOW] = 14381;
	ModelWeapon[PlayerWeapon.CROSSBOW] = 14366;
	ModelWeapon[PlayerWeapon.ARROW] = null;
	ModelWeapon[PlayerWeapon.BOLT] = null;
	ModelWeapon[PlayerWeapon.CHAKRAM] = 14367;
	ModelWeapon[PlayerWeapon.SHURIKEN] = 14368;
	ModelWeapon[PlayerWeapon.SWORD] = 14378;
	ModelWeapon[PlayerWeapon.LONG_SWORD] = 14375;
	ModelWeapon[PlayerWeapon.GREAT_SWORD] = 14379;
	ModelWeapon[PlayerWeapon.MACE] = 14377;
	ModelWeapon[PlayerWeapon.AXE] = 14380;
	ModelWeapon[PlayerWeapon.OGRE_AXE] = 14398;
	ModelWeapon[PlayerWeapon.HAMMER] = 14376;
	ModelWeapon[PlayerWeapon.STAFF] = 14369;
	ModelWeapon[PlayerWeapon.STAFF_SULPHOROUS_FLARE] = 14383;
	ModelWeapon[PlayerWeapon.STAFF_SULPHOROUS_SHOWER] = 14384;
	ModelWeapon[PlayerWeapon.STAFF_LIGHTNING] = 14385;
	ModelWeapon[PlayerWeapon.STAFF_FIREBALL] = 14386;
	ModelWeapon[PlayerWeapon.STAFF_TRIPLE_FIREBALL] = 14387;
	ModelWeapon[PlayerWeapon.STAFF_FORCE_OF_NATURE] = 14388;
	ModelWeapon[PlayerWeapon.STAFF_DEATH_RAY] = 14391;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_HALBERD] = 14392;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_HEART] = 14393;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_WIERDLING] = 14394;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_ORB] = 14395;
	
	// Stock game data
	var PlayerWeaponToObjectName: {[id: number] : {name: string}} = {};
	PlayerWeaponToObjectName[4] = {name: "Bow"};
	PlayerWeaponToObjectName[8] = {name: "CrossBow"};
	PlayerWeaponToObjectName[0x200] = {name: "Longsword"};
	PlayerWeaponToObjectName[0x400] = {name: "GreatSword"};
	PlayerWeaponToObjectName[0x800] = {name: "MorningStar"};
	PlayerWeaponToObjectName[0x1000] = {name: "BattleAxe"};
	PlayerWeaponToObjectName[0x2000] = {name: "OgreAxe"};
	PlayerWeaponToObjectName[0x100] = {name: "Sword"};
	PlayerWeaponToObjectName[0x4000] = {name: "WarHammer"};
	PlayerWeaponToObjectName[0x40] = {name: "RoundChakram"};
	PlayerWeaponToObjectName[0x80] = {name: "FanChakram"};
	PlayerWeaponToObjectName[2] = {name: "Quiver"};
	PlayerWeaponToObjectName[0x8000] = {name: "StaffWooden"};
	PlayerWeaponToObjectName[0x10000] = {name: "SulphorousFlareWand"};
	PlayerWeaponToObjectName[0x20000] = {name: "SulphorousShowerWand"};
	PlayerWeaponToObjectName[0x80000] = {name: "LesserFireballWand"};
	PlayerWeaponToObjectName[0x80000] = {name: "DemonsBreathWand"};
	PlayerWeaponToObjectName[0x400000] = {name: "DeathRayWand"};
	PlayerWeaponToObjectName[0x200000] = {name: "InfinitePainWand"};
	PlayerWeaponToObjectName[0x100000] = {name: "FireStormWand"};
	PlayerWeaponToObjectName[0x40000] = {name: "ForceWand"};
	PlayerWeaponToObjectName[0x800000] = {name: "OblivionHalberd"};
	PlayerWeaponToObjectName[0x1000000] = {name: "OblivionHeart"};
	PlayerWeaponToObjectName[0x2000000] = {name: "OblivionWierdling"};
	PlayerWeaponToObjectName[0x4000000] = {name: "OblivionOrb"};
	PlayerWeaponToObjectName[1] = {name: "Flag"};
	var PlayerWeaponToDesc: {[id: number]: {name: string}} = {};
	PlayerWeaponToDesc[1] = {name: "Modifier.db:FlagDesc"};
	PlayerWeaponToDesc[2] = {name: "Modifier.db:QuiverDesc"};
	PlayerWeaponToDesc[4] = {name: "Modifier.db:BowDesc"};
	PlayerWeaponToDesc[8] = {name: "Modifier.db:CrossbowDesc"};
	PlayerWeaponToDesc[0x40] = {name: "Modifier.db:RoundChakramDesc"};
	PlayerWeaponToDesc[0x80] = {name: "modifier.db:FanChakramDesc"};
	PlayerWeaponToDesc[0x100] = {name: "modifier.db:SwordDesc"};
	PlayerWeaponToDesc[0x200] = {name: "modifier.db:LongswordDesc"};
	PlayerWeaponToDesc[0x400] = {name: "modifier.db:GreatSwordDesc"};
	PlayerWeaponToDesc[0x800] = {name: "modifier.db:MorningStarDesc"};
	PlayerWeaponToDesc[0x1000] = {name: "modifier.db:BattleAxeDesc"};
	PlayerWeaponToDesc[0x2000] = {name: "modifier.db:OgreAxeDesc"};
	PlayerWeaponToDesc[0x4000] = {name: "modifier.db:WarHammerDesc"};
	PlayerWeaponToDesc[0x8000] = {name: "modifier.db:StaffWoodenDesc"};
	PlayerWeaponToDesc[0x10000] = {name: "modifier.db:SulphorousFlareWandDesc"};
	PlayerWeaponToDesc[0x20000] = {name: "modifier.db:SulphorousShowerWandDesc"};
	PlayerWeaponToDesc[0x40000] = {name: "modifier.db:ForceWandDesc"};
	PlayerWeaponToDesc[0x80000] = {name: "modifier.db:LesserFireballWandDesc"};
	PlayerWeaponToDesc[0x100000] = {name: "modifier.db:FireStormWandDesc"};
	PlayerWeaponToDesc[0x200000] = {name: "modifier.db:InfinitePainWandDesc"};
	PlayerWeaponToDesc[0x400000] = {name: "modifier.db:DeathRayWandDesc"};
	PlayerWeaponToDesc[0x800000] = {name: "modifier.db:OblivionHalberdDesc"};
	PlayerWeaponToDesc[0x1000000] = {name: "modifier.db:OblivionHeartDesc"};
	PlayerWeaponToDesc[0x2000000] = {name: "modifier.db:OblivionWierdlingDesc"};
	PlayerWeaponToDesc[0x4000000] = {name: "modifier.db:OblivionOrbDesc"};
	var PlayerArmorToObjectName: {[id: number] : {name: string, u1: string, u3: string}} = {};
	PlayerArmorToObjectName[0x8000] = {name: "LeatherArmor", u1: "LeatherArmorName", u3: "LeatherArmorDestroyed"};
	PlayerArmorToObjectName[0x10000] = {name: "ChainTunic", u1: "ChainTunicName", u3: "INVALID"};
	PlayerArmorToObjectName[0x20000] = {name: "Breastplate", u1: "SteelBreastplateName", u3: "BreastplateDestroyed"};
	PlayerArmorToObjectName[0x200000] = {name: "LeatherHelm", u1: "LeatherHelmetName", u3: "LeatherHelmDestroyed"};
	PlayerArmorToObjectName[0x400000] = {name: "SteelHelm", u1: "SteelHelmetName", u3: "SteelHelmDestroyed"};
	PlayerArmorToObjectName[0x2000000] = {name: "SteelShield", u1: "SteelShieldName", u3: "SteelShieldDestroyed"};
	PlayerArmorToObjectName[0x1000000] = {name: "WoodenShield", u1: "WoodenShieldName", u3: "WoodenShieldDestroyed"};
	PlayerArmorToObjectName[0x400] = {name: "StreetShirt", u1: "StreetShirtName", u3: "INVALID"};
	PlayerArmorToObjectName[4] = {name: "StreetPants", u1: "StreetPantsName", u3: "INVALID"};
	PlayerArmorToObjectName[1] = {name: "StreetSneakers", u1: "StreetSneakersName", u3: "INVALID"};
	PlayerArmorToObjectName[0x40] = {name: "LeatherBoots", u1: "LeatherBootsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x80] = {name: "LeatherArmoredBoots", u1: "LeatherArmoredBootsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x100] = {name: "PlateBoots", u1: "PlateBootsName", u3: "INVALID"};
	PlayerArmorToObjectName[8] = {name: "MedievalPants", u1: "MedievalPantsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x10] = {name: "LeatherLeggings", u1: "LeatherLeggingsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x20] = {name: "ChainLeggings", u1: "ChainLeggingsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x200] = {name: "PlateLeggings", u1: "PlateLeggingsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x800] = {name: "MedievalShirt", u1: "MedievalShirtName", u3: "INVALID"};
	PlayerArmorToObjectName[0x4000] = {name: "WizardRobe", u1: "WizardRobeName", u3: "INVALID"};
	PlayerArmorToObjectName[0x1000] = {name: "LeatherArmbands", u1: "LeatherArmbandsName", u3: "INVALID"};
	PlayerArmorToObjectName[0x2000] = {name: "PlateArms", u1: "PlateArmsName", u3: "INVALID"};
	PlayerArmorToObjectName[2] = {name: "MedievalCloak", u1: "MedievalCloakName", u3: "INVALID"};
	PlayerArmorToObjectName[0x40000] = {name: "ChainCoif", u1: "ChainCoifName", u3: "INVALID"};
	PlayerArmorToObjectName[0x80000] = {name: "WizardHelm", u1: "WizardHelmName", u3: "INVALID"};
	PlayerArmorToObjectName[0x100000] = {name: "ConjurerHelm", u1: "ConjurerHelmName", u3: "INVALID"};
	PlayerArmorToObjectName[0x800000] = {name: "OrnateHelm", u1: "OrnateHelmName", u3: "INVALID"};
	var PlayerArmorToDesc: {[id: number]: {name: string}} = {};
	PlayerArmorToDesc[4] = {name: "modifier.db:StreetPantsDesc"};
	PlayerArmorToDesc[8] = {name: "modifier.db:MedievalPantsDesc"};
	PlayerArmorToDesc[1] = {name: "modifier.db:StreetSneakersDesc"};
	PlayerArmorToDesc[0x40] = {name: "modifier.db:LeatherBootsDesc"};
	PlayerArmorToDesc[0x80] = {name: "modifier.db:LeatherArmoredBootsDesc"};
	PlayerArmorToDesc[0x100] = {name: "modifier.db:PlateBootsDesc"};
	PlayerArmorToDesc[0x10] = {name: "modifier.db:LeatherLeggingsDesc"};
	PlayerArmorToDesc[0x20] = {name: "modifier.db:ChainLeggingsDesc"};
	PlayerArmorToDesc[0x200] = {name: "modifier.db:PlateLeggingsDesc"};
	PlayerArmorToDesc[0x400] = {name: "modifier.db:StreetShirtDesc"};
	PlayerArmorToDesc[0x800] = {name: "modifier.db:MedievalShirtDesc"};
	PlayerArmorToDesc[0x1000] = {name: "modifier.db:LeatherArmbandsDesc"};
	PlayerArmorToDesc[0x2000] = {name: "modifier.db:PlateArmsDesc"};
	PlayerArmorToDesc[0x4000] = {name: "modifier.db:WizardRobeDesc"};
	PlayerArmorToDesc[0x8000] = {name: "modifier.db:LeatherArmorDesc"};
	PlayerArmorToDesc[0x10000] = {name: "modifier.db:ChainTunicDesc"};
	PlayerArmorToDesc[0x20000] = {name: "modifier.db:BreastplateDesc"};
	PlayerArmorToDesc[2] = {name: "modifier.db:MedievalCloakDesc"};
	PlayerArmorToDesc[0x1000000] = {name: "modifier.db:WoodenShieldDesc"};
	PlayerArmorToDesc[0x2000000] = {name: "modifier.db:SteelShieldDesc"};
	PlayerArmorToDesc[0x40000] = {name: "modifier.db:ChainCoifDesc"};
	PlayerArmorToDesc[0x80000] = {name: "modifier.db:WizardHelmDesc"};
	PlayerArmorToDesc[0x100000] = {name: "modifier.db:ConjurerHelmDesc"};
	PlayerArmorToDesc[0x200000] = {name: "modifier.db:LeatherHelmDesc"};
	PlayerArmorToDesc[0x400000] = {name: "modifier.db:SteelHelmDesc"};
	PlayerArmorToDesc[0x800000] = {name: "modifier.db:OrnateHelmDesc"};
	var AbilityDelays = {};
	AbilityDelays[PlayerAbility.ABILITY_BERSERKER_CHARGE] = [300];
	AbilityDelays[PlayerAbility.ABILITY_WARCRY] = [300];
	AbilityDelays[PlayerAbility.ABILITY_HARPOON] = [150];
	AbilityDelays[PlayerAbility.ABILITY_TREAD_LIGHTLY] = [30];
	AbilityDelays[PlayerAbility.ABILITY_EYE_OF_THE_WOLF] = [600];
	
	var AbilityImages = {};
	AbilityImages[PlayerAbility.ABILITY_BERSERKER_CHARGE] = [132051, 132056];
	AbilityImages[PlayerAbility.ABILITY_WARCRY] = [132049, 132054];
	AbilityImages[PlayerAbility.ABILITY_HARPOON] = [135678, 135679];
	AbilityImages[PlayerAbility.ABILITY_TREAD_LIGHTLY] = [132048, 132053];
	AbilityImages[PlayerAbility.ABILITY_EYE_OF_THE_WOLF] = [132052, 132057];
	
	export var TeamData = [
		null, // no team
		{color: 0xFF8080, name: 'Red'},
		{color: 0x4080FF, name: 'Blue'},
		{color: 0x00FF00, name: 'Green'},
		{color: 0x00FFFF, name: 'Cyan'},
		{color: 0xFFFF00, name: 'Yellow'},
		{color: 0xFF00FF, name: 'Violet'},
		{color: 0x4B4B4B, name: 'Black'},
		{color: 0xFFFFFF, name: 'White'},
		{color: 0xFF8000, name: 'Orange'}
	];
	
	var TeamMaterials = {
		'MaterialTeamRed': 0xFF0000,
		'MaterialTeamBlue': 0x0000FF,
		'MaterialTeamGreen': 0x00FF00,
		'MaterialTeamCyan': 0x00FFFF,
		'MaterialTeamYellow': 0xFFFF00,
		'MaterialTeamViolet': 0xFF00FF,
		'MaterialTeamBlack': 0x4B4B4B,
		'MaterialTeamWhite': 0xFFFFFF,
		'MaterialTeamOrange': 0xFF8000
	};
	
	var BirdiesImages = {
		delay: 1,
		image: [14821, 14919, 14909, 14890, 14920, 14934, 14921, 14499, 14650]
	};
	
	export var ReflectiveShield = [
		{name: "ReflectiveShieldNW", x: -20, y: -20},
		{name: "ReflectiveShieldN", x: 0, y: -20},
		{name: "ReflectiveShieldNE", x: 20, y: -20},
		{name: "ReflectiveShieldW", x: -20, y: 0},
		{name: "ReflectiveShieldE", x: 20, y: 0},
		{name: "ReflectiveShieldSW", x: -20, y: 20},
		{name: "ReflectiveShieldS", x: 0, y: 20},
		{name: "ReflectiveShieldSE", x: 20, y: 20}
	];
	
	var SphericalShieldImages = {
		delay: 1,
		image: [135998, 135999, 136001, 136002, 136003, 136004, 136005, 136006, 136007, 136008, 136009]	
	};
	
	var SphericalShieldNames = [
		"SphericalShieldNW",
		"SphericalShieldN",
		"SphericalShieldNE",
		"SphericalShieldW",
		"",
		"SphericalShieldE",
		"SphericalShieldSW",
		"SphericalShieldS",
		"SphericalShieldSE",
	];
	
	interface IEnchantData
	{
		image: number;
		desc: string;
	}
	
	var EnchantData: {[id: number]: IEnchantData} = {};
	EnchantData[Enchant.ENCHANT_INFRAVISION] = {image: 131875, desc: 'thing.db:Infravision'};
	
	var ItemEnchantData: {[id: number]: IEnchantData} = {};
	ItemEnchantData[ItemEnchant.Brilliance] = {image: 15037, desc: 'modifier.db:BrillianceItemEnchantDesc'};
	ItemEnchantData[ItemEnchant.FireProtect] = {image: 15039, desc: 'modifier.db:FireProtectItemEnchantDesc'};
	ItemEnchantData[ItemEnchant.LightningProtect] = {image: 15040, desc: 'modifier.db:LightningProtItemEnchantDesc'};
	ItemEnchantData[ItemEnchant.PoisonProtect] = {image: 15041, desc: 'modifier.db:PoisonProtectItemEnchantDesc'};
	ItemEnchantData[ItemEnchant.Regeneration] = {image: 15042, desc: 'thing.db:LesserHeal'};
	ItemEnchantData[ItemEnchant.Speed] = {image: 15038, desc: 'modifier.db:SpeedItemEnchantDesc'};
	
	var QuickBarSlots = [
		PlayerAbility.ABILITY_BERSERKER_CHARGE,
		PlayerAbility.ABILITY_WARCRY,
		PlayerAbility.ABILITY_HARPOON,
		PlayerAbility.ABILITY_TREAD_LIGHTLY,
		PlayerAbility.ABILITY_EYE_OF_THE_WOLF
	];
	
	var HealthPotions: string[] = [
		"RedPotion",
		"Meat",
		"RedApple"
	];
	
	var SoundEffectNames: string[] = [
		"NULL",
		"AnchorCast",
		"AnchorOn",
		"AnchorOff",
		"BlindCast",
		"BlindOn",
		"BlindOff",
		"BlinkCast",
		"BurnCast",
		"CleansingFlameCast",
		"ChannelLifeCast",
		"ChannelLifeEffect",
		"ChannelLifeStop",
		"CancelCast",
		"CharmCast",
		"CharmSuccess",
		"CharmFailure",
		"ConfuseCast",
		"ConfuseOn",
		"ConfuseOff",
		"CounterspellCast",
		"CurePoisonCast",
		"CurePoisonEffect",
		"DeathCast",
		"DeathOn",
		"DeathOff",
		"DeathTimer",
		"DeathRayCast",
		"DetonateGlyphCast",
		"DrainManaCast",
		"EarthquakeCast",
		"EnergyBoltCast",
		"EnergyBoltSustain",
		"FearCast",
		"FearOn",
		"FearOff",
		"ForceOfNatureCast",
		"ForceOfNatureReflect",
		"ForceOfNatureRelease",
		"GlyphCast",
		"GlyphDetonate",
		"FireballCast",
		"FireballExplode",
		"FirewalkCast",
		"FirewalkOn",
		"FirewalkOff",
		"FirewalkFlame",
		"FistCast",
		"FistHit",
		"ForceFieldCast",
		"FrostCast",
		"FrostOn",
		"FrostOff",
		"FumbleCast",
		"FumbleEffect",
		"GreaterHealCast",
		"GreaterHealEffect",
		"GreaterHealStop",
		"HasteCast",
		"HasteOn",
		"HasteOff",
		"InfravisionCast",
		"InfravisionOn",
		"InfraVisionOff",
		"InversionCast",
		"InvisibilityCast",
		"InvisibilityOn",
		"InvisibilityOff",
		"InvulnerabilityCast",
		"InvulnerabilityOn",
		"InvulnerabilityOff",
		"InvulnerableEffect",
		"LesserHealCast",
		"LesserHealEffect",
		"LightCast",
		"LightOn",
		"LightOff",
		"LightningCast",
		"LightningBolt",
		"LockCast",
		"ManaBombCast",
		"ManaBombEffect",
		"MarkCast",
		"MagicMissileCast",
		"MagicMissileDetonate",
		"MeteorCast",
		"MeteorShowerCast",
		"MeteorHit",
		"MoonglowCast",
		"MoonglowOn",
		"MoonglowOff",
		"NullifyCast",
		"NullifyOn",
		"NullifyOff",
		"PhantomCast",
		"PixieSwarmCast",
		"PixieHit",
		"PlasmaCast",
		"PlasmaSustain",
		"PoisonCast",
		"PoisonEffect",
		"ProtectionFromFireCast",
		"ProtectionFromFireOn",
		"ProtectionFromFireOff",
		"ProtectionFromFireEffect",
		"ProtectionFromElectricityCast",
		"ProtectionFromElectricityOn",
		"ProtectionFromElectricityOff",
		"ProtectionFromElectricityEffect",
		"ProtectionFromPoisonCast",
		"ProtectionFromPoisonOn",
		"ProtectionFromPoisonOff",
		"ProtectionFromPoisonEffect",
		"ProtectionFromMagicCast",
		"ProtectionFromMagicOn",
		"ProtectionFromMagicOff",
		"ProtectionFromMagicEffect",
		"PullCast",
		"PushCast",
		"ReflectiveShieldCast",
		"ReflectiveShieldOn",
		"ReflectiveShieldOff",
		"ReflectiveShieldEffect",
		"RegenerationOn",
		"RegenerationOff",
		"RunCast",
		"RunOn",
		"RunOff",
		"ShieldCast",
		"ShieldOn",
		"ShieldOff",
		"ShieldRepelled",
		"ShockCast",
		"ShockOn",
		"ShockOff",
		"Shocked",
		"SlowCast",
		"SlowOn",
		"SlowOff",
		"StunCast",
		"StunOn",
		"StunOff",
		"SummonCast",
		"SwapCast",
		"TagCast",
		"TagOn",
		"TagOff",
		"TeleportOut",
		"TeleportIn",
		"TeleportToMarkerCast",
		"TeleportToMarkerEffect",
		"TeleportPopCast",
		"TeleportToTargetCast",
		"TelekinesisCast",
		"TelekinesisOn",
		"TelekinesisOff",
		"ToxicCloudCast",
		"TriggerGlyphCast",
		"TurnUndeadCast",
		"TurnUndeadEffect",
		"VampirismCast",
		"VampirismOn",
		"VampirismOff",
		"DrainHealth",
		"VillainCast",
		"VillainOn",
		"VillainOff",
		"WallCast",
		"WallOn",
		"WallOff",
		"BerserkerChargeInvoke",
		"BerserkerCrash",
		"BerserkerChargeOn",
		"BerserkerChargeOff",
		"WarcryInvoke",
		"WarcryOn",
		"WarcryOff",
		"HarpoonInvoke",
		"HarpoonOn",
		"HarpoonOff",
		"TreadLightlyInvoke",
		"TreadLightlyOn",
		"TreadLightlyOff",
		"EyeOfTheWolfInvoke",
		"EyeOfTheWolfOn",
		"EyeOfTheWolfOff",
		"SpellPhonemeUp",
		"SpellPhonemeUpRight",
		"SpellPhonemeRight",
		"SpellPhonemeDownRight",
		"SpellPhonemeDown",
		"SpellPhonemeDownLeft",
		"SpellPhonemeLeft",
		"SpellPhonemeUpLeft",
		"FemaleSpellPhonemeUp",
		"FemaleSpellPhonemeUpRight",
		"FemaleSpellPhonemeRight",
		"FemaleSpellPhonemeDownRight",
		"FemaleSpellPhonemeDown",
		"FemaleSpellPhonemeDownLeft",
		"FemaleSpellPhonemeLeft",
		"FemaleSpellPhonemeUpLeft",
		"NPCSpellPhonemeUp",
		"NPCSpellPhonemeUpRight",
		"NPCSpellPhonemeRight",
		"NPCSpellPhonemeDownRight",
		"NPCSpellPhonemeDown",
		"NPCSpellPhonemeDownLeft",
		"NPCSpellPhonemeLeft",
		"NPCSpellPhonemeUpLeft",
		"NPCFemaleSpellPhonemeUp",
		"NPCFemaleSpellPhonemeUpRight",
		"NPCFemaleSpellPhonemeRight",
		"NPCFemaleSpellPhonemeDownRight",
		"NPCFemaleSpellPhonemeDown",
		"NPCFemaleSpellPhonemeDownLeft",
		"NPCFemaleSpellPhonemeLeft",
		"NPCFemaleSpellPhonemeUpLeft",
		"FireballWand",
		"SmallFireballWand",
		"FlareWand",
		"LightningWand",
		"DepletedWand",
		"Ricochet",
		"WeaponEffectFire",
		"WeaponEffectElectricity",
		"AwardSpell",
		"AwardGuide",
		"BlueFireDamage",
		"DrainMana",
		"ManaRecharge",
		"PermanentFizzle",
		"ManaEmpty",
		"Lock",
		"Unlock",
		"ElevEnable",
		"ElevDisable",
		"OpenWoodenDoor",
		"MoveWoodenDoor",
		"CloseWoodenDoor",
		"WoodenDoorLocked",
		"OpenGate",
		"MoveGate",
		"CloseGate",
		"GateLocked",
		"OpenWoodenGate",
		"CloseWoodenGate",
		"OpenHeavyWoodenDoor",
		"CloseHeavyWoodenDoor",
		"ElevStoneUp",
		"ElevStoneDown",
		"ElevWoodUp",
		"ElevWoodDown",
		"ElevMechUp",
		"ElevMechDown",
		"ElevLavaUp",
		"ElevLavaDown",
		"ElevGreenUp",
		"ElevGreenDown",
		"ElevLOTDUp",
		"ElevLOTDDown",
		"Gear1",
		"Gear2",
		"Gear3",
		"SmallRockMove",
		"MediumRockMove",
		"LargeRockMove",
		"BoulderMove",
		"WalkOnSnow",
		"WalkOnStone",
		"WalkOnDirt",
		"WalkOnWood",
		"WalkOnWater",
		"WalkOnMud",
		"RunOnSnow",
		"RunOnStone",
		"RunOnDirt",
		"RunOnWood",
		"RunOnWater",
		"RunOnMud",
		"PlayerFallThud",
		"BarrelMove",
		"BlackPowderBurn",
		"FireExtinguish",
		"PolypExplode",
		"PowderBarrelExplode",
		"BarrelBreak",
		"WaterBarrelBreak",
		"LOTDBarrelBreak",
		"WineCaskBreak",
		"BarrelStackBreak",
		"CoffinBreak",
		"WaspHiveBreak",
		"WorkstationBreak",
		"CrushLight",
		"CrushMedium",
		"CrushHard",
		"SentryRayHitWall",
		"SentryRayHit",
		"DeathRayKill",
		"TauntLaugh",
		"TauntShakeFist",
		"TauntPoint",
		"FlagPickup",
		"FlagDrop",
		"FlagRespawn",
		"FlagCapture",
		"TreasurePickup",
		"TreasureDrop",
		"GameOver",
		"ServerOptionsChange",
		"CamperAlarm",
		"PlayerEliminated",
		"CrownChange",
		"HumanMaleEatFood",
		"HumanMaleEatApple",
		"HumanMaleDrinkJug",
		"HumanMaleHurtLight",
		"HumanMaleHurtMedium",
		"HumanMaleHurtHeavy",
		"HumanMaleHurtPoison",
		"HumanMaleDie",
		"HumanMaleExertionLight",
		"HumanMaleExertionHeavy",
		"HumanFemaleEatFood",
		"HumanFemaleEatApple",
		"HumanFemaleDrinkJug",
		"HumanFemaleHurtLight",
		"HumanFemaleHurtMedium",
		"HumanFemaleHurtHeavy",
		"HumanFemaleHurtPoison",
		"HumanFemaleDie",
		"HumanFemaleExertionLight",
		"HumanFemaleExertionHeavy",
		"MonsterEatFood",
		"BatMove",
		"BatBite",
		"BatDie",
		"BatRecognize",
		"SmallSpiderMove",
		"SmallSpiderIdle",
		"SmallSpiderBite",
		"SmallSpiderDie",
		"SmallSpiderRecognize",
		"LargeSpiderMove",
		"LargeSpiderIdle",
		"LargeSpiderBite",
		"LargeSpiderDie",
		"LargeSpiderSpit",
		"LargeSpiderRecognize",
		"LargeSpiderHurt",
		"WebGooHit",
		"SkeletonMove",
		"SkeletonAttackInit",
		"SkeletonHitting",
		"SkeletonMissing",
		"SkeletonDie",
		"SkeletonRecognize",
		"SkeletonHurt",
		"SkeletonLordMove",
		"SkeletonLordAttackInit",
		"SkeletonLordHitting",
		"SkeletonLordMissing",
		"SkeletonLordDie",
		"SkeletonLordRecognize",
		"SkeletonLordHurt",
		"BomberRecognize",
		"BomberSummon",
		"BomberDie",
		"BomberMove",
		"DemonRecognize",
		"DemonTaunt",
		"DemonSpellInit",
		"DemonDie",
		"DemonMove",
		"DemonHurt",
		"GruntMove",
		"GruntTaunt",
		"GruntIdle",
		"GruntAttackInit",
		"GruntHitting",
		"GruntMissing",
		"GruntDie",
		"GruntBowTwang",
		"GruntRecognize",
		"GruntHurt",
		"OgreBruteListen",
		"OgreBruteTaunt",
		"OgreBruteIdle",
		"OgreBruteEngage",
		"OgreBruteRecognize",
		"OgreBruteMove",
		"OgreBruteHurt",
		"OgreBruteDie",
		"OgreBruteMeleeHit",
		"OgreBruteMeleeMiss",
		"OgreBruteAttackInit",
		"OgreWarlordListen",
		"OgreWarlordTaunt",
		"OgreWarlordIdle",
		"OgreWarlordEngage",
		"OgreWarlordRecognize",
		"OgreWarlordMove",
		"OgreWarlordHurt",
		"OgreWarlordDie",
		"OgreWarlordMeleeHit",
		"OgreWarlordMeleeMiss",
		"OgreWarlordAttackInit",
		"OgreWarlordThrow",
		"ScorpionRecognize",
		"ScorpionMove",
		"ScorpionIdle",
		"ScorpionAttackInit",
		"ScorpionStingHit",
		"ScorpionStingMiss",
		"ScorpionClawHit",
		"ScorpionClawMiss",
		"ScorpionHurt",
		"ScorpionDie",
		"LeechMove",
		"LeechIdle",
		"LeechAttackInit",
		"LeechHitting",
		"LeechMissing",
		"LeechDie",
		"LeechRecognize",
		"LeechHurt",
		"BearMove",
		"BearAttackInit",
		"BearHitting",
		"BearMissing",
		"BearDie",
		"BearRecognize",
		"BearHurt",
		"WolfMove",
		"WolfIdle",
		"WolfAttackInit",
		"WolfHitting",
		"WolfMissing",
		"WolfDie",
		"WolfRecognize",
		"WolfHurt",
		"WolfHowl",
		"WolfGrowl",
		"PlantSleep",
		"PlantAttackInit",
		"PlantHitting",
		"PlantMissing",
		"PlantDie",
		"PlantRecognize",
		"PlantHurt",
		"PlantGrowl",
		"MimicMove",
		"MimicIdle",
		"MimicAttackInit",
		"MimicHitting",
		"MimicMissing",
		"MimicDie",
		"MimicRecognize",
		"MimicHurt",
		"MimicMorph",
		"ZombieMove",
		"ZombieIdle",
		"ZombieAttackInit",
		"ZombieHitting",
		"ZombieMissing",
		"ZombieDie",
		"ZombieRecognize",
		"ZombieHurt",
		"ZombieGetUp",
		"VileZombieMove",
		"VileZombieIdle",
		"VileZombieAttackInit",
		"VileZombieHitting",
		"VileZombieMissing",
		"VileZombieDie",
		"VileZombieRecognize",
		"VileZombieHurt",
		"VileZombieGetUp",
		"ImpMove",
		"ImpShoot",
		"ImpSteal",
		"ImpDie",
		"ImpRecognize",
		"GolemMove",
		"GolemHitting",
		"GolemMissing",
		"GolemDie",
		"GolemHurt",
		"GolemRecognize",
		"MechGolemMove",
		"MechGolemAttackInit",
		"MechGolemHitting",
		"MechGolemMissing",
		"MechGolemDie",
		"MechGolemHurt",
		"MechGolemRecognize",
		"TowerRecognize",
		"TowerShoot",
		"SkullRecognize",
		"SkullShoot",
		"GhostMove",
		"GhostHitting",
		"GhostDie",
		"GhostHurt",
		"GhostRecognize",
		"WizardTalkable",
		"WizardMove",
		"WizardDie",
		"WizardHurt",
		"WizardRecognize",
		"WizardEngage",
		"WizardRetreat",
		"WaspMove",
		"WaspIdle",
		"WaspSting",
		"WaspDie",
		"WaspRecognize",
		"EmberDemonMove",
		"EmberDemonTaunt",
		"EmberDemonHitting",
		"EmberDemonMissing",
		"EmberDemonDie",
		"EmberDemonThrow",
		"EmberDemonRecognize",
		"EmberDemonHurt",
		"UrchinMove",
		"UrchinTaunt",
		"UrchinIdle",
		"UrchinDie",
		"UrchinThrow",
		"UrchinRecognize",
		"UrchinHurt",
		"UrchinFlee",
		"UrchinShamanMove",
		"UrchinShamanTaunt",
		"UrchinShamanIdle",
		"UrchinShamanDie",
		"UrchinShamanRecognize",
		"UrchinShamanHurt",
		"UrchinShamanFlee",
		"ArcherMove",
		"ArcherTaunt",
		"ArcherIdle",
		"ArcherDie",
		"ArcherMissileInit",
		"ArcherShoot",
		"ArcherRecognize",
		"ArcherHurt",
		"ArcherRetreat",
		"SwordsmanMove",
		"SwordsmanTaunt",
		"SwordsmanIdle",
		"SwordsmanAttackInit",
		"SwordsmanHitting",
		"SwordsmanMissing",
		"SwordsmanDie",
		"SwordsmanRecognize",
		"SwordsmanHurt",
		"SwordsmanRetreat",
		"BeholderMove",
		"BeholderIdle",
		"BeholderAttackInit",
		"BeholderDie",
		"BeholderRecognize",
		"BeholderHurt",
		"DryadMove",
		"DryadTaunt",
		"DryadDie",
		"DryadRecognize",
		"DryadHurt",
		"EvilCherubMove",
		"EvilCherubTaunt",
		"EvilCherubIdle",
		"EvilCherubMissileInit",
		"EvilCherubShoot",
		"EvilCherubDie",
		"EvilCherubRecognize",
		"EvilCherubHurt",
		"FishDie",
		"FrogDie",
		"FrogRecognize",
		"HecubahTaunt",
		"HecubahTalkable",
		"HecubahMove",
		"HecubahAttackInit",
		"HecubahDie",
		"HecubahRecognize",
		"HecubahHurt",
		"HecubahDieFrame0A",
		"HecubahDieFrame0B",
		"HecubahDieFrame98",
		"HecubahDieFrame194",
		"HecubahDieFrame283",
		"HecubahDieFrame439",
		"NecromancerTaunt",
		"NecromancerTalkable",
		"NecromancerMove",
		"NecromancerAttackInit",
		"NecromancerDie",
		"NecromancerRecognize",
		"NecromancerEngage",
		"NecromancerRetreat",
		"NecromancerHurt",
		"LichMove",
		"LichAttackInit",
		"LichDie",
		"LichRecognize",
		"LichHurt",
		"FlyingGolemMove",
		"FlyingGolemShoot",
		"FlyingGolemDie",
		"FlyingGolemRecognize",
		"FlyingGolemHurt",
		"NPCTalkable",
		"NPCIdle",
		"NPCDie",
		"NPCRecognize",
		"NPCRetreat",
		"NPCHurt",
		"MaidenIdle",
		"MaidenTalkable",
		"MaidenFlee",
		"MaidenDie",
		"MaidenHurt",
		"RatDie",
		"ShadeMove",
		"ShadeAttackInit",
		"ShadeDie",
		"ShadeRecognize",
		"ShadeHurt",
		"WeirdlingMove",
		"WillOWispMove",
		"WillOWispIdle",
		"WillOWispDie",
		"WillOWispHurt",
		"WillOWispRecognize",
		"WillOWispEngage",
		"TrollMove",
		"TrollIdle",
		"TrollDie",
		"TrollHurt",
		"TrollRecognize",
		"TrollAttackInit",
		"TrollFlatus",
		"MaleNPC1Talkable",
		"MaleNPC1Idle",
		"MaleNPC1Recognize",
		"MaleNPC1Engage",
		"MaleNPC1Retreat",
		"MaleNPC1Hurt",
		"MaleNPC1Die",
		"MaleNPC1AttackInit",
		"MaleNPC2Talkable",
		"MaleNPC2Idle",
		"MaleNPC2Recognize",
		"MaleNPC2Engage",
		"MaleNPC2Retreat",
		"MaleNPC2Hurt",
		"MaleNPC2Die",
		"MaleNPC2AttackInit",
		"Maiden1Talkable",
		"Maiden1Idle",
		"Maiden1Recognize",
		"Maiden1Retreat",
		"Maiden1Hurt",
		"Maiden1Die",
		"Maiden1AttackInit",
		"Maiden2Talkable",
		"Maiden2Idle",
		"Maiden2Recognize",
		"Maiden2Retreat",
		"Maiden2Hurt",
		"Maiden2Die",
		"Maiden2AttackInit",
		"HorvathTalkable",
		"HorvathEngage",
		"HorvathHurt",
		"HorvathDie",
		"Wizard1Talkable",
		"Wizard1Idle",
		"Wizard1Recognize",
		"Wizard1Engage",
		"Wizard1Retreat",
		"Wizard1Hurt",
		"Wizard1Die",
		"Wizard1AttackInit",
		"Wizard2Talkable",
		"Wizard2Idle",
		"Wizard2Recognize",
		"Wizard2Engage",
		"Wizard2Retreat",
		"Wizard2Hurt",
		"Wizard2Die",
		"Wizard2AttackInit",
		"FireKnight1Talkable",
		"FireKnight1Idle",
		"FireKnight1Recognize",
		"FireKnight1Engage",
		"FireKnight1Retreat",
		"FireKnight1Hurt",
		"FireKnight1Die",
		"FireKnight1AttackInit",
		"FireKnight2Talkable",
		"FireKnight2Idle",
		"FireKnight2Recognize",
		"FireKnight2Engage",
		"FireKnight2Retreat",
		"FireKnight2Hurt",
		"FireKnight2Die",
		"FireKnight2AttackInit",
		"Guard1Talkable",
		"Guard1Idle",
		"Guard1Recognize",
		"Guard1Engage",
		"Guard1Retreat",
		"Guard1Hurt",
		"Guard1Die",
		"Guard1AttackInit",
		"Guard2Talkable",
		"Guard2Idle",
		"Guard2Recognize",
		"Guard2Engage",
		"Guard2Retreat",
		"Guard2Hurt",
		"Guard2Die",
		"Guard2AttackInit",
		"WoundedNPCTalkable",
		"WoundedNPCIdle",
		"WoundedNPCRecognize",
		"WoundedNPCRetreat",
		"WoundedNPCHurt",
		"WoundedNPCDie",
		"WoundedNPCAttackInit",
		"HorrendousTalkable",
		"HorrendousRecognize",
		"HorrendousRetreat",
		"HorrendousHurt",
		"HorrendousDie",
		"HorrendousAttackInit",
		"SecretWallOpen",
		"SecretWallClose",
		"SecretWallEarthOpen",
		"SecretWallEarthClose",
		"SecretWallMetalOpen",
		"SecretWallMetalClose",
		"SecretWallStoneOpen",
		"SecretWallStoneClose",
		"SecretWallWoodOpen",
		"SecretWallWoodClose",
		"TriggerPressed",
		"TriggerReleased",
		"PotionUse",
		"PotionBreak",
		"RestoreHealth",
		"RestoreMana",
		"WallDestroyed",
		"WallDestroyedStone",
		"WallDestroyedWood",
		"WallDestroyedMetal",
		"ChestOpen",
		"CryptChestOpen",
		"SackChestOpen",
		"ChestLocked",
		"EggBreak",
		"Poisoned",
		"ButtonPress",
		"ButtonRelease",
		"LeverToggle",
		"SwitchToggle",
		"ChainPull",
		"ShortBellsUp",
		"LongBellsUp",
		"LongBellsDown",
		"BigBell",
		"MetallicBong",
		"Chime",
		"BigGong",
		"SmallGong",
		"MysticChant",
		"TripleChime",
		"Clank1",
		"Clank2",
		"Clank3",
		"MapOpen",
		"MapClose",
		"BookOpen",
		"BookClose",
		"PageTurn",
		"InventoryOpen",
		"InventoryClose",
		"InventoryPickup",
		"InventoryDrop",
		"SpellPickup",
		"SpellDrop",
		"SpellPopOffBook",
		"TrapEditorOpen",
		"TrapEditorClose",
		"ChangeSpellbar",
		"ExpandSpellbar",
		"CollapseSpellbar",
		"CreatureCageAppears",
		"CreatureCageHides",
		"ShopRepairItem",
		"MetalArmorPickup",
		"MetalArmorDrop",
		"MetalArmorBreak",
		"LeatherArmorPickup",
		"LeatherArmorDrop",
		"LeatherArmorBreak",
		"WoodenArmorPickup",
		"WoodenArmorDrop",
		"WoodenArmorBreak",
		"ClothArmorPickup",
		"ClothArmorDrop",
		"ClothArmorBreak",
		"ShoesPickup",
		"ShoesDrop",
		"MetalWeaponBreak",
		"WoodWeaponBreak",
		"KeyPickup",
		"KeyDrop",
		"AmuletPickup",
		"AmuletDrop",
		"TrapPickup",
		"TrapDrop",
		"BookPickup",
		"BookDrop",
		"ScrollPickup",
		"ScrollDrop",
		"WandPickup",
		"WandDrop",
		"PotionPickup",
		"PotionDrop",
		"MeatPickup",
		"MeatDrop",
		"ApplePickup",
		"AppleDrop",
		"ShroomPickup",
		"ShroomDrop",
		"SpectaclesPickup",
		"SpectaclesDrop",
		"MetalWeaponPickup",
		"MetalWeaponDrop",
		"WoodenWeaponPickup",
		"WoodenWeaponDrop",
		"BearTrapTriggered",
		"PoisonTrapTriggered",
		"StoneHitStone",
		"StoneHitEarth",
		"StoneHitWood",
		"StoneHitMetal",
		"StoneHitFlesh",
		"WoodHitStone",
		"WoodHitEarth",
		"WoodHitWood",
		"WoodHitMetal",
		"WoodHitFlesh",
		"MetalHitStone",
		"MetalHitEarth",
		"MetalHitWood",
		"MetalHitMetal",
		"MetalHitFlesh",
		"FleshHitStone",
		"FleshHitEarth",
		"FleshHitWood",
		"FleshHitMetal",
		"FleshHitFlesh",
		"DiamondHitStone",
		"DiamondHitEarth",
		"DiamondHitWood",
		"DiamondHitMetal",
		"DiamondHitFlesh",
		"HitStoneBreakable",
		"HitEarthBreakable",
		"HitWoodBreakable",
		"HitMetalBreakable",
		"HitMagicBreakable",
		"HitMetalShield",
		"PunchMissing",
		"LongswordMissing",
		"SwordMissing",
		"HammerMissing",
		"AxeMissing",
		"MaceMissing",
		"BowShoot",
		"CrossBowShoot",
		"BowEmpty",
		"CrossBowEmpty",
		"ArrowTrapShoot",
		"GreatSwordReflect",
		"ChakramThrow",
		"ChakramCatch",
		"ChakramFallToGround",
		"StaffBlock",
		"NextWeapon",
		"HeartBeat",
		"GenerateTick",
		"SummonClick",
		"SummonComplete",
		"SummonAbort",
		"ManaClick",
		"LevelUp",
		"JournalEntryAdd",
		"SecretFound",
		"EarthRumbleMajor",
		"EarthRumbleMinor",
		"ElectricalArc1",
		"FloorSpikesUp",
		"FloorSpikesDown",
		"SpikeBlockMove",
		"BoulderRoll",
		"ArcheryContestBegins",
		"HorrendousIsKilled",
		"StaffOblivionAchieve1",
		"StaffOblivionAchieve2",
		"StaffOblivionAchieve3",
		"StaffOblivionAchieve4",
		"FireGrate",
		"MechGolemPowerUp",
		"ShellSelect",
		"ShellClick",
		"ShellSlideIn",
		"ShellSlideOut",
		"ShellMouseBoom",
		"NoCanDo",
		"BallThrow",
		"BallGrab",
		"BallBounce",
		"BallHitGoal",
		"BirdAmbient1",
		"LoopAmbienceCastle",
		"LoopAmbienceCave",
		"LoopAmbienceForest",
		"LoopAmbienceInferno",
		"LoopAmbienceLOTD",
		"LoopAmbienceMine",
		"LoopAmbienceSwamp",
		"LoopAmbBird2",
		"LoopAmbBirdSwamp1",
		"LoopAmbBirdSwamp2",
		"LoopAmbBrook2",
		"LoopAmbBrook3",
		"LoopAmbCricket1",
		"LoopAmbCricket2",
		"LoopAmbCricket3",
		"LoopAmbCrow1",
		"LoopAmbCrow2",
		"LoopAmbCrowWL",
		"LoopAmbDog1",
		"LoopAmbDripCave1",
		"LoopAmbDripCave2",
		"LoopAmbDripRoom",
		"LoopAmbFrogSwamp1",
		"LoopAmbFrogSwamp2",
		"LoopAmbFrogSwamp3",
		"LoopAmbFrogSwamp4",
		"LoopAmbFrogSwamp5",
		"LoopAmbFliesSwamp",
		"LoopAmbRodentForest1",
		"LoopAmbRodentForest2",
		"LoopAmbCaveRumble",
		"LoopAmbSpiritsFOV",
		"LoopAmbSpiritsLOTD",
		"LoopAmbWindCave1",
		"LoopAmbWindCave2",
		"LoopAmbWindCave3",
		"LoopAmbWindLOTD",
		"LoopAmbWindWL1",
		"LoopAmbWindWL2",
		"LoopAmbHeartNox",
		"LoopAmbBird3",
		"LoopAmbBird4",
		"LoopAmbBird5",
		"LoopAmbBird6",
		"LoopAmbBird7",
		"LoopAmbBird8",
		"LoopAmbIxTemple",
		"LoopAmbLeaves",
		"LoopAmbHowls",
		"LoopAmbWeirdling",
		"LoopAmbSpikeWave",
		"LoopAmbBeachBirds",
		"LoopAmbBeachWaves",
		"LoopCampfire",
		"LoopTorch",
		"LoopBlueFire",
		"LoopWorkstation",
		"LoopWaterFountain",
		"LoopWindmill",
		"LoopOrrery",
		"LoopVandegraf",
		"LoopGearSmall",
		"LoopGearLarge",
		"LoopGearPulley",
		"LoopPolyp",
		"LoopSentryGlobe",
		"LoopSpikePillar",
		"HarpoonBroken",
		"HarpoonReel",
		"MonsterGeneratorDie",
		"MonsterGeneratorHurt",
		"MonsterGeneratorSpawn",
		"PlayerExit",
		"AwardLife",
		"SoulGateTouch",
		"QuestRespawn",
		"QuestFinalDeath",
		"QuestPlayerJoinGame",
		"QuestStatScreen",
		"QuestIntroScreen",
		"QuestPlayerExitGame",
		"QuestLockedChest",
		"LoopMonsterGenerator",
		"StoneDoorOpen",
		"StoneDoorClose",
		"DiamondDrop",
		"DiamondPickup",
		"RubyDrop",
		"RubyPickup",
		"EmeraldDrop",
		"EmeraldPickup",
		"EnableSharedKeyMode"	
	];
	
	interface JMap
	{
		type: string;
		objects: JMapObject[];
		tiles: JMapTile[];
		edges: JMapEdge[];
		walls: JMapWall[];
	}
	
	interface JXferTrigger
	{
		width: number;
		height: number;
		color: number[];
		shadow: number[];
	}
	
	interface JXferDoor
	{
		direction: number;
		key: number;
	}
	
	interface JMapObject
	{
		extent: number;
		type: number;
		x: number;
		y: number;
		xferDoor: JXferDoor;
		xferTrigger: JXferTrigger;
	}
	
	interface JMapEdge
	{
		tileid: number;
		tilevariation: number;
		edgeid: number;
		edgevariation: number;
	}
	
	interface JMapTile
	{
		x: number;
		y: number;
		id: number;
		variation: number;
		edges: JMapEdge[];
	}
	
	interface JMapWall
	{
		x: number;
		y: number;
		facing: number;
		id: number;
		variation: number;
		destructable: boolean;
		destructableId: number;
		secret: boolean;
		secretId: number;
		secretFlags: number;
		window: boolean;
	}
	
	interface JThingDb
	{
		things: JThing[];
		tiles: JTile[];
		edges: JTile[];
		walls: JWall[];
		audios: JAudio[];
		images: JImage[];
	}
	
	interface JImage
	{
		name: string;
		image?: number;
		anim: {
			type: string,
			delay: number,
			images: number[]
		};
	}
	
	interface JAudio
	{
		name: string;
		sounds: string[];
		volume: number;
	}
	
	interface JTile
	{
		id: number;
		rows: number;
		cols: number;
		frames: number;
		delay: number;
		images: number[];
	}
	
	interface JPlayerImage
	{
		name: string;
		delay: number;
		type: string;
		naked: number[];
		armor: { [id: number]: number[] };
		weapon: { [id: number]: number[] };
	}
	
	interface JThing
	{
		id: number;
		name: string;
		prettyname: string;
		x: number;
		y: number;
		z: number;
		height: number;
		width: number;
		cls: number;
		subclass: string;
		flags: number;
		update: string;
		draw: string;
		images: number[];
		frameTimer: number;
		animationType: string;
		playerImages?: { [stance: number]: JPlayerImage };
		monsterImages?: { [stance: number]: { name: string, delay: number, type: string, images: number[]}};
		stateImages?: { [state: number]: { name: string, delay: number, type: string, images: number[]}};
		monsterGeneratorImages?: { delay: number, type: string, images: number[] }[];
	}
	
	interface JWall
	{
		id: number;
		name: string;
		images: {x: number, y: number, image: number}[][][];
		flags: number;
	}
	
	interface JStrings
	{
		messages: {[id: string] : string};
	}
	
	interface JModifier
	{
		id: number;
		name: string;
		type: string;
		primarydesc: string;
		secondarydesc: string;
		identifydesc: string;
		color: number;
	}
	
	interface JArmor
	{
		name: string;
		desc: string;
		colors: number[];
		material: number;
		effectiveness: number;
		primary: number;
		secondary: number;
	}
	
	interface JWeapon
	{
		name: string;
		desc: string;
		colors: number[];
		material: number;
		effectiveness: number;
		primary: number;
		secondary: number;
	}
	
	interface JModifierDb
	{
		armors: JArmor[];
		weapons: JWeapon[];
		modifiers: JModifier[];
	}
	
	export interface IDrawable
	{
		x: number;
		y: number;
		ignore: boolean;
		sprite: PIXI.DisplayObject;
		visible: boolean;
		angle: number;
		d: number;
		stageNext: IDrawable;
		stagePrev: IDrawable;
		draw();
	}	
	
	export class Drawable implements IDrawable
	{
		x: number;
		y: number;
		z: number;
		sprite: PIXI.DisplayObject;
		ignore: boolean;
		visible: boolean;
		angle: number;
		d: number;
		stageNext: IDrawable;
		stagePrev: IDrawable;
		constructor()
		{
		}
		draw(): void
		{
		}
	}
	
	var _cachedSparkTextures: {[color: string]: PIXI.Texture} = {};
	function getSparkTexture(color: string)
	{
		if (_cachedSparkTextures[color])
			return _cachedSparkTextures[color];
		
		var buf = new PIXI.CanvasBuffer(20, 20);
		var grad = buf.context.createRadialGradient(10,10,10,10,10,0);
		grad.addColorStop(0, 'transparent');
		grad.addColorStop(1, color);
		buf.context.fillStyle = grad;
		buf.context.fillRect(0, 0, 20, 20);
		buf.context.fillStyle = 'white';
		buf.context.beginPath();
		buf.context.arc(10, 10, 1, 0, 2 * Math.PI);
		buf.context.fill();
		_cachedSparkTextures[color] = PIXI.Texture.fromCanvas(buf.canvas);
		return _cachedSparkTextures[color];
	}	
	
	var _cachedBubbleTextures: {[color: string]: PIXI.Texture} = {};
	function getBubbleTexture(color: string)
	{
		if (_cachedBubbleTextures[color])
			return _cachedBubbleTextures[color];
		
		var buf = new PIXI.CanvasBuffer(20, 20);
		var grad = buf.context.createRadialGradient(10,10,10,10,10,0);
		grad.addColorStop(0, 'transparent');
		grad.addColorStop(1, color);
		buf.context.fillStyle = grad;
		buf.context.fillRect(0, 0, 20, 20);
		_cachedBubbleTextures[color] = PIXI.Texture.fromCanvas(buf.canvas);
		return _cachedBubbleTextures[color];
	}
	
	function createBubble(color: SparkColor, frames: number, x?: number, y?: number, z?: number): BubbleDrawable
	{
		var clr: string;
		switch (color)
		{
			case SparkColor.Green:
				clr = '#20FF20';
				break;
			case SparkColor.Blue:
				clr = '#0000FF';
				break;
			case SparkColor.LightBlue:
				clr = '#64C8FF';
				break;
			case SparkColor.LightViolet:
				clr = '#FFC8FF';
				break;
			case SparkColor.Orange:
				clr = '#FFC832';
				break;
			case SparkColor.Red:
				clr = '#FF0000';
				break;
			case SparkColor.Violet:
				clr = '#FF64FF';
				break;
			case SparkColor.White:
				clr = '#FFFFFF';
				break;
			case SparkColor.Yellow:
				clr = '#FFFF00';
				break;
		}
		var o = new BubbleDrawable(clr, frames, x, y, z);
		theWorld.stage.addChild(o);
		theWorld.objectsArray.push(o);
		o.sprite.updateTransform();
		return o;
	}
	
	function createSpark(color: SparkColor, impulse: number, frames: number, x?: number, y?: number): SparkDrawable
	{
		var clr: string;
		switch (color)
		{
			case SparkColor.Green:
				clr = '#20FF20';
				break;
			case SparkColor.Blue:
				clr = '#0000FF';
				break;
			case SparkColor.LightBlue:
				clr = '#64C8FF';
				break;
			case SparkColor.LightViolet:
				clr = '#FFC8FF';
				break;
			case SparkColor.Orange:
				clr = '#FFC832';
				break;
			case SparkColor.Red:
				clr = '#FF8080';
				break;
			case SparkColor.Violet:
				clr = '#FF64FF';
				break;
			case SparkColor.White:
				clr = '#FFFFFF';
				break;
			case SparkColor.Yellow:
				clr = '#FFFF64';
				break;
		}
		var o = new SparkDrawable(clr, impulse, frames, x, y);
		theWorld.stage.addChild(o);
		theWorld.objectsArray.push(o);
		o.sprite.updateTransform();
		return o;
	}
	
	export class BubbleDrawable extends Drawable
	{
		frame: number;
		maxFrames: number;
		constructor(color: string, frames: number, x?: number, y?: number, z?: number)
		{
			super();
			var sprite = new PIXI.Sprite(getBubbleTexture(color));
			sprite.blendMode = PIXI.BLEND_MODES.ADD;
			sprite.anchor = new PIXI.Point(0.5,0.5);
			sprite.scale = new PIXI.Point(1.0, 1.0);
			this.sprite = sprite;
			this.ignore = false;
			this.visible = true;
			this.frame = 0;
			this.maxFrames = frames;
			this.x = x || 0;
			this.y = y || 0;
            this.z = z || 0;
		}
		draw()
		{
			this.frame++;
            this.sprite.x = Math.floor(this.x);
            this.sprite.y = Math.floor(this.y - this.z);
            if (this.frame >= this.maxFrames - 15)
                this.sprite.alpha = (15 - (this.frame - (this.maxFrames - 15))) / 20;
            if (this.frame >= this.maxFrames)
                this.visible = false;
			
			if (!this.visible)
			{
				theWorld.stage.removeChild(this);
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this), 1);
			}
		}
	}
	
	export class SparkDrawable extends Drawable
	{
		dx: number;
		dy: number;
		dz: number;
		frame: number;
		maxFrames: number;
		constructor(color: string, impulse: number, frames: number, x?: number, y?: number)
		{
			super();
			var sprite = new PIXI.Sprite(getSparkTexture(color));
			sprite.blendMode = PIXI.BLEND_MODES.ADD;
			sprite.anchor = new PIXI.Point(0.5,0.5);
			this.sprite = sprite;
			this.ignore = false;
			this.visible = true;
			this.frame = 0;
			this.maxFrames = frames;
			this.x = x;
			this.y = y;
            this.z = 0;
			
			var theta = Math.random() * Math.PI * 2;
			var phi = 0.1 * Math.PI + Math.random() * Math.PI * 0.4;
			phi = phi * phi;
			this.dx = impulse * Math.cos(theta) * Math.cos(phi);
			this.dy = impulse * Math.sin(theta) * Math.cos(phi);
			this.dz = impulse * Math.sin(phi);
		}
		draw()
		{
            this.z += this.dz;
			if (this.z <= 0)
				this.dz = -this.dz * 0.8;
			else
				this.dz -= 0.5;
			
			theWorld.stage.removeChild(this);
            this.frame++;
            this.x += this.dx;
            this.y += this.dy;
            this.sprite.x = Math.floor(this.x);
            this.sprite.y = Math.floor(this.y - this.z - 20);
            if (this.frame >= this.maxFrames - 15)
                this.sprite.alpha = (15 - (this.frame - (this.maxFrames - 15))) / 20;
            if (this.frame >= this.maxFrames)
                this.visible = false;
			
			if (this.visible)
			{
				theWorld.stage.addChild(this);
			}
			else
			{
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this), 1);
			}
		}
	}
		
	var _cachedChainLightningPattern: HTMLCanvasElement;
	export class ChainLightningDrawable extends Drawable
	{
		canvas: HTMLCanvasElement;
		context: CanvasRenderingContext2D;
		_sprite: PIXI.Sprite;
		extent1: number;
		extent2: number;
		lastDraw: number;
		constructor(extent1: number, extent2: number)
		{
			super();
			
			// these have to be set to something, but will be updated in draw()
			this.x = 0;
			this.y = 0;
			
			this.extent1 = extent1;
			this.extent2 = extent2;
			
			this.canvas = document.createElement('canvas');
			this.context = <CanvasRenderingContext2D>this.canvas.getContext('2d');
			this._sprite = new PIXI.Sprite(PIXI.Texture.fromCanvas(this.canvas));
			this._sprite.blendMode = PIXI.BLEND_MODES.LIGHTEN;
			this.sprite = this._sprite;
		}
		draw()
		{
			if (!_cachedChainLightningPattern)
			{
				_cachedChainLightningPattern = document.createElement('canvas');
			    _cachedChainLightningPattern.height = 40;
			    _cachedChainLightningPattern.width = 40;
			    var ctx = <CanvasRenderingContext2D>_cachedChainLightningPattern.getContext('2d');
			    var grad = ctx.createRadialGradient(20,20,20,20,20,0);
			    grad.addColorStop(0, 'black');
			    grad.addColorStop(1, 'rgba(175, 175, 255, 0.3)');
			    ctx.fillStyle = grad;
			    ctx.fillRect(0, 0, 40, 40);
			    ctx.strokeStyle = 'rgba(250, 250, 255, 1.0)';
			    ctx.lineWidth = 1;
			    ctx.beginPath();
			    ctx.moveTo(20, 20);
			    ctx.lineTo(21, 20);
			    ctx.stroke();
			}
			
			var o1 = theWorld.get(this.extent1), o2 = theWorld.get(this.extent2);
			if (!o1 || !o2)
				return;
			
			if (this.lastDraw == theWorld.currentFrame)
				return;
			this.lastDraw = theWorld.currentFrame;
			
			// update position of the sprite
			// we give ourselves a 20 pixel buffer for width of line
			var minx = Math.min(o1.x, o2.x);
			var miny = Math.min(o1.y, o2.y);
			var maxx = Math.max(o1.x, o2.x);
			var maxy = Math.max(o1.y, o2.y);
			var width = maxx - minx;
			var height = maxy - miny;
			Game.theWorld.stage.removeChild(this);
			this.x = Math.floor(minx + width / 2);
			this.y = Math.floor(miny + height / 2);
			Game.theWorld.stage.addChild(this);
			this.sprite.x = this.x;
			this.sprite.y = this.y - 20;
			this.canvas.width = width + 40;
			this.canvas.height = height + 40 + 40;
			this._sprite.anchor = new PIXI.Point(0.5, 0.5);
			this._sprite.texture.frame.x = 0;
			this._sprite.texture.frame.y = 0;
			this._sprite.texture.frame.width = width + 40;
			this._sprite.texture.frame.height = height + 40 + 40;
			this.sprite.updateTransform();
			
			// clear the canvas
			this.context.globalCompositeOperation = 'source-over';
			this.context.setTransform(1, 0, 0, 1, 0, 0);
			this.context.translate(-minx +20, -miny +20 +40);
			this.context.clearRect(minx - 20, miny - 20 - 40, maxx - minx + 20, maxy - miny + 40 + 40);
			
			// draw three lightning bolts
			for (var i = 0; i < 3; i++)
				this.drawLightning(o1.x, o1.y - 20, o2.x, o2.y - Math.random() * 40, i);
		}
		drawLightning(x1: number, y1: number, x2: number, y2: number, iteration: number)
		{
			// calculate the segments
			var segment = {p1: new PIXI.Point(x1, y1), p2: new PIXI.Point(x2, y2)};
			var segments: {p1: PIXI.Point, p2: PIXI.Point}[] = [segment];
			var offset = 40;
		    for (var g = 0; g < 4; g++)
		    {
		        var copy = segments.slice();
		        segments = [];
		        for (var i = 0; i < copy.length; i++)
		        {
		            var segment = copy[i];
		            var r = 1 - 2*Math.random();
		            var midpoint = new PIXI.Point(
						segment.p1.x + (segment.p2.x - segment.p1.x)/2,
						segment.p1.y + (segment.p2.y - segment.p1.y)/2);
		            var d = Math.sqrt((segment.p2.x - segment.p1.x) * (segment.p2.x - segment.p1.x) + (segment.p2.y - segment.p1.y) * (segment.p2.y - segment.p1.y));
		            midpoint.x += (segment.p2.y - segment.p1.y)/d * r * offset;
		            midpoint.y += -(segment.p2.x - segment.p1.x)/d * r * offset;
		            segments.push({p1: segment.p1, p2: midpoint});
		            segments.push({p1: midpoint, p2: segment.p2});
		        }
		        offset /= 2;
		    }
			
			// draw the segments
			this.context.globalCompositeOperation = 'lighten';
			if (iteration == 0)
			{
				for (var i = 0; i < segments.length; ++i)
				{
					var segment = segments[i];
					this.drawSegment(segment.p1.x, segment.p1.y, segment.p2.x, segment.p2.y);
				}
			}
			else
			{
				var color;
				if (iteration == 1)
					color = '#0000c8';
				else if (iteration == 2)
					color = '#000064';
				this.context.strokeStyle = color;
				this.context.lineWidth = 4;
				this.context.moveTo(segments[0].p1.x, segments[0].p1.y);
				for (var i = 0; i < segments.length; ++i)
					this.context.lineTo(segments[i].p2.x, segments[i].p2.y);
				this.context.stroke();
			}
		}
		drawSegment(x1: number, y1: number, x2: number, y2: number)
		{
			var angle = Math.atan2(y2 - y1, x2 - x1);
        	var d = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
			this.context.save();
	        this.context.translate(x1, y1);
	        this.context.rotate(angle);
	        this.context.drawImage(_cachedChainLightningPattern, 20, 10, 1, 20, 0, -10, d, 20);
	        this.context.drawImage(_cachedChainLightningPattern, 10, 10, 10, 20, -10, -10, 10, 20);
	        this.context.drawImage(_cachedChainLightningPattern, 21, 10, 10, 20, d, -10, 9, 20);
			this.context.restore();
		}
	}
		
	var _cachedEnergyBoltPattern: HTMLCanvasElement;
	export class EnergyBoltDrawable extends Drawable
	{
		canvas: HTMLCanvasElement;
		context: CanvasRenderingContext2D;
		_sprite: PIXI.Sprite;
		extent1: number;
		extent2: number;
		lastDraw: number;
		constructor(extent1: number, extent2: number)
		{
			super();
			
			// these have to be set to something, but will be updated in draw()
			this.x = 0;
			this.y = 0;
			
			this.extent1 = extent1;
			this.extent2 = extent2;
			
			this.canvas = document.createElement('canvas');
			this.context = <CanvasRenderingContext2D>this.canvas.getContext('2d');
			this._sprite = new PIXI.Sprite(PIXI.Texture.fromCanvas(this.canvas));
			this._sprite.blendMode = PIXI.BLEND_MODES.LIGHTEN;
			this.sprite = this._sprite;
		}
		draw()
		{
			if (!_cachedEnergyBoltPattern)
			{
				_cachedEnergyBoltPattern = document.createElement('canvas');
			    _cachedEnergyBoltPattern.height = 40;
			    _cachedEnergyBoltPattern.width = 40;
			    var ctx = <CanvasRenderingContext2D>_cachedEnergyBoltPattern.getContext('2d');
			    var grad = ctx.createRadialGradient(20,20,20,20,20,0);
			    grad.addColorStop(0, 'black');
			    grad.addColorStop(0.5, 'rgba(255, 255, 50, 0.3)');
			    ctx.fillStyle = grad;
			    ctx.fillRect(0, 0, 40, 40);
			    ctx.strokeStyle = 'rgba(255, 255, 120, 1.0)';
			    ctx.lineWidth = 4;
			    ctx.beginPath();
			    ctx.moveTo(20, 20);
			    ctx.lineTo(21, 20);
			    ctx.stroke();
			}
			
			var o1 = theWorld.get(this.extent1), o2 = theWorld.get(this.extent2);
			if (!o1 || !o2)
				return;
			
			if (this.lastDraw == theWorld.currentFrame)
				return;
			this.lastDraw = theWorld.currentFrame;
			
			// update position of the sprite
			// we give ourselves a 20 pixel buffer for width of line
			var minx = Math.min(o1.x, o2.x);
			var miny = Math.min(o1.y, o2.y);
			var maxx = Math.max(o1.x, o2.x);
			var maxy = Math.max(o1.y, o2.y);
			var width = maxx - minx;
			var height = maxy - miny;
			Game.theWorld.stage.removeChild(this);
			this.x = Math.floor(minx + width / 2);
			this.y = Math.floor(miny + height / 2);
			Game.theWorld.stage.addChild(this);
			this.sprite.x = this.x;
			this.sprite.y = this.y - 20;
			this.canvas.width = width + 40;
			this.canvas.height = height + 40 + 40;
			this._sprite.anchor = new PIXI.Point(0.5, 0.5);
			this._sprite.texture.frame.x = 0;
			this._sprite.texture.frame.y = 0;
			this._sprite.texture.frame.width = width + 40;
			this._sprite.texture.frame.height = height + 40 + 40;
			this.sprite.updateTransform();
			
			// clear the canvas
			this.context.globalCompositeOperation = 'source-over';
			this.context.setTransform(1, 0, 0, 1, 0, 0);
			this.context.translate(-minx +20, -miny +20 +40);
			this.context.clearRect(minx - 20, miny - 20 - 40, maxx - minx + 20, maxy - miny + 40 + 40);
			
			this.drawLightning(o1.x, o1.y - 20, o2.x, o2.y - Math.random() * 40, 0);
		}
		drawLightning(x1: number, y1: number, x2: number, y2: number, iteration: number)
		{
			// calculate the segments
			var segment = {p1: new PIXI.Point(x1, y1), p2: new PIXI.Point(x2, y2)};
			var segments: {p1: PIXI.Point, p2: PIXI.Point}[] = [segment];
			var offset = 40;
		    for (var g = 0; g < 3; g++)
		    {
		        var copy = segments.slice();
		        segments = [];
		        for (var i = 0; i < copy.length; i++)
		        {
		            var segment = copy[i];
		            var r = 1 - 2*Math.random();
		            var midpoint = new PIXI.Point(
						segment.p1.x + (segment.p2.x - segment.p1.x)/2,
						segment.p1.y + (segment.p2.y - segment.p1.y)/2);
		            var d = Math.sqrt((segment.p2.x - segment.p1.x) * (segment.p2.x - segment.p1.x) + (segment.p2.y - segment.p1.y) * (segment.p2.y - segment.p1.y));
		            midpoint.x += (segment.p2.y - segment.p1.y)/d * r * offset;
		            midpoint.y += -(segment.p2.x - segment.p1.x)/d * r * offset;
		            segments.push({p1: segment.p1, p2: midpoint});
		            segments.push({p1: midpoint, p2: segment.p2});
		        }
		        offset /= 2;
		    }
			
			// draw the segments
			this.context.globalCompositeOperation = 'lighten';
			if (iteration == 0)
			{
				for (var i = 0; i < segments.length; ++i)
				{
					var segment = segments[i];
					this.drawSegment(segment.p1.x, segment.p1.y, segment.p2.x, segment.p2.y);
				}
			}
			else
			{
				var color;
				if (iteration == 1)
					color = '#0000c8';
				else if (iteration == 2)
					color = '#000064';
				this.context.strokeStyle = color;
				this.context.lineWidth = 4;
				this.context.moveTo(segments[0].p1.x, segments[0].p1.y);
				for (var i = 0; i < segments.length; ++i)
					this.context.lineTo(segments[i].p2.x, segments[i].p2.y);
				this.context.stroke();
			}
		}
		drawSegment(x1: number, y1: number, x2: number, y2: number)
		{
			var angle = Math.atan2(y2 - y1, x2 - x1);
        	var d = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
			this.context.save();
	        this.context.translate(x1, y1);
	        this.context.rotate(angle);
	        this.context.drawImage(_cachedEnergyBoltPattern, 20, 10, 1, 20, 0, -10, d, 20);
	        //this.context.drawImage(_cachedEnergyBoltPattern, 10, 10, 10, 20, -10, -10, 10, 20);
	        //this.context.drawImage(_cachedEnergyBoltPattern, 21, 10, 10, 20, d, -10, 9, 20);
			this.context.restore();
		}
	}
	export class ArrowTailLinkDrawable extends Drawable
	{
		frame: number;
		numFrames: number;
		constructor(x1: number, y1: number, x2: number, y2: number, frames: number)
		{
			super();
			this.x = x1;
			this.y = y1;
			this.z = 32;
			this.numFrames = frames;
			this.frame = 0;
			
			var g = new PIXI.Graphics();
			g.lineColor = 0x808080;
			g.lineWidth = 1;
			g.moveTo(0, 0);
			g.lineTo(x2 - x1, y2 - y1);
			g.x = x1;
			g.y = y1 - this.z;
			this.sprite = g;
		}
		draw()
		{
			this.sprite.alpha = 1 - (this.frame++ / this.numFrames);
			if (this.sprite.alpha == 0)
			{
				theWorld.stage.removeChild(this);
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this), 1);
			}
		}
	}
	
	export class Chat
	{
		chatDiv: HTMLDivElement;
		chatInput: HTMLInputElement;
		constructor(chatDiv: HTMLDivElement, chatInput: HTMLInputElement)
		{
			this.chatDiv = chatDiv;
			this.chatInput = chatInput;
		
		}
		hasFocus(): boolean
		{
			return document.activeElement == this.chatInput;
		}
		loseFocus()
		{
			this.chatInput.blur();
		}
		takeFocus()
		{
			this.chatInput.focus();
		}
		clearInput()
		{
			this.chatInput.value = '';
		}
		getInput()
		{
			return this.chatInput.value;
		}
		trimLog()
		{
			while (this.chatDiv.children.length >= 50)
			{
				this.chatDiv.removeChild(this.chatDiv.firstChild);
			}		
		}
		addChat(name: string, msg: string, me=false)
		{
			var el = document.createElement('div');
			el.innerHTML = '<div class="chatmsg">&lt;<span class="chatname '+(me?'chatme':'')+'">'+_.escape(name)+'</span>&gt; <span class="chattext">'+_.escape(msg)+'</span></div>';
			this.chatDiv.appendChild(el);
			this.trimLog();
		}
		addMessage(msg: string)
		{
			var el = document.createElement('div');
			el.innerHTML = '<div class="chatsystemmsg">* '+_.escape(msg)+'</div>';
			this.chatDiv.appendChild(el);
			this.trimLog();
		}
		addKill(victim: string, attacker: string, mevictim=false, meattacker=false)
		{
			var el = document.createElement('div');
			el.innerHTML = '<div class="chatkill"><span class="chatname '+(mevictim?'chatme':'')+'">'+_.escape(victim)+'</span> was killed by <span class="chatname '+(meattacker?'chatme':'')+'">'+_.escape(attacker)+'</span></div>';
			this.chatDiv.appendChild(el);
			this.trimLog();
		}
	}
	
	export class Container extends PIXI.DisplayObject
	{
		childrenLists: IDrawable[];
		constructor()
		{
			super();
			
			this.childrenLists = new Array<IDrawable>(5880 / 8);
		}
		compareChildren(a: IDrawable, b: IDrawable)
		{
			return (a.y * 10000 + a.x) - (b.y * 10000 + b.x);
		}
		addChild(child: IDrawable)
		{
			if (child.sprite.parent)
				this.removeChild(child);
			var head = this.childrenLists[Math.floor(child.y / 8)];
			if (head == null || this.compareChildren(head, child) > 0)
			{
				child.stagePrev = null;
				child.stageNext = head;
				this.childrenLists[Math.floor(child.y / 8)] = child;
			}
			else
			{
				while (head.stageNext != null && this.compareChildren(head.stageNext, child) <= 0)
				{
					head = head.stageNext;
				}
				
				child.stageNext = head.stageNext;
				child.stagePrev = head;
				head.stageNext = child;
			}
			if (child.stageNext)
				child.stageNext.stagePrev = child;
			child.sprite.parent = <any>this;
		}
		removeChild(child: IDrawable)
		{
			if (child.stagePrev == null)
			{
				this.childrenLists[Math.floor(child.y / 8)] = child.stageNext;
			}
			else
			{
				child.stagePrev.stageNext = child.stageNext;
			}
			
			if (child.stageNext)
				child.stageNext.stagePrev = child.stagePrev;
			
			child.sprite.parent = null;
		}
		updateChild(child: IDrawable)
		{
			if (child.sprite.parent !== <any>this)
				return;
			this.removeChild(child);
			this.addChild(child);
		}
		updateTransform()
		{
			if (!this.visible)
				return;
			
			this.displayObjectUpdateTransform();
			for (var i = 0, j = this.childrenLists.length; i < j; ++i)
			{
				var child = this.childrenLists[i];
				while (child != null)
				{
					child.sprite.updateTransform();
					child = child.stageNext;
				}
			}
		}
		containerUpdateTransform()
		{
			this.updateTransform(); 
		}
		getBounds(matrix?: PIXI.Matrix) : PIXI.Rectangle
		{
			if(!this._currentBounds)
		    {
		
		        // TODO the bounds have already been calculated this render session so return what we have
		
		        var minX = Infinity;
		        var minY = Infinity;
		
		        var maxX = -Infinity;
		        var maxY = -Infinity;
		
		        var childBounds;
		        var childMaxX;
		        var childMaxY;
		
		        var childVisible = false;
		
				for (var i = 0, j = this.childrenLists.length; i < j; ++i)
				{
					var child = this.childrenLists[i];
					while (child != null)
					{
						if (!child.visible || !child.sprite.visible)
			            {
							child = child.stageNext;
			                continue;
			            }
			
			            childVisible = true;
			
			            childBounds = child.sprite.getBounds();
			
			            minX = minX < childBounds.x ? minX : childBounds.x;
			            minY = minY < childBounds.y ? minY : childBounds.y;
			
			            childMaxX = childBounds.width + childBounds.x;
			            childMaxY = childBounds.height + childBounds.y;
			
			            maxX = maxX > childMaxX ? maxX : childMaxX;
			            maxY = maxY > childMaxY ? maxY : childMaxY;
						child = child.stageNext;
					}
				}
		
		        if (!childVisible)
		        {
		            return PIXI.Rectangle.EMPTY;
		        }
		
		        var bounds = this._bounds;
		
		        bounds.x = minX;
		        bounds.y = minY;
		        bounds.width = maxX - minX;
		        bounds.height = maxY - minY;
		
		        this._currentBounds = bounds;
		    }
		
		    return this._currentBounds;
		}
		getLocalBounds() : PIXI.Rectangle
		{
		    var matrixCache = this.worldTransform;
		
		    this.worldTransform = PIXI.Matrix.IDENTITY;
		
		    for (var i = 0, j = this.childrenLists.length; i < j; ++i)
			{
				var child = this.childrenLists[i];
				while (child != null)
				{
					child.sprite.updateTransform();
					child = child.stageNext;
				}
			}
		
		    this.worldTransform = matrixCache;
		
		    this._currentBounds = null;
		
		    return this.getBounds( PIXI.Matrix.IDENTITY );
		}
		_renderWebGL(renderer: PIXI.WebGLRenderer)
		{
		}
		renderWebGL(renderer: PIXI.WebGLRenderer)
		{
		}
		renderCanvas(renderer: PIXI.CanvasRenderer)
		{
			if (!this.visible)
				return;
			// render the below children first
			for (var i = 0, j = this.childrenLists.length; i < j; ++i)
			{
				var child = this.childrenLists[i];
				while (child != null)
				{
					var next = child.stageNext;
					if (child instanceof Object && (child.getThing().flags & ThingFlags.BELOW))
					{
						child.draw();
						if (child.visible)
							child.sprite.renderCanvas(renderer);
					}
					child = next;
				}
			}
			
			// now render the rest
			for (var i = 0, j = this.childrenLists.length; i < j; ++i)
			{
				var child = this.childrenLists[i];
				while (child != null)
				{
					var next = child.stageNext;
					if (child instanceof Object && (child.getThing().flags & ThingFlags.BELOW))
					{
						child = next;
						continue;
					}
					
					child.draw();
					if (child.visible)
						child.sprite.renderCanvas(renderer);
					child = next;
				}
			}
		}
		destroy(destroyChildren: boolean = true)
		{
			super.destroy();
			
			if (destroyChildren)
			{
				for (var i = 0, j = this.childrenLists.length; i < j; ++i)
				{
					var child = this.childrenLists[i];
					while (child != null)
					{
						var next = child.stageNext;
						child.sprite.destroy();
						child = next;
					}
				}
			}
			for (var i = 0, j = this.childrenLists.length; i < j; ++i)
			{
				var child = this.childrenLists[i];
				while (child != null)
				{
					var next = child.stageNext;
					child.sprite.parent = null;
					child.stageNext = null;
					child.stagePrev = null;
					child = next;
				}
				this.childrenLists[i] = null;
			}
		}
		removeChildren()
		{
			for (var i = 0, j = this.childrenLists.length; i < j; ++i)
			{
				var child = this.childrenLists[i];
				while (child != null)
				{
					var next = child.stageNext;
					child.sprite.parent = null;
					child.stageNext = null;
					child.stagePrev = null;
					child = next;
				}
				this.childrenLists[i] = null;
			}
		}
		findObjects(loc: {x: number, y: number}, radius: number = 30): Object[]
		{
			var radius2 = radius * radius;
			var result: Object[] = new Array<Object>();
			
			for (var i = Math.floor(loc.y / 8); i <= Math.floor((loc.y + radius) / 8); i++)
			{
				for (var child = this.childrenLists[i]; child != null && child.y <= loc.y + radius; child = child.stageNext)
				{
					if (child instanceof Object)
					{
						if (Math.pow(loc.y - child.y, 2) + Math.pow(loc.x - child.x, 2) < radius2)
							result.push(child);
					}
				}
			}
			return result;
		}
	}
	
	export class Effect
	{
		// return false if it should be removed
		draw(below: boolean, graphics: PIXI.Graphics): boolean
		{
			return true;
		}
	}
	
	export class ParticleEffect
	{
		color: number;
		duration: number;
		totalDuration: number;
		radius: number;
		x: number;
		y: number;
		constructor(color: number, x: number, y: number, duration: number, radius?: number)
		{
			this.color = color;
			this.totalDuration = duration;
			this.duration = duration;
			if (radius === null)
				this.radius = 3;
			else
				this.radius = radius;
			this.x = x;
			this.y = y;
		}
		draw(graphics: PIXI.Graphics): boolean
		{
			if (this.duration <= 0)
				return false;
			graphics.beginFill(this.color, this.duration / this.totalDuration)
				.drawCircle(this.x, this.y, this.radius)
				.endFill();
			this.duration--;
			return true;
		}
	}
	
	export class SentryRayEffect
	{
		x1: number;
		y1: number;
		x2: number;
		y2: number;
		z: number;
		constructor(x1: number, y1: number, x2: number, y2: number)
		{
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.z = 20;
		}
		draw(below: boolean, graphics: PIXI.Graphics): boolean
		{
			if (below)
				graphics.lineStyle(2, 0xFF00FF, 1).moveTo(this.x1, this.y1 - this.z).lineTo(this.x2, this.y2 - this.z);
			return false;
		}
	}

	export class DurationEffect
	{
		type: number;
		extent1: number;
		extent2: number;
		object: IDrawable;
		
		constructor(type: number, extent1: number, extent2: number)
		{
			this.type = type;
			this.extent1 = extent1;
			this.extent2 = extent2;
			
			switch (this.type)
			{
			case 3:
				this.object = new ChainLightningDrawable(this.extent1, this.extent2);
				Game.theWorld.stage.addChild(this.object);
				Game.theWorld.objectsArray.push(this.object);
				break;
			case 4:
				this.object = new EnergyBoltDrawable(this.extent1, this.extent2);
				Game.theWorld.stage.addChild(this.object);
				Game.theWorld.objectsArray.push(this.object);
				break;
			}
		}
		destroy()
		{
			if (this.object)
			{
				theWorld.stage.removeChild(this.object);
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this.object), 1);
			}
		}
		draw(below: boolean, graphics: PIXI.Graphics): boolean
		{
			var o1 = theWorld.get(this.extent1),
				o2 = theWorld.get(this.extent2);
			
			if (below || !o1 || !o2)
				return false;

			switch (this.type)
			{
			case 7:
				graphics.lineColor = 0;
				graphics.lineWidth = 3;
				graphics.moveTo(o1.x, o1.y - 30);
				graphics.lineTo(o2.x, o2.y - 30);
				graphics.lineColor = 0x8B4513;
				graphics.lineWidth = 2;
				graphics.moveTo(o1.x, o1.y - 30);
				graphics.lineTo(o2.x, o2.y - 30);
				graphics.lineWidth = 0;
				break;
			}
			
			return true;
		}
	}
	
	export class Client
	{
		pendingActions: number;
		playerExtent: number;
		prevMouse: {x: number, y:number};
		prevWeapon: number;
		maxHealth: number;
		currentHealth: number;
		pings: {[value: number]: number};
		pingValue: number;
		summonEffects: { [id: number]: SummonEffectDrawable };
		rtts: number[];
		wsclient: WSClient.WSClient;
		constructor(options: IOptions)
		{
			this.pings = {};
			this.pingValue = 0;
			this.rtts = [];
			this.prevMouse = {x: 0, y: 0};
			this.prevWeapon = null;
			this.summonEffects = {};
			this.wsclient = new WSClient.WSClient(options.proxyServer, options.gameServer, options.gameServerPort,
				this, options.playerName, options.playerClass, options.gauntletMode);
		}
		sendping()
		{
			this.pingValue++;
			var value = this.pingValue;
			
			this.wsclient.send(new Uint8Array([0xAA, value & 0xff, (value >> 8) & 0xff, (value >> 16) & 0xff, (value >> 24) & 0xff]));
			this.pings[value] = Date.now();
		}
		getrtt()
		{
			var total = 0;
			for (var i = 0; i < this.rtts.length; i++)
				total += this.rtts[i];
			return Math.floor(total / this.rtts.length);
		}
		onimportantack(value: number)
		{
			if (this.pings[value])
			{
				var rtt = Date.now() - this.pings[value];
				delete this.pings[value];

				if (this.rtts.length == 10)
					this.rtts.splice(0, 1);
				this.rtts.push(rtt);
			}
		}
		onchat(extent: number, msg: string, x: number, y: number)
		{
			var name: string;
			var obj: Object = theWorld.get(extent);
			if (obj && obj instanceof PlayerObject)
				name = obj.player.name;
			else
				name = extent.toString(10);
			if (obj == thePlayer.object)
				theChat.addChat(name, msg, true);
			else
				theChat.addChat(name, msg);
		}
		onjoindata(extent: number)
		{
			console.log("Joined server as " + extent.toString(10));
			this.playerExtent = extent;
			theWorld.getOrCreate(extent, 0x2c9);
		}
		onnewmap(mapname: string)
		{
			mapname = mapname.substring(0, mapname.length - 4);
			console.log("Map: " + mapname);
			
			this.prevWeapon = null;
			
			theWorld.loadMap(mapname);
		}
		onnewplayer(extent: number, cls: number, name: string, wolname: string, respawnArmor: number, respawnWeapon: number)
		{
			console.log("New player: " + name + " (" + wolname + ") = " + extent.toString(10));
			var plr: Player = new Player(extent, cls, name, wolname, respawnArmor, respawnWeapon);
			theWorld.addPlayer(plr);
			if (extent == this.playerExtent)
				thePlayer = plr;
			else
				theWorld.displayMessage(name + " has joined the game.");
		}
		onplayerquit(extent: number)
		{
			theWorld.displayMessage(theWorld.players[extent].name + " has left the game.");
			delete theWorld.players[extent];
			this.onobjectdestroy(extent);
		}
		onplayerdied(extent: number)
		{
			var o = <PlayerObject>theWorld.get(extent);
			if (thePlayer && extent == thePlayer.object.extent)
			{
				//this.prevWeapon = null;
				//thePlayer.equipment = [];
				//thePlayer.inventory = [];
			}
			else if (o.player)
			{
				o.armor = o.player.respawnArmor;
				o.weapon = o.player.respawnWeapon;
			}
		}
		onupdate(extent: number, type: number, x: number, y: number, other?: Uint8Array)
		{
			if (extent == 0 || type == 0)
				return;
				
			var obj = theWorld.getOrCreate(extent, type);
			obj.update(x, y, other);
		}
		onobjectdestroy(extent: number)
		{
			var obj = theWorld.objects[extent];
			if (obj && !theWorld.players[extent] && thePlayer.inventory.indexOf(extent) < 0 && thePlayer.equipment.indexOf(extent) < 0)
			{
				delete theWorld.objects[extent];
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(obj), 1);
				theWorld.stage.removeChild(obj);
			}
		}
		onclientstatus(extent: number, status: number)
		{
			var o = <PlayerObject>theWorld.getOrCreate(extent, 0x2c9);
			o.setstatus(status);
			if (o == thePlayer.object)
				theWorld.updateHealthBar(1.0 * this.currentHealth / this.maxHealth, (thePlayer.object.status & 0x400) != 0);
		}
		onobeliskcharge(extent: number, frame: number)
		{
			var o = theWorld.get(extent | 0x8000);
			if (!o) return;
			o.currentFrame = Math.floor((frame / 50.0) * 7); // max charge is 50, there are 8 frames
		}
		ondoorangle(extent: number, frame: number)
		{
			var o = theWorld.get(extent | 0x8000);
			if (!o) return;
			o.currentFrame = frame;
		}
		onreportxstatus(extent: number, status: number)
		{
			var o = <StateObject>theWorld.get(extent);
			o.state = status;
			o.currentFrame = 0;
			o.numFrames = null;
		}
		onreportmodifier(extent: number, effectiveness: number, material: number, primary: number, secondary: number)
		{
			var o = <WeaponArmorObject>theWorld.get(extent);
			if (o == null)
				return;
			
			if (material != 0xFF)
				o.material = theModifiers.modifiers[material];
			if (effectiveness != 0xFF)
				o.effectiveness = theModifiers.modifiers[effectiveness];
			if (primary != 0xFF)
				o.primaryenchant = theModifiers.modifiers[primary];
			if (secondary != 0xFF)
				o.secondaryenchant = theModifiers.modifiers[secondary];
			
			o.updateTexture();
		}
		oncharges(extent: number, _current: number, _total: number)
		{
			var o = <WeaponArmorObject>theWorld.get(extent);
			o.charges = {current: _current, total: _total};
			theWorld.updateWeapon();
		}
		onarmor(extent: number, bitmask: number, equip: boolean, mod?: {material: number, effectiveness: number, primary: number, secondary: number})
		{
			var o = <PlayerObject>theWorld.getOrCreate(extent & 0x7fff, 0x2c9);
			if (mod)
			{
				if (mod.material == 0xFF) mod.material = null;
				if (mod.effectiveness == 0xFF) mod.effectiveness = null;
				if (mod.primary == 0xFF) mod.primary = null;
				if (mod.secondary == 0xFF) mod.secondary = null;
			}
			o.equiparmor(bitmask, equip, mod);
			if (o == thePlayer.object)
				Game.theWorld.updateInventory();
		}
		onweapon(extent: number, bitmask: number, equip: boolean, mod?: {material: number, effectiveness: number, primary: number, secondary: number})
		{
			var o = <PlayerObject>theWorld.getOrCreate(extent & 0x7fff, 0x2c9);
			if (mod)
			{
				if (mod.material == 0xFF) mod.material = null;
				if (mod.effectiveness == 0xFF) mod.effectiveness = null;
				if (mod.primary == 0xFF) mod.primary = null;
				if (mod.secondary == 0xFF) mod.secondary = null;
			}
			o.equipweapon(bitmask, equip, mod);
			if (o == thePlayer.object)
				Game.theWorld.updateInventory();
		}
		onequip(extent: number)
		{
			thePlayer.equipment.push(extent);
			Game.theWorld.updateInventory();
			var o = <WeaponArmorObject>theWorld.get(extent), thing = o.getThing();
			var mod = {
				material: o.material == null ? null : o.material.id,
				effectiveness: o.effectiveness == null ? null : o.effectiveness.id,
				primary: o.primaryenchant == null ? null : o.primaryenchant.id,
				secondary: o.secondaryenchant == null ? null : o.secondaryenchant.id
			};
			if (thing.cls & ThingClass.WEAPON)
			{
				theWorld.updateWeapon();
				var bitmask = <number>_.findKey(PlayerWeaponToObjectName, (object: {name: string}) => object.name == thing.name);
				thePlayer.object.equipweapon(bitmask, true, mod);
			}
			else
			{
				var bitmask = <number>_.findKey(PlayerArmorToObjectName, (object: {name: string}) => object.name == thing.name);
				thePlayer.object.equiparmor(bitmask, true, mod);
			}
			if (extent == this.prevWeapon)
				this.prevWeapon = null;
		}
		ondequip(extent: number)
		{
			if (thePlayer.equipment.indexOf(extent) >= 0)
			{
				thePlayer.equipment.splice(thePlayer.equipment.indexOf(extent), 1);
				Game.theWorld.updateInventory();
			}
			var o = <WeaponArmorObject>theWorld.get(extent);
			if (!o)
				return;
				
			var thing = o.getThing();
			if (thing.cls & ThingClass.WEAPON)
			{
				theWorld.updateWeapon();
				var bitmask = <number>_.findKey(PlayerWeaponToObjectName, (object: {name: string}) => object.name == thing.name);
				thePlayer.object.equipweapon(bitmask, false);
			}
			else
			{
				var bitmask = <number>_.findKey(PlayerArmorToObjectName, (object: {name: string}) => object.name == thing.name);
				thePlayer.object.equiparmor(bitmask, false);
			}
		}
		onpickup(extent: number, type: number)
		{
			theWorld.getOrCreate(extent, type);
			if (theWorld.get(extent).getThing().cls & ThingClass.WEAPON)
				if (this.prevWeapon == null)
					this.prevWeapon = extent;
			thePlayer.inventory.push(extent);
			Game.theWorld.updateInventory();
		}
		ondrop(extent: number, type: number)
		{
			if (thePlayer.equipment.indexOf(extent) >= 0)
				this.ondequip(extent);
			if (this.prevWeapon == extent)
				this.prevWeapon = null;
			thePlayer.inventory.splice(thePlayer.inventory.indexOf(extent), 1);
			Game.theWorld.updateInventory();
		}
		onabilityreset(ability: number)
		{
			this.onabilitystate(ability, true);
		}
		onabilitystate(ability: number, state: boolean)
		{
			var slot = QuickBarSlots.indexOf(ability);
			if (state)
				theWorld.quickBarTimers[slot] = [0, 0];
			else
				theWorld.quickBarTimers[slot] = [AbilityDelays[ability], AbilityDelays[ability]];
		}
		onenchantment(extent: number, enchantment: number)
		{
			var monster: MonsterObject = <MonsterObject>theWorld.get(extent);
			if (monster)
				monster.enchantment = enchantment;
			if (thePlayer && monster == thePlayer.object)
				theWorld.updateEnchants();
		}
		onitemenchantment(enchantment: number)
		{
			if (thePlayer)
			{
				thePlayer.itemenchant = enchantment;
				theWorld.updateEnchants();
			}
		}
		onreportframe(extent: number, frame: number)
		{
			var o = theWorld.get(extent);
			o.currentFrame = frame;
		}
		onaudio(u1: number, id: number, u2: number)
		{
			var pan;
			if (u1 & 0x80)
				pan = -(0x100-u1);
			else
				pan = u1;
			var vol = u2 / 100;
			var aud = theThings.audio[id];
			var r = Math.min((Math.random() * (aud.sounds.length - 1)) | 0, (aud.sounds.length - 1));
			playaudio(aud.sounds[r], pan / 50, aud.volume * vol);
		}
		onaudioplayer(u1: number, id: number, u2: number)
		{
			var pan;
			if (u1 & 0x80)
				pan = -(0x100-u1);
			else
				pan = u1;
			var vol = u2 / 100;
			var aud = theThings.audio[id];
			var r = Math.min((Math.random() * (aud.sounds.length - 1)) | 0, (aud.sounds.length - 1));
			playaudio(aud.sounds[r], pan / 50, aud.volume * vol);
		}
		onwalldestroy(id: number)
		{
			var wall = theWorld.destructableWalls[id];
			if (wall)
			{
				// FIXME
				this.onremovewall((wall.x / 23) | 0, (wall.y / 23) | 0);
			}
		}
		onwallopen(id: number)
		{
			var wall = theWorld.secretWalls[id];
			if (wall)
			{
				wall.ignore = true;
				wall.sprite.visible = false;
			}
		}
		onwallclose(id: number)
		{
			var wall = theWorld.secretWalls[id];
			if (wall)
			{
				wall.ignore = false;
				wall.sprite.visible = true;
			}			
		}
		onaddwall(matId: number, facing: number, variation: number, x: number, y: number)
		{
			var wall = theWorld.wallsByLoc[x * 1000 + y];
			if (!wall)
			{
				var wallDesc: JMapWall = {
					'x': x,
					'y': y,
					'facing': facing,
					'id': matId,
					'variation': variation,
					'destructable': false,
					'destructableId': null,
					'secret': false,
					'secretId': null,
					'secretFlags': 0,
					'window': false
				};
				
				wall = new Wall(wallDesc);
				theWorld.stage.addChild(wall);
				theWorld.wallsArray.push(wall);
				theWorld.wallsByLoc[x * 1000 + y] = wall;
			}
			else
			{
				wall.facing = facing;
				wall.variation = variation;
				wall.wall = theThings.walls[matId];
				wall.updateSprite();
			}
		}
		onremovewall(x: number, y: number)
		{
			var wall = theWorld.wallsByLoc[x * 1000 + y];
			theWorld.stage.removeChild(wall);
			theWorld.wallsArray.splice(theWorld.wallsArray.indexOf(wall), 1);
			delete theWorld.wallsByLoc[x * 1000 + y];
		}
		onballstatus(status: number, carrier: number)
		{
			theWorld.ballStatus = {
				'status': status,
				'carrier': carrier || null
			};
			theWorld.updateBallStatus();
		}
		onflagstatus(status: number, team: number, u1: number, carrier: number)
		{
			if (!theWorld.flagStatus)
			{
				theWorld.flagStatus = {
					1: {'status': 0, 'carrier': null},
					2: {'status': 0, 'carrier': null},
				};
			}
			if (team == 0)
			{
				theWorld.updateFlagStatus();
				return;
			}
			if (theWorld.flagStatus[team].carrier && theWorld.players[theWorld.flagStatus[team].carrier])
				theWorld.players[theWorld.flagStatus[team].carrier].flag = null;
			if (carrier)
			{
				theWorld.players[carrier].flag = new WeaponArmorObject(0, theThings.fromName('Flag').id);
				theWorld.players[carrier].flag.material = _.find(<any>theModifiers.modifiers, (m: JModifier) => m.name == ('MaterialTeam'+TeamData[team].name));
				theWorld.players[carrier].flag.updateTexture();
			}
			theWorld.flagStatus[team] = {
				'status': status,
				'carrier': carrier || null
			};
			theWorld.updateFlagStatus();
		}
		onhealth(health: number, maxhealth?: number)
		{
			if (maxhealth !== undefined)
				this.maxHealth = maxhealth;
			this.currentHealth = health;
			
			theWorld.updateHealthBar(1.0 * this.currentHealth / this.maxHealth, (thePlayer.object.status & 0x400) != 0);
		}
		onhealthdelta(extent: number, delta: number)
		{
			var player: PlayerObject = <PlayerObject>theWorld.get(extent);
			player.addHealthDelta(delta);
		}
		onstamina(stamina: number)
		{
			thePlayer.updateStamina(stamina);
		}
		oninformkill(attacker1: number, attacker2: number, victim: number)
		{
			var oa1 = theWorld.get(attacker1),
				oa2 = theWorld.get(attacker2),
				ov = theWorld.get(victim);
			if (ov instanceof PlayerObject)
			{
				if (oa1 && oa1 instanceof PlayerObject)
					theChat.addKill(ov.player.name, oa1.player.name, ov.player == thePlayer, oa1.player == thePlayer);
				else
					theChat.addKill(ov.player.name, 'nasty thing', ov.player == thePlayer);
			}
		}
		oninformmsg(id: string, ...args: any[])
		{
			if (id == "use.c:HadAbility")
				return; // this gets spammed for some reason
			
			if (getstring(id))
			{
				var s = getstring(id);
				theWorld.displayMessage(vsprintf(s, args));
			}
			else
			{
				theWorld.displayMessage(id);
			}
		}
		onfxwhiteflash(x: number, y: number)
		{
			theWorld.flashEffect = true;
		}
		onfxjiggle(intensity: number)
		{
			theWorld.jiggleEffect = {intensity: intensity, frame: 0};
		}
		onfxshield(extent: number, id: number)
		{
			var o = <PlayerObject>theWorld.get(extent);
			if (o)
			{
				o.shield = {'id': id, 'frame': 0, 'delay': 0};
			}
		}
		onfxricochet(x: number, y: number)
		{
			for (var i = 0; i < 5; i++)
			{
				var o = createSpark(SparkColor.Blue, 1.5, 15, x, y);
			}
		}
		onfxbluesparks(x: number, y: number)
		{
			for (var i = 0; i < 30; i++)
			{
				var o = createSpark(SparkColor.Blue, 3.0, 45, x, y);
				o.dz *= o.dz;
			}
		}
		onfxyellowsparks(x: number, y: number)
		{
            for (var i = 0; i < 50; i++) {
                var o = createSpark(SparkColor.Yellow, 3.0, 45, x, y);
                o.dz *= o.dz;
            }
		}
		onfxcyansparks(x: number, y: number)
		{
			
		}
		onfxvioletsparks(x: number, y: number)
		{
			
		}
		onfxexplosion(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("FireBoom").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxlesserexplosion(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("MediumFireBoom").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxthinexplosion(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("ThinFireBoom").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxteleport(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("TeleportPoof").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxdamagepoof(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("DamagePoof").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxcounterspell(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("CounterspellBoom").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxsmokeblast(x: number, y: number)
		{
			var obj = new Object(0, theThings.fromName("Smoke").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
			var obj = new Object(0, theThings.fromName("Puff").id);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxsparkexplosion(x: number, y: number, intensity: number)
		{
			for (var i = 0; i < 80 * intensity; i++)
			{
				createSpark(SparkColor.Orange, 3.0 + 1.5 * intensity, 45, x, y);
			}
			
			var explosionType: number;
			if (intensity > 0.6666)
				explosionType = theThings.fromName("FireBoom").id;
			else
				explosionType = theThings.fromName("MediumFireBoom").id;
			
			var obj = new Object(0, explosionType);
			obj.update(x, y);
			theWorld.stage.addChild(obj);
			theWorld.objectsArray.push(obj);
		}
		onfxdeathray(x1: number, y1: number, x2: number, y2: number)
		{
			var dx = x1 - x2,
				dy = y1 - y2,
				d = Math.sqrt(dx*dx + dy*dy),
				angle = Math.atan2(dy, dx);
			for (var i = 0; i < d; i += 2)
			{
				var px = x2 + i * Math.cos(angle) + Math.floor(Math.random() * 20) - 10,
					py = y2 + i * Math.sin(angle) + Math.floor(Math.random() * 20) - 10;
				createSpark(SparkColor.Violet, 1.0, 25, px, py);
			}
		}
		onfxsentryray(x1: number, y1: number, x2: number, y2: number)
		{
			var effect = new SentryRayEffect(x1, y1, x2, y2);
			theWorld.addEffect(effect);
		}
		onfxduration(type: number, extent1: number, extent2: number)
		{
			if (type < 8)
			{
				theWorld.addEffect(new DurationEffect(type, extent1, extent2));
			}
			else
			{
				for (var i = 0; i < theWorld.effects.length; i++)
				{
					var effect = theWorld.effects[i];
					if (effect instanceof DurationEffect)
					{
						if (effect.extent1 == extent1 && effect.extent2 == extent2)
						{
							effect.destroy();
							theWorld.effects.splice(i, 1);
							break;
						}
					}
				}
			}
		}
		onfxdeltaz(extent: number, z: number, dz1: number, dz2: number)
		{
			var o = <FistMeteorObject> theWorld.get(extent);
			o.deltaz(z, -(0x100 - dz1) * 2, dz2 * 2);
		}
		onfxsummon(id: number, x: number, y: number, type: number, orientation: number, frames: number)
		{
			var effect = new SummonEffectDrawable(id, x, y, type, orientation, frames);
			this.summonEffects[id] = effect;
		}
		onfxsummoncancel(id: number)
		{
			var effect = this.summonEffects[id];
			if (effect)
			{
				effect.destroy();
			}
		}
		onreportz(extent: number, z: number)
		{
			var o = theWorld.get(extent);
			o.updateZ(z);
		}
		onforgetdrawables(timestamp: number)
		{
			for (var i = 0; i < theWorld.objectsArray.length; i++)
			{
				var obj = theWorld.objectsArray[i];
				
				if (obj instanceof Object)
				{
					if (obj.extent < 0x8000 && obj.lastUpdate && obj.lastUpdate.frame < timestamp && !(obj instanceof PlayerObject))
					{
						delete theWorld.objects[obj.extent];
						theWorld.objectsArray.splice(i, 1);
						theWorld.stage.removeChild(obj);
						i--;
					}
				}
				else
				{
					theWorld.objectsArray.splice(i, 1);
					theWorld.stage.removeChild(obj);
					i--;
				}
			}
		}
		onteamjoin(team: number, extent: number, type: number)
		{
			var o = theWorld.getOrCreate(extent, type);
			if (o instanceof PlayerObject)
				o.setTeam(team);
			theWorld.updateFlagStatus();
		}
		onunlockdoor(extent: number)
		{
			var o = <DoorObject>theWorld.get(extent | 0x8000);
			if (o)
			{
				o.unlocked = true;
			}
		}
		onfxgeneratordestroyed(x: number, y: number, u1: number)
		{
			this.onfxexplosion(x, y);
			for (var i = 0; i < 80 * 0.5; i++)
			{
				createSpark(SparkColor.Green, 3.0 + 1.5 * 0.5, 45, x, y);
			}
		}
		onfxgeneratordamaged(x: number, y: number)
		{
			for (var i = 0; i < 80 * 0.25; i++)
			{
				createSpark(SparkColor.Green, 3.0 + 1.5 * 0.25, 45, x, y);
			}
		}
		sendready()
		{
			this.wsclient.send(new Uint8Array([0xC0]), true);
		}
		sendquit()
		{
			this.wsclient.send(new Uint8Array([0xAE]), true);
			this.wsclient.send(new Uint8Array([0x0A]));
		}
		sendtext(msg: string)
		{
			var extent = thePlayer ? thePlayer.object.extent : 0;
			var x = thePlayer ? thePlayer.object.x : 0;
			var y = thePlayer ? thePlayer.object.y : 0;
			var flags = 0x04;
			var length = msg.length + 1;
			var buf = [0xA8, extent & 0xFF, extent >> 8, flags, x & 0xFF, x >> 8, y & 0xFF, y >> 8, length & 0xFF, length >> 8, 0];
			for (var i = 0; i < msg.length; i++)
			{
				buf.push(msg.charCodeAt(i) & 0xff);
				buf.push(msg.charCodeAt(i) >> 8);
			}
			buf.push(0);
			buf.push(0);
			
			this.wsclient.send(new Uint8Array(buf));
		}
		sendability(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x7A, id]));
		}
		senduse(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x74, id & 0xff, (id >> 8) & 0xff]));
		}
		sendequip(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x75, id & 0xff, (id >> 8) & 0xff]));
		}
		senddequip(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x76, id & 0xff, (id >> 8) & 0xff]));
		}
		sendpickup(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x73, id & 0xff, (id >> 8) & 0xff]));
		}
		sendcollide(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x7B, id & 0xff, (id >> 8) & 0xff]));
		}
		sendupdate(mouse: {x: number, y: number})
		{
			if (!thePlayer)
				return;
			
			var buffer = new Uint8Array(64);
			var idx = 0;
			
			// send current orientation
			var orientation = (Math.atan2(mouse.y - thePlayer.object.y, mouse.x - thePlayer.object.x) + Math.PI) / (2 * Math.PI);
			orientation = (Math.max(Math.min(Math.floor(orientation * 255), 255), 0) + 128) & 0xFF;
			buffer[idx++] = 0x3F;
			buffer[idx++] = 0x00; // placeholder
			buffer[idx++] = 0x01;
			buffer[idx++] = 0x00;
			buffer[idx++] = 0x00;
			buffer[idx++] = 0x00;
			buffer[idx++] = orientation;
			
			// send actions
			if (this.pendingActions & Action.Attack)
			{
				buffer[idx++] = 0x06;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
			}
			if (this.pendingActions & Action.Jump)
			{
				buffer[idx++] = 0x07;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
			}
			if (this.pendingActions & Action.Move)
			{
				buffer[idx++] = 0x02;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00; // ???
			}
			this.pendingActions = 0;
			
			// update length field
			buffer[1] = idx - 2;
				
			if (mouse.x != this.prevMouse.x || mouse.y != this.prevMouse.y)
			{
				buffer[idx++] = 0xAC;
				buffer[idx++] = mouse.x & 0xFF;
				buffer[idx++] = mouse.x >> 8;
				buffer[idx++] = mouse.y & 0xFF;
				buffer[idx++] = mouse.y >> 8;
				this.prevMouse = mouse;
			}
			
			this.wsclient.send(buffer.subarray(0, idx));
		}
		attack()
		{
			this.pendingActions |= Action.Attack;
		}
		jump()
		{
			this.pendingActions |= Action.Jump;
		}
		move()
		{
			this.pendingActions |= Action.Move;
		}
		swapweapon()
		{
			var cur = thePlayer.equipment.filter((extent) => (theWorld.get(extent).getThing().cls & ThingClass.WEAPON) != 0)[0];
			if (this.prevWeapon != null)
			{
				this.sendequip(this.prevWeapon);
			}
			else if (cur)
			{
				this.senddequip(cur);
			}
			if (cur)
				this.prevWeapon = cur;
			else
				this.prevWeapon = null;
			playaudiobyname('NextWeapon', 0, 1);
		}
	}
	
	export class Object implements IDrawable
	{
		extent: number;
		type: number;
		x: number;
		y: number;
		z: number;
		visible: boolean;
		ignore: boolean;
		angle: number;
		d: number;
		stageNext: IDrawable;
		stagePrev: IDrawable;
		dx: number;
		dy: number;
		prevUpdate: { frame: number, x: number, y: number };
		lastUpdate: { frame: number, x: number, y: number };
		orientation: number;
		currentFrame: number;
		sprite: PIXI.DisplayObject;
		_sprite: PIXI.Sprite;
		frameTimer: number;
		frameTimerReset: number;
		numFrames: number;
		
		constructor(extent: number, type: number)
		{
			this.ignore = false;
			this.x = 0;
			this.y = 0;
			this.z = 0;
			this.angle = 0;
			this.d = Infinity;
			this.visible = false;
			this.extent = extent;
			this.type = type;
			this.orientation = 0;
			this.currentFrame = 0;
			this.frameTimerReset = this.getThing().frameTimer * 2;
			this.frameTimer = this.frameTimerReset;
			
			this.sprite = this._sprite = new PIXI.Sprite();
			
			var thing = this.getThing();
			if (thing.flags & ThingFlags.EDIT_VISIBLE)
				this.sprite.visible = false;

			switch (thing.draw)
			{
				case 'AnimateDraw':
				case 'ConditionalAnimateDraw':
				case 'SlaveDraw':
				case 'DoorDraw':
				case 'WeaponAnimateDraw':
				case 'FlagDraw':
				case 'SummonEffectDraw':
					this.numFrames = this.getThing().images.length;
					break;
				case 'VectorAnimateDraw':
					this.numFrames = this.getThing().images.length / 8;
					break;
				default:
					this.numFrames = 1;
					break;
			}
			
			this.dx = 0;
			this.dy = 0;
			this.lastUpdate = null;
			this.prevUpdate = null;
		}
		draw()
		{
			var thing = this.getThing();
			
			if (thing.draw == 'AnimateDraw'
				|| thing.draw == 'ConditionalAnimateDraw' // FIXME
				|| thing.draw == 'VectorAnimateDraw'
				|| thing.draw == 'SummonEffectDraw'
				|| thing.draw == 'WeaponAnimateDraw')
			{
				this.frameTimer--;
				if (this.frameTimer <= 0)
				{
					this.currentFrame = this.currentFrame + 1;
					this.frameTimer = this.frameTimerReset;
					if (this.currentFrame == this.numFrames)
					{
						if (thing.animationType == 'OneShot')
						{
							// stay on the last frame
							this.currentFrame--;
						}
						else if (thing.animationType == 'OneShotRemove')
						{
							// remove this object
							this.currentFrame--;
							this.sprite.visible = false;
							if (this.extent)
								delete theWorld.objects[this.extent];
							theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this), 1);
							theWorld.stage.removeChild(this);
						}
						else if (thing.animationType == 'LoopAndFade')
						{
							// loop it
							this.currentFrame = 0;
						}
						else
						{
							// loop it
							this.currentFrame = 0;
						}
					}
				}
			}
			
			if (thing.animationType == 'LoopAndFade')
			{
				this.sprite.alpha -= 0.05;	
				if (this.sprite.alpha == 0)
				{
					// remove this object
					if (this.extent)
						delete theWorld.objects[this.extent];
					theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this), 1);
					theWorld.stage.removeChild(this);
				}
			}
			
			if (thing.draw == 'VectorAnimateDraw')
			{
				var numFrames = thing.images.length / 8;
				if (thing.images[this.orientation * numFrames + this.currentFrame])
					this._sprite.texture = getimage(thing.images[this.orientation * numFrames + this.currentFrame]);
			}
			else if (thing.draw == 'SlaveDraw' || 
				thing.draw == 'DoorDraw' || 
				thing.draw == 'AnimateDraw' ||
				thing.draw == 'ConditionalAnimateDraw' || 
				thing.draw == 'WeaponAnimateDraw' ||
				thing.draw == 'HarpoonDraw' ||
				thing.draw == 'ArrowDraw' ||
				thing.draw == 'WeakArrowDraw' ||
				thing.draw == 'SummonEffectDraw')
			{
				if (thing.images[this.currentFrame])
					this._sprite.texture = getimage(thing.images[this.currentFrame]);
			}
			else if (thing.draw == 'NoDraw')
			{
				this._sprite.visible = false;
			}
			else if (!this._sprite.texture.valid)
			{
				if (thing.images && thing.images.length > 0)
				{
					this._sprite.texture = getimage(thing.images[0]);
				}
				else
				{
					var g = new PIXI.Graphics();
					g.beginFill(0xFFFFFF);
					g.drawRect(0, 0, 20, 20);
					g.endFill();
					var text = new PIXI.Text(this.extent.toString(), {font: '10px serif', fill: 'black'});
					text.x = 0;
					text.y = 0;
					var sprite = new PIXI.Container();
					sprite.addChild(g);
					sprite.addChild(text);
					this._sprite.texture = sprite.generateTexture(theWorld.renderer, PIXI.SCALE_MODES.LINEAR, 1.0);				
					this.sprite.x = this.x - 10;
					this.sprite.y = this.y - 10;
				}
			}
		}
		getThing() : JThing
		{
			return theThings.objects[this.type];
		}
		updateZ(z: number): void
		{
			var thing = this.getThing();
			this.sprite.y -= z - this.z;
			this.z = z;
			
			if (this.z < 0)
			{
				if (this._sprite.mask === null)
				{
					this._sprite.mask = new PIXI.Graphics();
				}
				var g = <PIXI.Graphics>this._sprite.mask;
				g.parent = this._sprite;
				g.clear();
				g.beginFill(0).drawRect(0, 0, thing.width, thing.height + this.z - thing.z).endFill();
				g.updateTransform();
			}
			else if (this._sprite.mask)
			{
				var g = <PIXI.Graphics>this._sprite.mask;
				g.parent = null;
				this._sprite.mask = null;
			}
		}
		update(x: number, y: number, other?: Uint8Array) : void
		{
			this.updateLocation(x, y);
		}
		updateLocation(x: number, y: number): void
		{
			var thing = this.getThing();
			this.prevUpdate = this.lastUpdate;
			this.lastUpdate = {frame: theClient.wsclient.timestamp, x: x, y: y};
			
			theWorld.stage.removeChild(this);
			this.x = x;
			this.y = y;
			theWorld.stage.addChild(this);
			
			this.sprite.x = this.x - Math.floor(thing.width / 2);
			this.sprite.y = this.y - Math.floor(thing.height / 2) - (this.z + thing.z);
		}
		tooltip(): string
		{
			return getstring(this.getThing().prettyname);
		}
	}
	
	export class DeathBallObject extends Object
	{
		draw(): void
		{
			super.draw();
			
			// spawn some green sparks
			
		}
	}
	
	var _cachedPixieTexture: PIXI.Texture;
	export class PixieObject extends Object
	{
		constructor(extent: number, type: number)
		{
			super(extent, type);
			
			if (!_cachedPixieTexture)
			{
				var buf = new PIXI.CanvasBuffer(40, 40);
				var grad = buf.context.createRadialGradient(20,20,20,20,20,0);
				grad.addColorStop(0, 'transparent');
				grad.addColorStop(0.8, 'rgba(255,200,0,0.5)');
				buf.context.fillStyle = grad;
				buf.context.fillRect(0, 0, 40, 40);
				buf.context.strokeStyle = 'rgba(255, 255, 0, 1.0)';
				buf.context.lineWidth = 2;
				buf.context.beginPath();
				buf.context.moveTo(18, 20);
				buf.context.lineTo(26, 20);
				buf.context.stroke();
				_cachedPixieTexture = PIXI.Texture.fromCanvas(buf.canvas);
			}

			this._sprite.anchor.x = 0.5;
			this._sprite.anchor.y = 0.5;
			this._sprite.blendMode = PIXI.BLEND_MODES.ADD;
			this._sprite.texture = _cachedPixieTexture;
		}
		update(x: number, y: number, other?: Uint8Array) : void
		{
			theWorld.stage.removeChild(this);
			this.x = x;
			this.y = y + 10;
			theWorld.stage.addChild(this);
			
			this._sprite.x = this.x;
			this._sprite.y = this.y - 30;
		}
		draw(): void
		{
			if (this.prevUpdate && this.lastUpdate)
			{
				var dx = this.lastUpdate.x - this.prevUpdate.x;
				var dy = this.lastUpdate.y - this.prevUpdate.y;
				this._sprite.rotation = Math.atan2(dy, dx);
			}
		}
	}
	
	export class ProjectileObject extends Object
	{
		orientation: number;
		lastEffectX: number;
		lastEffectY: number;
		update(x: number, y: number, other?: Uint8Array) : void
		{
			super.update(x, y);
			this.orientation = (other[0] >> 4) & 0x7;
			if (other[0] & 0x80)
				this.currentFrame = other[1];
			
			switch (this.getThing().draw)
			{
			case 'ArrowDraw':
			case 'WeakArrowDraw':
				if (!this.lastEffectX)
					this.lastEffectX = this.x;
				if (!this.lastEffectY)
					this.lastEffectY = this.y;
				break;
			}
		}
		draw(): void
		{
			var thing = this.getThing();
			if (thing.draw == 'ArrowDraw' || thing.draw == 'WeakArrowDraw')
			{
				var d = (this.x - this.lastEffectX) * (this.x - this.lastEffectX) + (this.y - this.lastEffectY) * (this.y - this.lastEffectY);
				if (d > 200)
				{
					var link = new ArrowTailLinkDrawable(this.lastEffectX, this.lastEffectY, this.x, this.y, 10);
					Game.theWorld.stage.addChild(link);
					Game.theWorld.objectsArray.push(link);
					this.lastEffectX = this.x;
					this.lastEffectY = this.y;
				}
			}
			
			super.draw();
		}
	}
	
	export class FistMeteorObject extends Object
	{
		dz: number;
		dz2: number;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			this.dz = 0;
		}
		draw(): void
		{
			this.updateZ(Math.max(this.z + this.dz, 0));
			if (this.z == 0)
				this.dz = this.dz2;
			super.draw();
		}
		deltaz(z: number, dz1: number, dz2: number)
		{
			this.dz = dz1;
			this.dz2 = dz2;
			this.updateZ(z);
		}
	}	
	
	export class HarpoonObject extends Object
	{
		orientation: number;
		update(x: number, y: number, other?: Uint8Array) : void
		{
			super.update(x, y);
			this.orientation = (other[0] >> 4) & 0x7;
			this.currentFrame = other[1];
		}
	}
	
	export class DoorObject extends Object
	{
		lockSprite: PIXI.Sprite;
		unlocked: boolean;
		xfer: JXferDoor;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			this.unlocked = false;
			if (this.getThing().draw == 'DoorDraw')
			{
				var container = new PIXI.Container();
				container.addChild(this._sprite);
				this.lockSprite = new PIXI.Sprite();
				container.addChild(this.lockSprite);
				this.sprite = container;
			}
		}
		draw()
		{
			super.draw();
			if (this.getThing().draw == 'DoorDraw')
			{
				// TODO gold lock support
				if (!this.unlocked && this.xfer && this.xfer.key == 1)
				{
					var image;
					if (this.xfer.direction & 8)
						image = 135923; // DoorLockSilverSE
					else
						image = 135924;
					this.lockSprite.texture = getimage(image);
					this.lockSprite.visible = true;
				}
				else
					this.lockSprite.visible = false;
			}
		}
	}
	
	export class BoulderObject extends Object
	{
		prevAnimX: number;
		prevAnimY: number;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			
			this.prevAnimX = null;
			this.prevAnimY = null;
		}
		draw()
		{
			var thing = this.getThing();
			if (thing.draw != 'BoulderDraw')
			{
				super.draw();
				return;
			}
			
			if (this.prevAnimX === null)
				this.prevAnimX = this.x;
			if (this.prevAnimY === null)
				this.prevAnimY = this.y;
			
			var dx = this.x - this.prevAnimX,
				dy = this.y - this.prevAnimY;
			
			if (dx*dx + dy*dy >= 100)
			{
				var frame = this.currentFrame & 0xf, dir;
				if (dx <= 0 && dy > 0)
					dir = 0;
				else if (dx <= 0)
					dir = 0x10;
				else if (dy > 0)
					dir = 0x10;
				else
					dir = 0;
				if (dy > 0)
				{
					frame = (frame + 1) % 16;
				}
				else
				{
					frame = (16 + frame - 1) % 16;
				}
				this.currentFrame = dir | frame;
				this.prevAnimX = this.x;
				this.prevAnimY = this.y;
			}
			if (thing.images[this.currentFrame])
				this._sprite.texture = getimage(thing.images[this.currentFrame]);
		}
	}
	
	export class TriggerObject extends Object
	{
		xfer: JXferTrigger;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			if (this.getThing().draw == 'PressurePlateDraw')
			{
				this.sprite = new PIXI.Graphics();
			}
		}
		draw()
		{
			if (this.getThing().draw != 'PressurePlateDraw')
			{
				super.draw();
				return;
			}
						
			if (this.xfer)
			{
				var g = <PIXI.Graphics>this.sprite;
				var color = (this.xfer.color[0] << 16) | (this.xfer.color[1] << 8) | this.xfer.color[0];
				var shadow = (this.xfer.shadow[0] << 16) | (this.xfer.shadow[1] << 8) | this.xfer.shadow[0];
				var rot = Math.sqrt(2);
				var w = this.xfer.width*rot / 4, h = this.xfer.height*rot / 4;
				g.clear();
				g.lineStyle(1, shadow, 1).drawPolygon([new PIXI.Point(-w+h+1, -w-h+1), new PIXI.Point(w+h+1, w-h+1), new PIXI.Point(w-h+1, w+h+1), new PIXI.Point(-w-h+1, -w+h+1)]);
				g.lineStyle(1, color, 1).drawPolygon([new PIXI.Point(-w+h, -w-h), new PIXI.Point(w+h, w-h), new PIXI.Point(w-h, w+h), new PIXI.Point(-w-h, -w+h)]);
			}
		}
	}
	
	export class StateObject extends Object
	{
		state: number;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			this.state = 0;
			// state & 0x40 -> add a ShinySpot
		}
		draw(): void
		{
			var thing = this.getThing();
			var stat = thing.stateImages[this.state & 0xf];
			if (!stat) return;
			
			this.frameTimerReset = stat.delay * 2;
			if (!this.numFrames)
				this.frameTimer = this.frameTimerReset;
			this.numFrames = stat.images.length;
			this.frameTimer--;
			if (this.frameTimer <= 0)
			{
				this.currentFrame = this.currentFrame + 1;
				this.frameTimer = this.frameTimerReset;
				if (this.currentFrame == this.numFrames)
				{
					if (stat.type == 'OneShot')
					{
						// stay on the last frame
						this.currentFrame--;
					}
					else
					{
						// loop it
						this.currentFrame = 0;
					}
				}
			}
			this._sprite.texture = getimage(stat.images[this.currentFrame]);
		}
	}
	
	export class MonsterGeneratorObject extends StateObject
	{
		healthDeltas: PIXI.Text[];
		hologramSprite: PIXI.Sprite;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			this.healthDeltas = [];
			this.sprite = new PIXI.Container();
			(<PIXI.Container>this.sprite).addChild(this._sprite);
			this.hologramSprite = new PIXI.Sprite();
			(<PIXI.Container>this.sprite).addChild(this.hologramSprite);
		}
		addHealthDelta(delta: number)
		{
			var text = new PIXI.Text('', {font: 'bold 8pt monospace', fill: 'yellow', stroke: 'black', strokeThickness: 2});
			text.x = this.getThing().width / 2;
			text.y = this.getThing().height / 2 - 35;
			text.text = (-delta).toString();
			this.healthDeltas.push(text);
			(<PIXI.Container>this.sprite).addChild(text);
		}
		draw(): void
		{
			var thing = this.getThing();
			// update health texts
			for (var i = 0; i < this.healthDeltas.length; i++)
			{
				var text = this.healthDeltas[i];
				if (text.y <= -10)
				{
					(<PIXI.Container>this.sprite).removeChild(this.healthDeltas[i]);
					this.healthDeltas.splice(i, 1);
					i--;
				}
				else
				{
					text.y -= 2;
				}
			}
			
			var stat = thing.monsterGeneratorImages[this.state >> 8];
			if (!stat) return;
			
			this.frameTimerReset = stat.delay * 2;
			if (!this.numFrames)
				this.frameTimer = this.frameTimerReset;
			this.numFrames = stat.images.length;
			this.frameTimer--;
			if (this.frameTimer <= 0)
			{
				this.currentFrame = this.currentFrame + 1;
				this.frameTimer = this.frameTimerReset;
				if (this.currentFrame == this.numFrames)
				{
					if (stat.type == 'OneShot')
					{
						// stay on the last frame
						this.currentFrame--;
					}
					else
					{
						// loop it
						this.currentFrame = 0;
					}
				}
			}
			this._sprite.texture = getimage(stat.images[this.currentFrame]);
			
			stat = thing.monsterGeneratorImages[4];
			this.hologramSprite.texture = getimage(stat.images[((theWorld.currentFrame/2) | 0) % stat.images.length]);
		}
	}
	
	export class WeaponArmorObject extends Object
	{
		canvas: HTMLCanvasElement;
		ctx: CanvasRenderingContext2D;
		charges: {total: number, current: number};
		desc: JArmor | JWeapon;
		material: JModifier;
		effectiveness: JModifier;
		primaryenchant: JModifier;
		secondaryenchant: JModifier;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			this.canvas = document.createElement('canvas');
			this.ctx = <CanvasRenderingContext2D>this.canvas.getContext('2d');
			this.charges = null;
			this.material = null;
			this.effectiveness = null;
			this.primaryenchant = null;
			this.secondaryenchant = null;
			
			var name = this.getThing().name;
			if (theModifiers.armors[name])
				this.desc = theModifiers.armors[name];
			else
				this.desc = theModifiers.weapons[name];
			
			this.updateTexture();
		}
		tooltip(): string
		{
			var s = '';
			if (this.effectiveness)
				s += getstring(this.effectiveness.identifydesc) + ' ';
			if (this.material)
				s += getstring(this.material.identifydesc) + ' ';
			s += getstring(this.desc.desc);
			if (this.primaryenchant)
				s += ' ' + getstring(this.primaryenchant.primarydesc);
			if (this.secondaryenchant)
				s += ' ' + getstring(this.secondaryenchant.secondarydesc);
			return s;
		}
		draw(): void
		{
			var thing = this.getThing();
			//if (thing.draw != 'StaticDraw')
			//	console.warn('WeaponArmorObject with weird draw', thing.draw);
			if (thing.draw == 'FlagDraw')
			{
				this.frameTimer--;
				if (this.frameTimer <= 0)
				{
					this.currentFrame++;
					this.frameTimer = this.frameTimerReset;
					if (this.currentFrame == this.numFrames)
					{
						// loop
						this.currentFrame = 0;
					}
				}
				this.updateTexture();
			}
		}
		updateTexture()
		{
			var imageData = this.ctx.createImageData(128, 128);
			var colors = this.desc.colors.slice();
			if (this.effectiveness)
				colors[this.desc.effectiveness] = this.effectiveness.color;
			if (this.material)
				colors[this.desc.material] = this.material.color;
			if (this.primaryenchant)
				colors[this.desc.primary] = this.primaryenchant.color;
			if (this.secondaryenchant)
				colors[this.desc.secondary] = this.secondaryenchant.color;
			drawimage(imageData, this.getThing().images[this.currentFrame], colors);
			this.ctx.putImageData(imageData, 0, 0);
			this._sprite.texture = PIXI.Texture.fromCanvas(this.canvas);
		}
	}
	
	export class MonsterObject extends Object
	{
		stance: number;
		numFrames: number;
		delay: number;
		enchantment: number;
		shield: {id: number, frame: number, delay: number};
		canvas: HTMLCanvasElement;
		canvasContext: CanvasRenderingContext2D;
		healthDeltas: PIXI.Text[];
		constructor(extent: number, type: number)
		{
			super(extent, type);
			this.delay = 0;
			this.stance = 0;
			this.healthDeltas = [];
			
			var thing = this.getThing();
			this.canvas = document.createElement('canvas');
			this.canvas.width = thing.width;
			this.canvas.height = thing.height;
			this.canvasContext = <CanvasRenderingContext2D>this.canvas.getContext('2d');
			this._sprite.texture = PIXI.Texture.fromCanvas(this.canvas);
						
			this.sprite = new PIXI.Container();
			(<PIXI.Container>this.sprite).addChild(this._sprite);
		}
		addHealthDelta(delta: number)
		{
			var text = new PIXI.Text('', {font: 'bold 8pt monospace', fill: this == thePlayer.object ? 'red' : 'yellow', stroke: 'black', strokeThickness: 2});
			text.x = this.canvas.width / 2;
			text.y = this.canvas.height / 2 - 35;
			text.text = (-delta).toString();
			this.healthDeltas.push(text);
			(<PIXI.Container>this.sprite).addChild(text);
		}
		update(x: number, y: number, other?: Uint8Array)
		{
			this.updateLocation(x, y);
			
			var thing = this.getThing();
			var orientation = (other[0] >> 4) & 0x7;
			var stance = other[0] & 0xf;
			var frame = other[1];
			
			if (orientation != this.orientation || stance != this.stance)
			{
				this.orientation = orientation;
				this.stance = stance;
				if (thing.monsterImages[this.stance] !== undefined)
				{
					this.delay = thing.monsterImages[this.stance].delay + 1;
					this.numFrames = thing.monsterImages[this.stance].images.length / 8;
				}
			}
			
			if (other[0] & 0x80)
			{
				this.currentFrame = frame;
			}
		}
		draw(): void
		{			
			var thing = this.getThing();
			// update health texts
			for (var i = 0; i < this.healthDeltas.length; i++)
			{
				var text = this.healthDeltas[i];
				if (text.y <= -10)
				{
					(<PIXI.Container>this.sprite).removeChild(this.healthDeltas[i]);
					this.healthDeltas.splice(i, 1);
					i--;
				}
				else
				{
					text.y -= 2;
				}
			}
						
			// everything after this point can be ignored if player not visible to us
			if (!this.visible)
				return;
			
			if (thing.monsterImages[this.stance] === undefined)
			{
				this.sprite.visible = false;
				return;
			}
					
			if (this.numFrames != 0)
			{
				this.delay -= 1;
				if (this.delay == 0)
				{
					if (thing.monsterImages[this.stance].type == 'Loop')
						this.currentFrame = (this.currentFrame + 1) % this.numFrames;
					else if (this.currentFrame < this.numFrames - 1)
						this.currentFrame++;
					this.delay = thing.monsterImages[this.stance].delay + 1;
				}
			}
			
			var imageData = this.canvasContext.createImageData(thing.width, thing.height);
			drawimage(imageData, thing.monsterImages[this.stance].images[this.orientation * this.numFrames + this.currentFrame], [0, 0, 0, 0, 0, 0]);
			this.canvasContext.putImageData(imageData, 0, 0);
			this.sprite.visible = true;
		}
		hasenchant(enchant: number): boolean
		{
			return (this.enchantment & (1 << enchant)) != 0;
		}
	}
	
	var _cachedProtectFromFireTexture: PIXI.Texture;
	var _cachedProtectFromShockTexture: PIXI.Texture;
	var _cachedProtectFromPoisonTexture: PIXI.Texture;
	export class PlayerObject extends MonsterObject
	{
		player: Player;
		redraw: boolean;
		obs: boolean;
		status: number; // 0x0001 obs, 0x0400 poison
		loaded: boolean;
		armor: number;
		armorModifiers: {[id: number]: {material: number, effectiveness: number, primary: number, secondary: number}};
		weapon: number;
		weaponModifiers: {[id: number]: {material: number, effectiveness: number, primary: number, secondary: number}};
		nameText: PIXI.Text;
		constructor(extent: number, type: number)
		{
			super(extent, type);
			
			this.redraw = true;
			this.loaded = false;
			this.obs = false;
			this.status = 0;
			this.armor = 0;
			this.armorModifiers = {};
			this.weapon = 0;
			this.weaponModifiers = {};
			
			this.nameText = new PIXI.Text('', { font: '8pt monospace', fill: '#CCCCCC', dropShadow: true, dropShadowColor: 'black', dropShadowDistance: 1});
			(<PIXI.Container>this.sprite).addChild(this.nameText);
		}
		setName(name: string)
		{
			this.nameText.text = name;
			this.nameText.x = 64 - this.nameText.width/2;
			this.nameText.y = 10;
		}
		setTeam(team: number)
		{
			this.player.team = team;
			if (TeamData[team])
				this.nameText.style.fill = '#' + (0x1000000 + TeamData[team].color).toString(16).slice(1); // horrible javascript hacks
			else
				this.nameText.style.fill = 0xCCCCCC;
			this.nameText.dirty = true;
		}
		update(x: number, y: number, other?: Uint8Array) : void
		{
			this.updateLocation(x, y);
			
			var orientation = (other[0] >> 4) & 0x7;
			var stance;
			if (other[0] & 0x80)
			{
				stance = other[2];
			}
			else
			{
				stance = other[1];
			}
			
			if (orientation != this.orientation || stance != this.stance)
			{
				var thing = this.getThing();
				
				if (stance != this.stance)
					this.currentFrame = 0; // reset animation
					
				if (other[0] & 0x80)
					this.currentFrame = other[1];
				
				this.orientation = orientation;
				this.stance = stance;
				this.delay = thing.playerImages[this.stance].delay + 1;
				this.numFrames = (thing.playerImages[this.stance].naked.length) / 8;
				this.currentFrame %= this.numFrames;
				this.redraw = true;
			}
		}
		draw()
		{			
			var dx = 0, dy = 0;
			if (this.lastUpdate && this.prevUpdate)
			{
				dx = this.lastUpdate.x - this.prevUpdate.x;
				dy = this.lastUpdate.y - this.prevUpdate.y;
			}
			
			var thing = this.getThing();				
			var stance = this.stance;
			if (stance == PlayerStance.WALK && this.hasenchant(Enchant.ENCHANT_SNEAK))
				stance = PlayerStance.SNEAK;
			
			// update health texts
			for (var i = 0; i < this.healthDeltas.length; i++)
			{
				var text = this.healthDeltas[i];
				if (text.y <= -10)
				{
					(<PIXI.Container>this.sprite).removeChild(this.healthDeltas[i]);
					this.healthDeltas.splice(i, 1);
					i--;
				}
				else
				{
					text.y -= 2;
				}
			}
			
			// update shield animation
			if (this.shield)
			{
				if (this.shield.delay >= 1)
				{
					this.shield.delay = 0;
					this.shield.frame++;
				}
				this.shield.delay++;
			}
			
			// everything after this point can be ignored if player not visible to us
			if (!this.visible)
				return;
			
			if (this.obs)
			{
				if (this.player == thePlayer)
				{
					this.sprite.alpha = 0.4;
				}
				else
				{
					this.sprite.alpha = 0;
					return;
				}
			}
			else
			{
				this.sprite.alpha = 1.0;
			}
			
			if (this.numFrames != 0)
			{
				this.delay -= 1;
				if (this.delay == 0)
				{
					if (thing.playerImages[stance].type == 'Loop')
						this.currentFrame = (this.currentFrame + 1) % this.numFrames;
					else if (this.currentFrame < this.numFrames - 1)
						this.currentFrame++;
					this.redraw = true;
					this.delay = thing.playerImages[stance].delay + 1;
				}
			}
			if (this.player)
			{
				if (this.orientation >= 8 || this.type == 0 || !thing.playerImages[this.stance])
				{
					this.sprite.visible = false;
					return;
				}
				
				var color: number;
				var colors: number[] = this.player.colors.slice();
				var overrideColors = false;
				var visible = this == thePlayer.object || thePlayer.object.hasenchant(Enchant.ENCHANT_INFRAVISION);
				if (this.player.team && this.player.team == thePlayer.team)
					visible = true;
				var imageData = this.canvasContext.createImageData(128, 128);
				
				if (this.status & 0x400)
				{
					// poisoned
					colors[0] = 0xB0E67E;
					colors[5] = 0xB0E67E;
				}
				
				if (this.hasenchant(Enchant.ENCHANT_INVISIBLE))
				{
					this.nameText.alpha = 0.0;
					
					if (this.stance == PlayerStance.WALK)
						this.sprite.alpha = 0.3;
					else if (this.stance == PlayerStance.RUN)
						this.sprite.alpha = 0.6;
					else if (visible)
					{
						this.sprite.alpha = 0.8;
						overrideColors = true;
						color = 0x00C800;
						colors[0] = color;
						colors[1] = color;
						colors[2] = color;
						colors[3] = color;
						colors[4] = color;
						colors[5] = color;
					}
					else
						this.sprite.alpha = 0.0;
				}
				
				if (!this.hasenchant(Enchant.ENCHANT_INVISIBLE) || visible)
				{
					this.nameText.alpha = 1.0;
				}
				
				if (this.hasenchant(Enchant.ENCHANT_INVULNERABLE))
				{
					overrideColors = true;
					if (Math.floor(theWorld.currentFrame / 2) % 2)
						color = 0xFFFFFF;
					else
						color = 0x8AE4FF;
					colors[0] = color;
					colors[1] = color;
					colors[2] = color;
					colors[3] = color;
					colors[4] = color;
					colors[5] = color;
				}
				
				drawimage(imageData, thing.playerImages[stance].naked[this.orientation * this.numFrames + this.currentFrame], colors);
				
				for (var i = 0; i < 32; i++)
				{
					if ((1 << i) == PlayerArmor.MEDIEVAL_CLOAK) continue; // draw this last
					if ((this.armor & (1 << i)) && thing.playerImages[stance].armor[1 << i])
					{
						var mod = this.armorModifiers[1 << i];
						if (!overrideColors)
						{
							if (mod)
								getarmorcolors(colors, 1 << i, mod.material, mod.effectiveness, mod.primary, mod.secondary);
							else
								getarmorcolors(colors, 1 << i);
						}
						drawimage(imageData, thing.playerImages[stance].armor[1 << i][this.orientation * this.numFrames + this.currentFrame], colors);
					}
				}

				for (var i = 0; i < 32; i++)
				{
					if ((this.weapon & (1 << i)) && thing.playerImages[stance].weapon[1 << i])
					{
						var mod = this.weaponModifiers[1 << i];
						if (!overrideColors)
						{
							if (mod)
								getweaponcolors(colors, 1 << i, mod.material, mod.effectiveness, mod.primary, mod.secondary);
							else
								getweaponcolors(colors, 1 << i);
						}
						drawimage(imageData, thing.playerImages[stance].weapon[1 << i][this.orientation * this.numFrames + this.currentFrame], colors);
					}
				}
								
				if ((this.armor & PlayerArmor.MEDIEVAL_CLOAK) &&  thing.playerImages[stance].armor[PlayerArmor.MEDIEVAL_CLOAK])
				{
					var mod = this.armorModifiers[PlayerArmor.MEDIEVAL_CLOAK];
					if (!overrideColors)
					{
						if (mod)
							getarmorcolors(colors, PlayerArmor.MEDIEVAL_CLOAK, mod.material, mod.effectiveness, mod.primary, mod.secondary);
						else
							getarmorcolors(colors, PlayerArmor.MEDIEVAL_CLOAK);
					}
					drawimage(imageData, thing.playerImages[stance].armor[PlayerArmor.MEDIEVAL_CLOAK][this.orientation * this.numFrames + this.currentFrame], colors);
				}
				
				this.canvasContext.putImageData(imageData, 0, 0);
				this.sprite.visible = true;
				this.redraw = false;
			}
							
			if ((this.weapon & 1) && this.player.flag)
			{
				this.player.flag.draw();
				this.canvasContext.globalCompositeOperation = 'destination-over';
				this.canvasContext.drawImage(<HTMLCanvasElement>this.player.flag._sprite.texture.baseTexture.source, 15, -30);
				this.canvasContext.globalCompositeOperation = 'source-over';
			}
			
			// XXX should these be real drawables?
			// update enchantment effects
			if (this.hasenchant(Enchant.ENCHANT_INFRAVISION))
			{
				var n = Math.floor(Math.random() * 3);
				for (var i = 0; i < n; i++)
				{
					var bubble = createBubble(SparkColor.Green, 15, this.x + Math.random() * 15, this.y + Math.random() * 15, 50 + Math.random() * 10);
				}
			}
			if (this.hasenchant(Enchant.ENCHANT_VAMPIRISM))
			{
				for (var i = 0; i < 3; i++)
				{
					var bubble = createBubble(SparkColor.Violet, 15, this.x + (1 - 2*Math.random()) * 15, this.y + Math.random() * 15, Math.random() * 50);
				}
			}
			if (this.hasenchant(Enchant.ENCHANT_HASTED) && (dx || dy))
			{
				for (var i = 0; i < 2; i++)
				{
					var bubble = createBubble(SparkColor.Blue, 15, this.x + Math.random() * 15, this.y + 1 + Math.random() * 15);
					bubble = createBubble(SparkColor.White, 15, this.x + Math.random() * 15, this.y + 1 + Math.random() * 15);
				}
			}
			if (this.hasenchant(Enchant.ENCHANT_RUN))
			{
				for (var i = 0; i < 2; i++)
				{
					var bubble = createBubble(SparkColor.Red, 15, this.x + Math.random() * 15, this.y + 1 + Math.random() * 15);
					bubble = createBubble(SparkColor.Yellow, 15, this.x + Math.random() * 15, this.y + 1 + Math.random() * 15);
				}
			}
			if (this.hasenchant(Enchant.ENCHANT_SLOWED))
			{
				for (var i = 0; i < 1; i++)
				{
					var bubble = createBubble(SparkColor.Yellow, 15, this.x + Math.random() * 25, this.y + 5, Math.random() * 25);
				}	
			}
			if (this.hasenchant(Enchant.ENCHANT_SHOCK))
			{
				/* emit light blue sparks */
				for (var i = 0; i < 2; i++)
				{
					var spark = createSpark(SparkColor.LightBlue, 10, 15, this.x, this.y + 1);
					spark.dz *= 0.5;
				}
			}
			if (this.hasenchant(Enchant.ENCHANT_ANTI_MAGIC) || 
				this.hasenchant(Enchant.ENCHANT_CONFUSED) || 
				this.hasenchant(Enchant.ENCHANT_HELD) ||
				this.hasenchant(Enchant.ENCHANT_CHARMING))
			{
				var frame = Math.floor(theWorld.currentFrame / (BirdiesImages.delay + 1)) % BirdiesImages.image.length;
				var texture = getimage(BirdiesImages.image[frame]);
				this.canvasContext.drawImage(<HTMLImageElement> texture.baseTexture.source, 0, -32);
			}
			if (this.hasenchant(Enchant.ENCHANT_PROTECT_FROM_FIRE)) {
                if (!_cachedProtectFromFireTexture) {
                    var buf = new PIXI.CanvasBuffer(20, 20);
                    var grad = buf.context.createRadialGradient(10, 10, 10, 10, 10, 0);
                    grad.addColorStop(0, 'transparent');
                    grad.addColorStop(0.9, 'rgba(255,100, 100,0.7)');
                    buf.context.fillStyle = grad;
                    buf.context.fillRect(0, 0, 20, 20);
                    buf.context.strokeStyle = 'rgba(255, 100,100, 1.0)';
                    buf.context.lineWidth = 2;
                    buf.context.beginPath();
                    buf.context.moveTo(8, 10);
                    buf.context.lineTo(12, 10);
                    buf.context.stroke();
                    _cachedProtectFromFireTexture = PIXI.Texture.fromCanvas(buf.canvas);
                }
                var phi = ((Game.theWorld.currentFrame % 30) / 30) * Math.PI * 2 - Math.PI;
                var theta = Math.PI / 8;
                var rx = -30 * Math.cos(theta) * Math.cos(phi);
                var ry = 30 * Math.sin(theta) * Math.cos(phi);
                var rz = 30 * Math.sin(phi);
                this.canvasContext.drawImage(<HTMLCanvasElement>_cachedProtectFromFireTexture.baseTexture.source, rx + 54, ry + 54 - rz);
                var phi = ((Game.theWorld.currentFrame % 30) / 30) * Math.PI * 2 - Math.PI + Math.PI;
                var theta = Math.PI / 8;
                var rx = -30 * Math.cos(theta) * Math.cos(phi);
                var ry = 30 * Math.sin(theta) * Math.cos(phi);
                var rz = 30 * Math.sin(phi);
                this.canvasContext.drawImage(<HTMLCanvasElement>_cachedProtectFromFireTexture.baseTexture.source, rx + 54, ry + 54 - rz);
            }
            if (this.hasenchant(Enchant.ENCHANT_PROTECT_FROM_POISON)) {
                if (!_cachedProtectFromPoisonTexture) {
                    var buf = new PIXI.CanvasBuffer(20, 20);
                    var grad = buf.context.createRadialGradient(10, 10, 10, 10, 10, 0);
                    grad.addColorStop(0, 'transparent');
                    grad.addColorStop(0.9, 'rgba(100,255, 100,0.7)');
                    buf.context.fillStyle = grad;
                    buf.context.fillRect(0, 0, 20, 20);
                    buf.context.strokeStyle = 'rgba(100, 255,100, 1.0)';
                    buf.context.lineWidth = 2;
                    buf.context.beginPath();
                    buf.context.moveTo(8, 10);
                    buf.context.lineTo(12, 10);
                    buf.context.stroke();
                    _cachedProtectFromPoisonTexture = PIXI.Texture.fromCanvas(buf.canvas);
                }
                var phi = ((Game.theWorld.currentFrame % 30) / 30) * Math.PI * 2 - Math.PI / 2;
                var theta = 0.5 * Math.PI / 8;
                var rx = -30 * Math.cos(theta) * Math.cos(phi);
                var ry = 30 * Math.sin(theta) * Math.cos(phi);
                var rz = 30 * Math.sin(phi);
                this.canvasContext.drawImage(<HTMLCanvasElement>_cachedProtectFromPoisonTexture.baseTexture.source, rx + 54, ry + 54 - rz);
                var phi = ((Game.theWorld.currentFrame % 30) / 30) * Math.PI * 2 - Math.PI / 2 + Math.PI;
                var theta = 0.5 * Math.PI / 8;
                var rx = -30 * Math.cos(theta) * Math.cos(phi);
                var ry = 30 * Math.sin(theta) * Math.cos(phi);
                var rz = 30 * Math.sin(phi);
                this.canvasContext.drawImage(<HTMLCanvasElement>_cachedProtectFromPoisonTexture.baseTexture.source, rx + 54, ry + 54 - rz);
            }
            if (this.hasenchant(Enchant.ENCHANT_PROTECT_FROM_ELECTRICITY)) {
                if (!_cachedProtectFromShockTexture) {
                    var buf = new PIXI.CanvasBuffer(20, 20);
                    var grad = buf.context.createRadialGradient(10, 10, 10, 10, 10, 0);
                    grad.addColorStop(0, 'transparent');
                    grad.addColorStop(0.9, 'rgba(150, 150, 255,0.7)');
                    buf.context.fillStyle = grad;
                    buf.context.fillRect(0, 0, 20, 20);
                    buf.context.strokeStyle = 'rgba(200, 200, 255, 1.0)';
                    buf.context.lineWidth = 2;
                    buf.context.beginPath();
                    buf.context.moveTo(8, 10);
                    buf.context.lineTo(12, 10);
                    buf.context.stroke();
                    _cachedProtectFromShockTexture = PIXI.Texture.fromCanvas(buf.canvas);
                }
                var phi = ((Game.theWorld.currentFrame % 30) / 30) * Math.PI * 2 - Math.PI;
                var theta = 6 * Math.PI / 8;
                var rx = -30 * Math.cos(theta) * Math.cos(phi);
                var ry = 30 * Math.sin(theta) * Math.cos(phi);
                var rz = -30 * Math.sin(phi);
                this.canvasContext.drawImage(<HTMLCanvasElement>_cachedProtectFromShockTexture.baseTexture.source, rx + 54, ry + 54 - rz);
                var phi = ((Game.theWorld.currentFrame % 30) / 30) * Math.PI * 2 - Math.PI + Math.PI;
                var theta = 6 * Math.PI / 8;
                var rx = -30 * Math.cos(theta) * Math.cos(phi);
                var ry = 30 * Math.sin(theta) * Math.cos(phi);
                var rz = -30 * Math.sin(phi);
                this.canvasContext.drawImage(<HTMLCanvasElement>_cachedProtectFromShockTexture.baseTexture.source, rx + 54, ry + 54 - rz);
            }
			if (this.hasenchant(Enchant.ENCHANT_SHIELD))
			{
				var frame = Math.floor(theWorld.currentFrame / (SphericalShieldImages.delay + 1)) % SphericalShieldImages.image.length;
				var texture = getimage(SphericalShieldImages.image[frame]);
				this.canvasContext.drawImage(<HTMLImageElement> texture.baseTexture.source, 0, -2);
				
				if (this.shield)
				{
					var shield = theThings.fromName(SphericalShieldNames[this.shield.id]);
					if (shield && this.shield.frame < shield.images.length)
					{
						var texture = getimage(shield.images[this.shield.frame]);
						this.canvasContext.drawImage(<HTMLImageElement> texture.baseTexture.source, 0, 0);
					}
					else
						this.shield = null;
				}
			}
			if (this.hasenchant(Enchant.ENCHANT_REFLECTIVE_SHIELD))
			{
				var rshield = theThings.fromName(ReflectiveShield[this.orientation].name);
				var frame = Math.floor(theWorld.currentFrame / (rshield.frameTimer + 1)) % rshield.images.length;
				var texture = getimage(rshield.images[frame]);
				if (ReflectiveShield[this.orientation].y < 0)
					this.canvasContext.globalCompositeOperation = 'destination-over';
				this.canvasContext.drawImage(<HTMLImageElement> texture.baseTexture.source, ReflectiveShield[this.orientation].x, ReflectiveShield[this.orientation].y);
				this.canvasContext.globalCompositeOperation = 'source-over';
			}
		}
		equiparmor(bitmask: number, equip: boolean, mod?: {material: number, effectiveness: number, primary: number, secondary: number})
		{
			if (equip)
			{
				this.armor |= bitmask;
				if (mod)
					this.armorModifiers[bitmask] = mod;
				else
					this.armorModifiers[bitmask] = null;
			}
			else
				this.armor &= ~bitmask;
		}
		equipweapon(bitmask: number, equip: boolean, mod?: {material: number, effectiveness: number, primary: number, secondary: number})
		{
			if (equip)
			{
				this.weapon |= bitmask;
				if (mod)
					this.weaponModifiers[bitmask] = mod;
				else
					this.weaponModifiers[bitmask] = null;
			}
			else
				this.weapon &= ~bitmask;
		}
		setobs(mode: boolean)
		{
			this.loaded = true;
			if (this.obs != mode)
			{
				this.obs = mode;
				this.redraw = true;
			}
		}
		setstatus(status: number)
		{
			this.status = status;
			this.setobs((status & 1) != 0);
		}
	}
	
	export class SummonEffectDrawable extends Drawable
	{
		summonId: number;
		lifeFrame: number;
		numLifeFrames: number;
		monster: MonsterObject;
		effectObjects: Object[];
		constructor(id: number, x: number, y: number, type: number, orientation: number, frames: number)
		{
			super();
			
			this.summonId = id;
			this.x = x;
			this.y = y;
			this.visible = false;
			this.ignore = true;
			this.lifeFrame = 0;
			this.numLifeFrames = frames;
			this.sprite = new PIXI.Sprite();
			
			// we are a Drawable on the stage just so we can get called each frame
			theWorld.stage.addChild(this);
			
			this.monster = new MonsterObject(0, type);
			theWorld.stage.addChild(this.monster);
			theWorld.objectsArray.push(this.monster);
			orientation = Math.round((orientation / 256) * 8) % 8;
            orientation = [4, 7, 6, 5, 3, 0, 1, 2][orientation];
			this.monster.update(x, y, new Uint8Array([orientation << 4 | 8, 0]));
			this.monster.sprite.alpha = 0;
			
			var angle = 0;
			var dist = 40;
			this.effectObjects = [];
			for (var i = 0; i < 24; i++)
			{
				var o = new Object(0, theThings.fromName("SummonEffect").id);
				theWorld.stage.addChild(o);
				theWorld.objectsArray.push(o);
				o.update(x + Math.floor(Math.sin(angle) * dist), y + Math.floor(Math.cos(angle) * dist));
				o.currentFrame = i % 12;
				o.frameTimer = o.frameTimerReset = 1;
				angle -= Math.PI * 2 / 24;
				this.effectObjects.push(o);
			}
		}
		destroy()
		{
			delete theClient.summonEffects[this.summonId];
			theWorld.stage.removeChild(this);
			for (var i = 0; i < this.effectObjects.length; i++)
			{
				var o = this.effectObjects[i];
				theWorld.stage.removeChild(o);
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(o), 1);
			}
			theWorld.stage.removeChild(this.monster);
			theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(this.monster), 1);
		}
		draw()
		{
			if (this.lifeFrame == this.numLifeFrames)
			{
				this.destroy();
			}
			else
			{
				this.monster.sprite.alpha = this.lifeFrame / this.numLifeFrames;
				this.lifeFrame++;
			}
		}
	}
	
	export class Player
	{
		object: PlayerObject;
		cls: number;
		name: string;
		wolname: string;
		colors: number[];
		respawnArmor: number;
		respawnWeapon: number;
		inventory: number[];
		equipment: number[];
		stamina: number;
		itemenchant: number; // bitmask of ItemEnchant
		team: number;
		flag: WeaponArmorObject;
		constructor(extent: number, cls: number, name: string, wolname: string, respawnArmor: number, respawnWeapon: number)
		{
			this.object = <PlayerObject>theWorld.getOrCreate(extent, 0x2C9);
			this.object.player = this;
			this.object.setName(name);
			
			this.flag = null;
			this.team = 0;
			this.cls = cls;
			this.name = name;
			this.wolname = wolname;
			this.colors = [0x734D22, 0xDA9A6E, 0xDA9A6E, 0xDA9A6E, 0xDA9A6E, 0xDA9A6E];
			
			this.inventory = new Array<number>();
			this.equipment = new Array<number>();
			this.object.armor = this.respawnArmor = respawnArmor;
			this.object.weapon = this.respawnWeapon = respawnWeapon;
		}
		usePoison()
		{
			var extent = _.find(this.inventory, (extent) => theWorld.get(extent).type == 631);
			if (extent)
				theClient.senduse(extent);
		}
		useHealth()
		{
			for (var i = 0; i < HealthPotions.length; i++)
			{
				var type: number = theThings.fromName(HealthPotions[i]).id;
				var extent = _.find(this.inventory, (extent) => theWorld.get(extent).type == type);
				if (extent)
					theClient.senduse(extent);
			}
		}
		useMana()
		{
			var extent = _.find(this.inventory, (extent) => theWorld.get(extent).type == 638);
			if (extent)
				theClient.senduse(extent);
		}
		updateStamina(stamina?: number)
		{
			if (stamina == null)
			{
				if (this.stamina != 100)
				{
					this.stamina += 3;
					if (this.stamina > 100) this.stamina = 100;
					theWorld.updateStamina();
				}
			}
			else
			{
				this.stamina = stamina;
				theWorld.updateStamina();
			}
		}
	}
	
	export class ModifierDb
	{
		armors: { [name: string]: JArmor };
		weapons: { [name: string]: JWeapon };
		modifiers: { [id: number]: JModifier };
		constructor(db: JModifierDb)
		{
			this.armors = {};
			this.weapons = {};
			this.modifiers = {};
			db.weapons.forEach((armor) => {
				this.armors[armor.name] = armor;
			})
			db.armors.forEach((w) => {
				this.weapons[w.name] = w;
			})
			db.modifiers.forEach((m) => {
				this.modifiers[m.id] = m;
				
				if (TeamMaterials[m.name])
					m.color = TeamMaterials[m.name];
			})
		}
	}
	
	export class ThingDb
	{
		objects: { [id: number]: JThing };
		objectByName: { [name: string]: JThing };
		edges: { [id: number]: JTile };
		tiles: { [id: number]: JTile };
		walls: { [id: number]: JWall };
		audio: { [id: number]: JAudio };
		audioByName: { [name: string]: JAudio };
		images: { [name: string]: JImage };
		constructor(db: JThingDb)
		{
			this.objects = {};
			this.objectByName = {};
			this.edges = {};
			this.tiles = {};
			this.walls = {};
			this.audio = {};
			this.audioByName = {};
			this.images = {};
			
			db.things.forEach((thing: JThing) => {
				this.objects[thing.id] = thing;
				this.objectByName[thing.name] = thing;
			});
			db.tiles.forEach((tile: JTile) => {
				this.tiles[tile.id] = tile;
			});
			db.edges.forEach((tile: JTile) => {
				this.edges[tile.id] = tile;
			});
			db.walls.forEach((wall: JWall) => {
				this.walls[wall.id] = wall;
			});
			db.audios.forEach((audio: JAudio) => {
				var idx = SoundEffectNames.indexOf(audio.name);
				if (idx >= 0)
					this.audio[idx] = audio;
				this.audioByName[audio.name] = audio;
			});
			db.images.forEach((img: JImage) => {
				this.images[img.name] = img;
			});
		}
		fromName(name: string)
		{
			return this.objectByName[name] || null;
		}
	}
	
	export class Wall extends Drawable
	{
		_sprite: PIXI.Sprite;
		wall: JWall;
		variation: number;
		facing: number;
		window: boolean;
		ignore: boolean;
		constructor(mapWall: JMapWall)
		{
			super();
			this.ignore = false;
			this.visible = false;
			this.wall = theThings.walls[mapWall.id];
			var facing: number = mapWall.facing;
			this.facing = facing;
			this.variation = mapWall.variation;
			this.window = mapWall.window;
			this.x = mapWall.x * 23;
			this.y = mapWall.y * 23;
			
			this._sprite = new PIXI.Sprite();
			this.sprite = this._sprite;
			this.updateSprite();
			
			if (mapWall.secretFlags & 8)
			{
				// initialize open
				this.ignore = true;
				this.sprite.visible = false;
			}
		}
		updateSprite()
		{
			var facing = this.facing;
			if (this.window)
			{
				if (facing == WallFacing.UP)
					facing = WallFacing.WINDOWUP;
				else if (facing == WallFacing.DOWN)
					facing = WallFacing.WINDOWDOWN; 
			}
			this._sprite.texture = getimage(this.wall.images[facing][this.variation][0].image);
			this.sprite.x = this.x - 52 + this.wall.images[facing][this.variation][0].x; // XXX appears to be off by 4 or so
			this.sprite.y = this.y - 72 - this.wall.images[facing][this.variation][0].y;
		}
		draw()
		{
			var angle = Math.atan2(Game.thePlayer.object.y - this.y, Game.thePlayer.object.x - this.x) / Math.PI;
			var bottom = false;
			if (this.facing == WallFacing.DOWN)
				bottom = angle >= -0.75 && angle < 0.25;
			else if (this.facing == WallFacing.UP)
				bottom = !(angle >= -0.25 && angle < 0.75);
			else if (this.facing == WallFacing.ARROWDOWN || this.facing == WallFacing.TNORTHWEST || this.facing == WallFacing.TNORTHEAST)
				bottom = angle >= -0.75 && angle < -0.25;
			else if (this.facing == WallFacing.ARROWUP || this.facing == WallFacing.TSOUTHWEST || this.facing == WallFacing.TSOUTHEAST)
			 	bottom = !(angle < 0.75 && angle > 0.25);
			else if (this.facing == WallFacing.ARROWRIGHT)
				bottom = (angle < 0.25 || angle >= 0.75);
			else if (this.facing == WallFacing.ARROWLEFT)
				bottom = !(angle >= 0.25 && angle <= 0.75);
			
			if (bottom && !(this.wall.flags & WallFlag.Short))
				this.sprite.alpha = 0.4;
			else
				this.sprite.alpha = 1.0;			
			
			var newfacing = null;
			if (this.facing == WallFacing.TNORTHWEST)
			{
				if (angle >= -0.75 && angle <= -0.25)
					newfacing = WallFacing.ARROWDOWN;
				else if (angle <= -0.75 || angle >= 0.75)
					newfacing = WallFacing.ARROWRIGHT;
				else
					newfacing = WallFacing.UP;
			}
			if (this.facing == WallFacing.TNORTHEAST)
			{
				if (angle >= -0.75 && angle <= -0.25)
					newfacing = WallFacing.ARROWDOWN;
				else if (angle >= -0.25 && angle <= 0.25)
					newfacing = WallFacing.ARROWLEFT;
				else
					newfacing = WallFacing.DOWN;
			}
			if (this.facing == WallFacing.TSOUTHEAST)
			{
				if (angle >= -0.25 && angle <= 0.25)
					newfacing = WallFacing.ARROWLEFT;
				else if (angle >= 0.25 && angle <= 0.75)
					newfacing = WallFacing.ARROWUP;
				else
					newfacing = WallFacing.UP;
			}
			if (this.facing == WallFacing.TSOUTHWEST)
			{
				if (angle >= 0.25 && angle <= 0.75)
					newfacing = WallFacing.ARROWUP;
				else if (angle <= -0.75 || angle >= 0.75)
					newfacing = WallFacing.ARROWRIGHT;
				else
					newfacing = WallFacing.DOWN;
			}
			if (newfacing !== null && (this.wall.flags & Game.WallFlag.Shadow))
				this._sprite.texture = getimage(this.wall.images[newfacing][this.variation][0].image);
		}
	}

	export class World
	{
		players: { [extent: number]: Player };
		objects: { [extent: number]: Object };
		effects: Effect[];
		objectsArray: IDrawable[];
		immobileObjects: { [extent: number]: Object };
		walls: { [id: number]: Wall };
		wallsArray: Wall[];
		wallsByLoc: { [loc: number]: Wall };
		secretWalls: { [id: number]: Wall };
		destructableWalls: { [id: number]: Wall };
		lastUpdate: number;
		currentFrame: number;
		targetingAbility: number;
		im: PIXI.interaction.InteractionManager;
		renderer: PIXI.CanvasRenderer;
		belowstage: PIXI.CanvasBuffer;
		stage: Container;
		effectstage: PIXI.Container;
		effectGraphic: PIXI.Graphics;
		allstage: PIXI.Container;
		guistage: PIXI.Container;
		infoText: PIXI.Text;
		pickupCircle: PIXI.Graphics;
		poisonBar: PIXI.Sprite;
		healthBar: PIXI.Graphics;
		potionUi: PIXI.Container;
		potionPoisonImage: PIXI.Sprite;
		potionPoisonCount: PIXI.Text;
		potionHealthImage: PIXI.Sprite;
		potionHealthCount: PIXI.Text;
		weaponChargesText: PIXI.Text;
		weaponImage: PIXI.Sprite;
		staminaCircle: PIXI.Graphics;
		inventoryScreenPage: number;
		inventoryScreen: PIXI.Container;
		inventoryScreenItems: PIXI.Container;
		inventoryScreenModel: PIXI.Container;
		inventoryScreenHandle: PIXI.Sprite;
		inventoryScreenUp: PIXI.Sprite;
		inventoryScreenDown: PIXI.Sprite;
		quickBarUi: PIXI.Container;
		quickBarImages: PIXI.Sprite[];
		quickBarOverlays: PIXI.Graphics[];
		quickBarTimers: number[][];
		enchantIconContainer: PIXI.Container;
		enchantIcons: {sprite: PIXI.Sprite, data: IEnchantData}[];
		teamBorderImage: PIXI.Sprite;
		ballImage: PIXI.Sprite;
		flagImages: { [id: number]: PIXI.Sprite };
		overlay: PIXI.Graphics;
		messages: { msg: string, timestamp: number, o: PIXI.Text }[];
		messagesContainer: PIXI.Container;
		cursor: string;
		cursorObject: Object;
		cursorSprite: PIXI.Sprite;
		toolTipContainer: PIXI.Container;
		toolTipBox: PIXI.Graphics;
		toolTipText: PIXI.Text;
		idleTimer: number;
		flashEffect: boolean;
		jiggleEffect: {intensity: number, frame: number};
		ballStatus: {status: number, carrier: number};
		flagStatus: {[id: number]: {status: number, carrier: number}};
		
		constructor()
		{
			this.lastUpdate = 0;
			this.objects = {};
			this.players = {};
			this.effects = [];
			this.objectsArray = [];
			this.wallsArray = [];
			this.currentFrame = 0;
			this.targetingAbility = null;
			this.idleTimer = Date.now();
		}
		addEffect(effect: Effect)
		{
			this.effects.push(effect);
		}
		addPlayer(player: Player)
		{
			this.players[player.object.extent] = player;
		}
		get(extent: number)
		{
			return this.objects[extent];
		}
		getOrCreate(extent: number, type: number)
		{
			if (this.objects[extent] === undefined)
			{
				var o = createobject(extent, type);				
				this.objects[extent] = o;
				this.objectsArray.push(o);
					
				this.stage.addChild(o);
				
				return o;
			}
				
			return this.objects[extent];
		}
		loadMap(mapname: string)
		{
			theAssets.load(mapname + ".json", (url, response) => {
				var map: JMap = JSON.parse(response);
				this.processMap(map);
				theClient.sendready();
				return true;
			});
		}
		processMap(map: JMap)
		{
			// remove old data except players
			this.walls = {};
			this.wallsArray = [];
			this.wallsByLoc = {};
			this.secretWalls = {};
			this.destructableWalls = {};
			this.objects = _.pick(this.objects, (value: Object) => value instanceof PlayerObject);
			this.objectsArray = this.objectsArray.filter((value) => value instanceof PlayerObject);
			this.stage.removeChildren();
			this.belowstage.context.clearRect(0, 0, 5880, 5880);
			this.objectsArray.forEach((o) => {
				this.stage.addChild(o);
			});
			if (thePlayer)
			{
				thePlayer.itemenchant = 0;
				this.updateEnchants();
				this.updateInventory();
				this.updateWeapon();
			}
			
			// load the new data
			map.tiles.forEach((mapTile) => {
				var buf = new PIXI.CanvasBuffer(46, 46);
				var data = buf.context.createImageData(46, 46);
				var tile = theThings.tiles[mapTile.id];
				drawtile(data.data, tile.images[mapTile.variation]);
				for (var i = 0; i < mapTile.edges.length; i++)
				{
					var e = mapTile.edges[i];
					drawedge(data.data, theThings.tiles[e.tileid].images[e.tilevariation], theThings.edges[e.edgeid].images[e.edgevariation]);
				}
				buf.context.putImageData(data, 0, 0);
				
				this.belowstage.context.drawImage(buf.canvas, mapTile.x * 23 - 11, mapTile.y * 23 + 11);
			});
			
			map.walls.forEach((mapWall, idx) => {
				if (theThings.walls[mapWall.id].images[mapWall.facing].length <= mapWall.variation) // FIXME ?
					mapWall.variation = 0;
				
				var wall = new Wall(mapWall);
				this.stage.addChild(wall);
				
				this.walls[idx] = wall;
				this.wallsArray.push(wall);
				this.wallsByLoc[mapWall.x * 1000 + mapWall.y] = wall;
				if (mapWall.secret)
					this.secretWalls[mapWall.secretId] = wall; 
				if (mapWall.destructable)
					this.destructableWalls[mapWall.destructableId] = wall;
			});
			
			map.objects.forEach((obj: JMapObject) => {
				if (theThings.objects[obj.type].cls & ThingClass.IMMOBILE)
				{
					// IMMOBILE extents overlap with other object extents
					var o = this.getOrCreate(obj.extent | 0x8000, obj.type);
					o.update(obj.x, obj.y);
					o.draw();
					
					if (obj.xferDoor)
						(<DoorObject>o).xfer = obj.xferDoor;
					if (obj.xferTrigger)
						(<TriggerObject>o).xfer = obj.xferTrigger;
				}
			});
		}
		createUi()
		{
			var createImage = (x: number, y: number, image: number) => {
				var sprite = new PIXI.Sprite(getimage(image));
				sprite.x = x;
				sprite.y = y;
				return sprite;
			};
			var createText = (msg: string, font: PIXI.TextStyle, x: number, y:number) => {
				var text = new PIXI.Text(msg, font);
				text.x = x;
				text.y = y;
				return text;
			};
			this.guistage.addChild(createImage(-1, this.renderer.height - 127, 14508));
			this.guistage.addChild(createImage(this.renderer.width - 92, this.renderer.height - 202, 14453));
			this.guistage.addChild(createImage(this.renderer.width - 92, this.renderer.height - 202, 14505 /* 14448 */));
			this.poisonBar = createImage(this.renderer.width - 92, this.renderer.height - 202, 14450);
			this.poisonBar.visible = false;
			this.guistage.addChild(this.poisonBar);
			
			this.healthBar = new PIXI.Graphics();
			this.healthBar.x = this.renderer.width - 53;
			this.healthBar.y = this.renderer.height - 169;
			this.healthBar.alpha = 0.7;
			this.guistage.addChild(this.healthBar);
			this.updateHealthBar(0, false);
			
			var potionPoisonHotkey = createText('Z', {font: '6pt monospace', fill: 'white'}, 936, 755);
			var potionHealthHotkey = createText('X', {font: '6pt monospace', fill: 'white'}, 963, 755);
			this.potionPoisonImage = new PIXI.Sprite();
			this.potionPoisonImage.x = 888;
			this.potionPoisonImage.y = 683;
			this.potionPoisonImage.visible = false;
			this.potionHealthImage = new PIXI.Sprite();
			this.potionHealthImage.x = 915;
			this.potionHealthImage.y = 683;
			this.potionHealthImage.visible = false;
			this.potionPoisonCount = new PIXI.Text('', {font: '6pt monospace', fill: 'white'});
			this.potionPoisonCount.x = 936;
			this.potionPoisonCount.y = 729;
			this.potionHealthCount = new PIXI.Text('', {font: '6pt monospace', fill: 'white'});
			this.potionHealthCount.x = 963;
			this.potionHealthCount.y = 729;
			this.potionUi = new PIXI.Container();
			this.potionUi.addChild(this.potionPoisonImage);
			this.potionUi.addChild(this.potionHealthImage);
			this.potionUi.addChild(this.potionPoisonCount);
			this.potionUi.addChild(this.potionHealthCount);
			this.potionUi.addChild(potionPoisonHotkey);
			this.potionUi.addChild(potionHealthHotkey);
			this.potionUi.cacheAsBitmap = true;
			this.guistage.addChild(this.potionUi);
			
			this.staminaCircle = new PIXI.Graphics();
			this.staminaCircle.x = 50;
			this.staminaCircle.y = 719;
			this.guistage.addChild(this.staminaCircle);
			var weaponHotkey = createText('V', {font: '6pt monospace', fill: 'white'}, 18, 742);
			this.weaponImage = new PIXI.Sprite();
			this.weaponChargesText = createText('', {font: '6pt monospace', fill: 'white'}, 69, 672);
			this.weaponChargesText.anchor = new PIXI.Point(0.5, 0.5);
			this.guistage.addChild(this.weaponImage);
			this.guistage.addChild(weaponHotkey);
			this.guistage.addChild(this.weaponChargesText);
			
			this.quickBarUi = new PIXI.Container();
			this.quickBarUi.x = 360;
			this.quickBarUi.y = 693;
			this.quickBarUi.addChild(createImage(0, 0, 14415));
			this.quickBarUi.addChild(createImage(0, 0, 14446));
			this.quickBarUi.addChild(createImage(0, 0, 14447));
			this.quickBarImages = [];
			this.quickBarOverlays = [];
			this.quickBarTimers = [];
			for (var i = 0; i < 5; i++)
			{
				var sprite = new PIXI.Sprite();
				sprite.x = 71 + i * 37;
				sprite.y = 34;
				sprite.texture = getimage(AbilityImages[QuickBarSlots[i]][0]);
				this.quickBarImages.push(sprite);
				this.quickBarUi.addChild(sprite);
				
				var g = new PIXI.Graphics();
				g.x = 71 + i * 37;
				g.y = 34;
				this.quickBarOverlays.push(g);
				this.quickBarUi.addChild(g);
				
				this.quickBarTimers.push([0, 0]);
			}
			this.quickBarUi.addChild(createImage(0, 0, 14424));
			this.quickBarUi.addChild(createText('A', {font: '6pt monospace', fill: 'white'}, 83, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14425));
			this.quickBarUi.addChild(createText('S', {font: '6pt monospace', fill: 'white'}, 120, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14426));
			this.quickBarUi.addChild(createText('D', {font: '6pt monospace', fill: 'white'}, 158, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14427));
			this.quickBarUi.addChild(createText('F', {font: '6pt monospace', fill: 'white'}, 195, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14428));
			this.quickBarUi.addChild(createText('G', {font: '6pt monospace', fill: 'white'}, 232, 62));
			this.guistage.addChild(this.quickBarUi);
			
			this.enchantIcons = [];
			this.enchantIconContainer = new PIXI.Container();
			this.enchantIconContainer.x = 5;
			this.enchantIconContainer.y = 5;
			this.guistage.addChild(this.enchantIconContainer);
			
			this.inventoryScreenPage = 0;
			this.inventoryScreen = new PIXI.Container();
			this.inventoryScreen.visible = false;
			this.inventoryScreenModel = new PIXI.Container();
			this.inventoryScreenModel.x = 10;
			this.inventoryScreenModel.y = 10;
			this.inventoryScreen.addChild(this.inventoryScreenModel);
			this.inventoryScreen.addChild(createImage(0, 0, 14755));
			this.inventoryScreen.addChild(createImage(254, 13, 14775));
			this.inventoryScreen.addChild(createImage(314, 13, 14757));
			this.inventoryScreen.addChild(createImage(314, 63, 14763));
			this.inventoryScreen.addChild(createImage(314, 113, 14767));
			this.inventoryScreenItems = new PIXI.Container();
			this.inventoryScreenItems.x = 314;
			this.inventoryScreenItems.y = 13;
			this.inventoryScreen.addChild(this.inventoryScreenItems);	
			this.inventoryScreenHandle = createImage(522, 25, 14601);
			this.inventoryScreenUp = createImage(522, 0, 14778);
			this.inventoryScreenDown = createImage(522, 150, 14780);
			this.inventoryScreen.addChild(this.inventoryScreenHandle);	
			this.inventoryScreen.addChild(this.inventoryScreenUp);
			this.inventoryScreen.addChild(this.inventoryScreenDown);	
			this.guistage.addChild(this.inventoryScreen);
			
			this.teamBorderImage = createImage(0, 0, 15078);
			this.teamBorderImage.visible = false;
			this.ballImage = createImage(920, 650, 14968);
			this.ballImage.visible = false;
			this.flagImages = {
				1: createImage(890, 685, 14940),
				2: createImage(890, 725, 14946)
			};
			this.flagImages[1].visible = false;
			this.flagImages[2].visible = false;
			this.guistage.addChild(this.teamBorderImage);
			this.guistage.addChild(this.ballImage);
			this.guistage.addChild(this.flagImages[1]);
			this.guistage.addChild(this.flagImages[2]);
			
			this.messages = [];
			this.messagesContainer = new PIXI.Container();
			this.guistage.addChild(this.messagesContainer);
			
			this.cursor = 'CursorSelect';
			this.cursorObject = null;
			this.cursorSprite = new PIXI.Sprite();
			this.cursorSprite.anchor = new PIXI.Point(0, 0);
			this.guistage.addChild(this.cursorSprite);
			
			this.toolTipContainer = new PIXI.Container();
			this.toolTipBox = new PIXI.Graphics();
			this.toolTipText = new PIXI.Text('', {font: '8px monospace', fill: 'yellow'});
			this.toolTipContainer.addChild(this.toolTipBox);
			this.toolTipContainer.addChild(this.toolTipText);
			this.guistage.addChild(this.toolTipContainer);
			
			this.updateInventory();
		}
		updateAbilities()
		{
			for (var i = 0; i < 5; i++)
			{
				var overlay = this.quickBarOverlays[i];
				var timer = this.quickBarTimers[i];
				
				if (timer[0] == 0)
				{
					overlay.visible = false;
					continue;
				}
				
				this.quickBarTimers[i][0] = timer[0] - 1;
				overlay.visible = true;
				overlay.clear();
				overlay.beginFill(0x000000, 0.5)
					.drawRect(0, 0, 30, Math.floor(timer[0] / timer[1] * 30))
					.endFill();
			}
		}
		updateHealthBar(percent: number, poison: boolean)
		{
			if (poison)
				this.poisonBar.visible = true;
			else
				this.poisonBar.visible = false;
			this.healthBar.clear();
			this.healthBar.beginFill(0x000000, 0.5);
			this.healthBar.drawRect(0, 0, 15, 126);
			this.healthBar.endFill();
			if (poison)
				this.healthBar.beginFill(0x00D000, 0.7);
			else
				this.healthBar.beginFill(0xD00000, 0.7);
			this.healthBar.drawRect(0, Math.floor(126 * (1 - percent)), 15, Math.floor(126 * percent));
			this.healthBar.endFill();
		}
		displayMessage(_msg: string)
		{
			var m = {msg: _msg, timestamp: this.currentFrame, o: new PIXI.Text(_msg, {font: '10px monospace', fill: 'white', stroke: 'black', strokeThickness: 1})};
			m.o.anchor = new PIXI.Point(0.5, 0.5);
			if (this.messages.length >= 3)
				this.messages.splice(0, 1);
			this.messages.push(m);
			
			theChat.addMessage(_msg);
		}
		updateMessages()
		{
			for (var i = 0; i < this.messages.length; i++)
			{
				var m = this.messages[i];
				var remaining = (m.timestamp + 90) - this.currentFrame;
				
				if (remaining <= 0)
				{
					this.messages.splice(0, 1);
					i--;
					continue;
				}
			}
			
			this.messagesContainer.removeChildren();
			for (var i = 0; i < this.messages.length; i++)
			{
				var m = this.messages[i];
				this.messagesContainer.addChild(m.o);
				m.o.x = 512;
				m.o.y = 600 - 15 * i;
			}
		}
		updatePotions()
		{
			this.potionUi.cacheAsBitmap = false;
			
			// count number of each type in inventory
			var counts = _.countBy(thePlayer.inventory, (extent) => this.get(extent).type);
			
			// cure poison potions
			if (counts[631])
			{
				//console.log('found ' + counts[631] + ' poison potions');
				this.potionPoisonImage.texture = getimage(theThings.objects[631].images[0]);
				this.potionPoisonImage.visible = true;
				this.potionPoisonCount.text = counts[631].toString();
			}
			else
			{
				this.potionPoisonImage.visible = false;
				this.potionPoisonCount.text = '';
			}
			
			// mana potions
			if (counts[638])
			{
				//console.log('found ' + counts[638] + ' mana potions');
			}
			
			// health foods
			for (var i = 0; i < HealthPotions.length; i++)
			{
				var type: number = theThings.fromName(HealthPotions[i]).id;
				if (counts[type])
				{
					//console.log('found ' + counts[type] + ' health potions');	
					this.potionHealthImage.texture = getimage(theThings.objects[type].images[0]);	
					this.potionHealthImage.visible = true;
					this.potionHealthCount.text = counts[type].toString();
					break;
				}
			}
			if (i == HealthPotions.length)
			{
				this.potionHealthImage.visible = false;
				this.potionHealthCount.text = '';
			}
			
			this.potionUi.cacheAsBitmap = true;
		}
		updateStamina()
		{
			var angle = 2 * ((100 - thePlayer.stamina) / 100) - 0.5;
			this.staminaCircle.clear();
			this.staminaCircle.beginFill(0xff00ff, 0.5)
				.moveTo(0, 0)
				.arc(0, 0, 28, -0.5 * Math.PI, angle * Math.PI)
				.endFill();
		}
		updateEnchants()
		{
			this.enchantIconContainer.removeChildren();
			this.enchantIcons = [];
			
			var xoffset = 0;
			for (var i = 0; i < 32; i++)
			{
				var data = EnchantData[i];
				if (!data || !thePlayer.object.hasenchant(i))
					continue;
				
				var sprite = new PIXI.Sprite(getimage(data.image));
				sprite.x = xoffset++ * 35;
				sprite.y = 0;
				this.enchantIconContainer.addChild(sprite);
				this.enchantIcons.push({'sprite': sprite, 'data': data});
			}
			for (var i = 0; i < 6; i++)
			{
				var data = ItemEnchantData[i];
				if (!data || !(thePlayer.itemenchant & (1 << i)))
					continue;
				
				var sprite = new PIXI.Sprite(getimage(data.image));
				sprite.x = xoffset++ * 35;
				sprite.y = 0;
				this.enchantIconContainer.addChild(sprite);
				this.enchantIcons.push({'sprite': sprite, 'data': data});
			}
		}
		updateBallStatus()
		{
			if (this.ballStatus)
			{
				this.ballImage.visible = true;
				switch (this.ballStatus.status)
				{
				case 0:
					this.ballImage.texture = getimage(14968); // BallAtHome
					break;
				case 1:
					this.ballImage.texture = getimage(14969); // BallAway
					break;
				case 2:
					this.ballImage.texture = getimage(14972); // BallBlue
					break;
				case 4:
					this.ballImage.texture = getimage(14973); // BallRed
					break;
				}
			}
			else
				this.ballImage.visible = false;
		}
		updateFlagStatus()
		{
			if (this.flagStatus)
			{
				// FIXME if we want to support more than two teams
				this.flagImages[1].visible = true;
				switch (this.flagStatus[1].status)
				{
				case 0:
					this.flagImages[1].texture = getimage(14940);
					break;
				case 1:
					this.flagImages[1].texture = getimage(14941);
					break;
				case 2:
					this.flagImages[1].texture = getimage(14945);
					break;
				}
				this.flagImages[2].visible = true;
				switch (this.flagStatus[2].status)
				{
				case 0:
					this.flagImages[2].texture = getimage(14946);
					break;
				case 1:
					this.flagImages[2].texture = getimage(14949);
					break;
				case 2:
					this.flagImages[2].texture = getimage(14950);
					break;
				}
				if (thePlayer)
				{
					if (this.flagImages[thePlayer.team])
					{
						this.teamBorderImage.visible = true;
						this.teamBorderImage.x = this.flagImages[thePlayer.team].x - 4;
						this.teamBorderImage.y = this.flagImages[thePlayer.team].y - 4;
					}
					else
						this.teamBorderImage.visible = false;
				}
			}
			else
			{
				_.each(<any>this.flagImages, (v: PIXI.Sprite, k: number) => {
					v.visible = false;
				});
			}
		}
		hideTooltip()
		{
			this.toolTipBox.clear();
			this.toolTipText.text = '';
		}
		showTooltip(msg: string)
		{
			this.toolTipText.text = msg;
			this.toolTipBox.clear();
			this.toolTipBox.beginFill(0, 0.5)
				.drawRect(-2, -2, this.toolTipText.width + 4, this.toolTipText.height + 4)
				.endFill();
		}
		updateInventory()
		{
			if (!thePlayer)
				return;
				
			this.updatePotions();
			
			if (!this.inventoryScreen.visible)
				return;
				
			var createImage = (x: number, y: number, image: number) => {
				var sprite = new PIXI.Sprite(getimage(image));
				sprite.x = x;
				sprite.y = y;
				return sprite;
			};
			
			this.inventoryScreenItems.removeChildren();
			this.inventoryScreenModel.removeChildren();
			
			var modelBg = new PIXI.Graphics();
			modelBg.beginFill(0);
			modelBg.drawRect(0, 0, 210, 215);
			modelBg.endFill();
			this.inventoryScreenModel.addChild(modelBg);
			
			this.inventoryScreenHandle.y = 25 + this.inventoryScreenPage * 5;
			
			if (thePlayer)
			{
				var canvas = document.createElement('canvas');
				canvas.height = 256;
				var ctx = <CanvasRenderingContext2D> canvas.getContext('2d');
				var imageData = ctx.createImageData(256, 256);
				var colors = thePlayer.colors.slice();
				drawimage(imageData, 14342, colors);
				
				for (var i = 0; i < 32; i++)
				{
					if (thePlayer.object.armor & (1 << i))
					{
						var mod = thePlayer.object.armorModifiers[1 << i];
						if (mod)
							getarmorcolors(colors, 1 << i, mod.material, mod.effectiveness, mod.primary, mod.secondary);
						else
							getarmorcolors(colors, 1 << i);
						drawimage(imageData, ModelArmor[1 << i], colors);
					}
				}
				
				for (var i = 0; i < 32; i++)
				{
					if ((thePlayer.object.weapon & (1 << i)) && ModelWeapon[1 << i])
					{
						var mod = thePlayer.object.weaponModifiers[1 << i];
						if (mod)
							getweaponcolors(colors, 1 << i, mod.material, mod.effectiveness, mod.primary, mod.secondary);
						else
							getweaponcolors(colors, 1 << i);
						drawimage(imageData, ModelWeapon[1 << i], colors);
					}
				}
				
				ctx.putImageData(imageData, 0, 5);
				var img = new PIXI.Sprite(PIXI.Texture.fromCanvas(canvas));
				this.inventoryScreenModel.addChild(img);
				
				var offset = this.inventoryScreenPage * 12;
				for (var row = 0; row < 3; row++)
				{
					for (var col = 0; col < 4; col++)
					{
						if (offset + row * 4 + col >= thePlayer.inventory.length)
							break;
						
						var extent = thePlayer.inventory[offset + row*4 + col];
						var o = new PIXI.Sprite(this.objects[extent]._sprite.texture);
						o.x = col * 50 - 40;
						o.y = row * 50 - 40;
						this.inventoryScreenItems.addChild(o);
						if (thePlayer.equipment.indexOf(extent) >= 0)
						{
							var o = createImage(col * 50, row * 50, 14672);
							this.inventoryScreenItems.addChild(o);
							o.alpha = 0.5;
						}
						else if (theClient.prevWeapon == extent)
						{
							var o = createImage(col * 50, row * 50, 14691);
							this.inventoryScreenItems.addChild(o);
							o.alpha = 0.5;							
						}
					}
				}
			}
		}
		updateWeapon()
		{
			var extent = thePlayer.equipment.filter((extent) => (theWorld.get(extent).getThing().cls & ThingClass.WEAPON) != 0)[0];
			if (!extent)
			{
				this.weaponImage.texture = getimage(14399);
				this.weaponImage.x = 23;
				this.weaponImage.y = this.renderer.height - 78;
				this.weaponChargesText.text = '';
				return;
			}
			var o = <WeaponArmorObject> theWorld.get(extent);
			this.weaponImage.texture = o._sprite.texture;
			this.weaponImage.x = -13;
			this.weaponImage.y = this.renderer.height - 112;
			if (o.charges)
				this.weaponChargesText.text = o.charges.current.toString();
			else
				this.weaponChargesText.text = '';
		}
		render(renderer: PIXI.CanvasRenderer)
		{
			this.renderer = renderer;
			this.allstage = new PIXI.Container();
			this.stage = new Container();
			this.effectstage = new PIXI.Container();
			this.effectGraphic = new PIXI.Graphics();
			this.belowstage = new PIXI.CanvasBuffer(5880, 5880);
			this.guistage = new PIXI.Container();
			this.infoText = new PIXI.Text('', {font: '12px serif', fill: 'white'});
			this.infoText.y = 100;
			this.overlay = new PIXI.Graphics();
			
			this.pickupCircle = new PIXI.Graphics();
			this.pickupCircle.beginFill(0x880000);
			this.pickupCircle.drawCircle(0, 0, 10);
			this.pickupCircle.endFill();
			this.pickupCircle.visible = false;
			
			this.allstage.interactive = false;
			this.allstage.interactiveChildren = false;
			this.allstage.addChild(new PIXI.Sprite(PIXI.Texture.fromCanvas(this.belowstage.canvas)));
			this.allstage.addChild(this.overlay);
			this.allstage.addChild(this.pickupCircle);
			this.allstage.addChild(this.stage);
			this.allstage.addChild(this.effectstage);
			this.effectstage.addChild(this.effectGraphic);
			this.guistage.addChild(this.allstage);
			this.guistage.addChild(this.infoText);
			this.im = new PIXI.interaction.InteractionManager(this.renderer, {});
			
			this.createUi();
			
			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			window.onkeydown = ev => this.onkeydown(ev);
			window.onmousedown = (ev) => {
				var evt: MouseEvent = <MouseEvent>this.im.mouse.originalEvent;
				if (evt != null && (ev.buttons & 1))
				{
					var loc: PIXI.Point;
					var global = this.im.mouse.global;
					loc = this.im.mouse.getLocalPosition(this.guistage);
					if (!(new PIXI.Rectangle(0, 0, this.renderer.width, this.renderer.height)).contains(global.x, global.y))
					{
						// ignore mouse clicks outside of the game
						return;
					}
					this.idleTimer = Date.now();
					if (theChat.hasFocus())
					{
						theChat.loseFocus();
						return;
					}
					if (this.targetingAbility !== null)
					{
						theClient.sendability(this.targetingAbility);
						this.targetingAbility = null;
						return;
					}
					// inventory screen
					if (this.inventoryScreen.visible)
					{
						if (loc.x >= 314 && loc.x < 514 && loc.y >= 13 && loc.y < 163)
						{
							var offset = this.inventoryScreenPage * 12;
							var col = Math.floor((loc.x - 314) / 50);
							var row = Math.floor((loc.y - 13) / 50);
							
							if (thePlayer && offset + row * 4 + col < thePlayer.inventory.length)
							{
								var extent = thePlayer.inventory[offset + row * 4 + col];
								if (this.objects[extent].getThing().cls & ThingClass.FOOD)
									theClient.senduse(extent);
								else if (thePlayer.equipment.indexOf(extent) >= 0)
									theClient.senddequip(extent);
								else
									theClient.sendequip(extent);
							}
							return;
						}
						if (this.inventoryScreenDown.getBounds().contains(global.x, global.y))
						{
							this.inventoryScreenPage += 1;
							if (this.inventoryScreenPage > 16)
								this.inventoryScreenPage = 16;
							this.updateInventory();
							return;
						}
						if (this.inventoryScreenUp.getBounds().contains(global.x, global.y))
						{
							this.inventoryScreenPage -= 1;
							if (this.inventoryScreenPage < 0)
								this.inventoryScreenPage = 0;
							this.updateInventory();
							return;
						}
					}
					
					// show/hide inventory button
					if (loc.x >= 5 && loc.x < 30 && loc.y >= 650 && loc.y < 680)
					{
						this.inventoryScreen.visible = !this.inventoryScreen.visible;
						if (this.inventoryScreen.visible)
							playaudiobyname('InventoryOpen', 0, 1);
						else
							playaudiobyname('InventoryClose', 0, 1);
						this.updateInventory();
						return;
					}
					// swap weapon area
					if (loc.x >= 25 && loc.x <= 80 && loc.y >= 700 && loc.y <= 750)
					{
						theClient.swapweapon();
						return;
					}
					for (var i = 0; i < this.quickBarImages.length; i++)
					{
						if (this.quickBarImages[i].getBounds().contains(global.x, global.y))
						{
							this.targetingAbility = null;
							switch (QuickBarSlots[i])
							{
								case PlayerAbility.ABILITY_BERSERKER_CHARGE:
								case PlayerAbility.ABILITY_HARPOON:
									this.targetingAbility = QuickBarSlots[i];
									break;
								default:
									theClient.sendability(QuickBarSlots[i]);
									break;
							}
							return;
						}
					}
					
					var thing = this.cursorObject ? this.cursorObject.getThing() : null;
					var cls = thing ? thing.cls : 0;
					if (cls & ThingClass.PICKUP)
					{
						theClient.sendpickup(this.cursorObject.extent);
						return;
					}
					else if (cls & ThingClass.TRIGGER)
					{
						theClient.sendcollide(this.cursorObject.extent);
						return;
					}
					else if (cls & ThingClass.MONSTER)
					{
						if (thing.subclass.indexOf('SHOPKEEPER') >= 0)
						{
							// TODO trade
							return;
						}
						// TODO friendlies
						// CursorTalk
					}

					theClient.attack();
				}
			};
			window.setTimeout(() => this.tick(), 33);
		}
		onkeydown(ev: KeyboardEvent)
		{
			this.idleTimer = Date.now();
			if (theChat.hasFocus())
			{
				switch (ev.keyCode)
				{
				case 0x1B: // ESC
					theChat.clearInput();
					theChat.loseFocus();
					break;
				case 0x0A: // ENTER
				case 0x0D:
					if (theChat.getInput() != '')
						theClient.sendtext(theChat.getInput());
					theChat.clearInput();
					theChat.loseFocus();
					break;
				default:
					break;
				}
				return;
			}
			switch (ev.keyCode)
			{
				case 0x0A: // ENTER
				case 0x0D:
					theChat.takeFocus();
					break;
				case 0x1B: // ESC
					this.targetingAbility = null;
					this.inventoryScreen.visible = false;
					this.updateInventory();
					break;
				case 0x41: // A
					theClient.sendability(QuickBarSlots[0]);
					break;
				case 0x53: // S
					theClient.sendability(QuickBarSlots[1]);
					break;
				case 0x44: // D
					theClient.sendability(QuickBarSlots[2]);
					break;
				case 0x46: // F
					theClient.sendability(QuickBarSlots[3]);
					break;
				case 0x47: // G
					theClient.sendability(QuickBarSlots[4]);
					break;
				case 0x51: // Q
				case 0x49: // I
					this.inventoryScreen.visible = !this.inventoryScreen.visible;
					if (this.inventoryScreen.visible)
						playaudiobyname('InventoryOpen', 0, 1);
					else
						playaudiobyname('InventoryClose', 0, 1);
					this.updateInventory();
					break;
				case 0x56: // V
					theClient.swapweapon();
					break;
				case 0x58: // X
					thePlayer.useHealth();
					break;
				case 0x5A: // Z
					thePlayer.usePoison();
					break;
				case 0x20: // Space
					theClient.jump();
					break;
			}
		}
		updateCursor(mouse: PIXI.Point)
		{
			switch (this.cursor)
			{
				case 'CursorMove':
					var cursorangle = Math.atan2(this.renderer.height / 2 - mouse.y, this.renderer.width / 2 - mouse.x);
					var cursordist = Math.pow(mouse.y - this.renderer.height / 2, 2) + Math.pow(mouse.x - this.renderer.width / 2, 2);
					var frame = Math.min(Math.floor(((cursorangle + Math.PI) / (Math.PI * 2)) *32), 31);
					if (cursordist >= 10000) // running
						frame += 32;
					this.cursorSprite.texture = getimage(theThings.images['CursorMove'].anim.images[frame]);
					break;
				default:
					var cursor = theThings.images[this.cursor].anim;				
					var frame = Math.floor(this.currentFrame / (cursor.delay + 1)) % cursor.images.length;
					this.cursorSprite.texture = getimage(cursor.images[frame]);
					break;
			}
		}
		mouseover()
		{
			// reset state
			this.hideTooltip();
			this.cursor = 'CursorMove';
			this.cursorObject = null;
			this.pickupCircle.visible = false;
				
			// handle mouse over in UI
			var global = this.im.mouse.global;
			var loc = this.im.mouse.getLocalPosition(this.guistage);
			this.cursorSprite.x = loc.x - 64;
			this.cursorSprite.y = loc.y - 64;
			
			// inventory screen
			if (this.inventoryScreen.visible && this.inventoryScreen.getBounds().contains(global.x, global.y))
			{
				this.cursor = 'CursorSelect';
				if (loc.x >= 314 && loc.x < 514 && loc.y >= 13 && loc.y < 163)
				{
					var offset = this.inventoryScreenPage * 12;
					var col = Math.floor((loc.x - 314) / 50);
					var row = Math.floor((loc.y - 13) / 50);
					
					if (thePlayer && offset + row * 4 + col < thePlayer.inventory.length)
					{
						var extent = thePlayer.inventory[offset + row * 4 + col];
						this.showTooltip(this.get(extent).tooltip());
					}
					return;
				}
				return;
			}
			
			if (loc.x >= 5 && loc.x < 30 && loc.y >= 650 && loc.y < 680)
			{
				// show/hide inventory button
				this.cursor = 'CursorSelect';
				return;
			}
			
			if (loc.x >= 25 && loc.x <= 80 && loc.y >= 700 && loc.y <= 750)
			{
				// weapon area
				this.cursor = 'CursorSelect';
				if (!thePlayer) return;
				var extent = thePlayer.equipment.filter((extent) => (theWorld.get(extent).getThing().cls & ThingClass.WEAPON) != 0)[0];
				if (!extent) return;
				this.showTooltip(theWorld.get(extent).tooltip());
				return;
			}
			
			// abilities in quickbar
			for (var i = 0; i < this.quickBarImages.length; i++)
			{
				if (this.quickBarImages[i].getBounds().contains(global.x, global.y))
				{
					var str;
					if (QuickBarSlots[i] == PlayerAbility.ABILITY_BERSERKER_CHARGE)
						str = 'thing.db:BerserkerCharge'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_EYE_OF_THE_WOLF)
						str = 'thing.db:EyeOfTheWolf'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_HARPOON)
						str = 'thing.db:Harpoon'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_TREAD_LIGHTLY)
						str = 'thing.db:TreadLightly'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_WARCRY)
						str = 'thing.db:Warcry'
					this.cursor = 'CursorSelect';
					this.showTooltip(getstring(str));
					return;
				}
			}
			
			// enchant icons
			for (var i = 0; i < this.enchantIcons.length; i++)
			{
				var icon = this.enchantIcons[i];
				if (icon.sprite.getBounds().contains(global.x, global.y))
				{
					this.showTooltip(getstring(icon.data.desc));
					this.cursor = 'CursorSelect';
					return;
				}
			}
			
			// handle mouse over in game
			var loc = this.im.mouse.getLocalPosition(this.allstage);
			var objs = this.stage.findObjects(loc, 20).filter((x) => (x.getThing().cls & (ThingClass.PICKUP | ThingClass.TRIGGER | ThingClass.MONSTER)) != 0 && x.visible);
			if (objs.length > 0)
			{
				var closest: Object = null;
				var closest2 = Infinity;
				for (var i = 0; i < objs.length; i++)
				{
					var o = objs[i];
					var dist = Math.pow(loc.y - o.y, 2) + Math.pow(loc.x - o.x, 2);
					if (dist < closest2)
					{
						closest = o;
						closest2 = dist;
					}
				}
				
				var thing = closest.getThing();
				var dist = Math.pow(thePlayer.object.x - closest.x, 2) + Math.pow(thePlayer.object.y - closest.y, 2);
				var cls = thing.cls;
				if (cls & ThingClass.PICKUP)
				{
					this.pickupCircle.x = o.x;
					this.pickupCircle.y = o.y;
					this.pickupCircle.visible = true;
					
					// TODO check if class is correct
					
					if (dist < 5625)
					{
						this.cursorObject = closest;
						this.showTooltip(o.tooltip());
						this.cursor = 'CursorPickup';
					}
					else
					{
						this.cursor = 'CursorPickupFar';
					}
				}
				else if (cls & ThingClass.TRIGGER)
				{
					if (dist < 5625)
					{
						this.cursorObject = closest;
						this.cursor = 'CursorUse';
					}
				}
				else if (cls & ThingClass.MONSTER)
				{
					if (dist < 15625)
					{
						if (thing.subclass.indexOf('SHOPKEEPER') >= 0)
						{
							this.cursorObject = closest;
							this.cursor = 'CursorTrade';
						}
						// TODO friendlies
						// CursorTalk
					}
				}
			}
		}
		tick(/*timestamp: number*/)
		{			
			this.renderer.view.style.cursor = 'none';
			
			if (Date.now() - this.idleTimer >= 15*60*1000) // 15 minute idle timer
			{
				document.location.href = 'idle.html';
				return;
			}
			window.setTimeout(() => this.tick(), 33);
			
			this.currentFrame++;
			
			// once a second send out a ping
			if ((this.currentFrame % 33) == 0)
				theClient.sendping();
			
			// update server with mouse location and input
			if (thePlayer && thePlayer.object.loaded)
			{
				var evt: MouseEvent = <MouseEvent>this.im.mouse.originalEvent;
				if (evt == null)
				{
					theClient.sendupdate({x: 0, y: 0});
				}
				else
				{
					var loc = this.im.mouse.getLocalPosition(this.allstage);
					if (evt.buttons & 2)
						theClient.move();
					theClient.sendupdate(loc);
				}
			}
			
			// center the stage on the player
			if (thePlayer)
			{
				this.allstage.x = Math.floor(-thePlayer.object.x + this.renderer.width / 2);
				this.allstage.y = Math.floor(-thePlayer.object.y + this.renderer.height / 2); 
				
				if (this.jiggleEffect)
				{
					this.jiggleEffect.frame++;
					
					this.allstage.y += Math.floor(40 * this.jiggleEffect.intensity * Math.sin(this.jiggleEffect.frame / 5 * Math.PI * 2));
					
					if (this.jiggleEffect.frame >= 5)
						this.jiggleEffect = null;
				}
				
				if (thePlayer.object.x !== undefined)
				{
					this.infoText.text = 'ping: ' + theClient.getrtt().toString() + 'ms\n' + 'x: ' + thePlayer.object.x.toString() + ', y: ' + thePlayer.object.y.toString();
				}
				
				// shadows
				var polygons = Visibility.generate(thePlayer.object, this.wallsArray, this.objectsArray);
				this.overlay.clear();
				for (var i = 0; i < this.effects.length; i++)
				{
					this.effects[i].draw(true, this.overlay);
				}
				this.overlay.lineWidth = 0;
				this.overlay.beginFill(0);
				for (var i = 0; i < polygons.length; i ++)
				{
					var polygon = polygons[i];
					var points = [];
					for (var j = 0; j < polygon.length; j++)
					{
						points.push(polygon[j].x);
						points.push(polygon[j].y);
					}
					this.overlay.drawPolygon(points);
				}
				this.overlay.endFill();
			}
			
			this.effectGraphic.clear();
			for (var i = 0; i < this.effects.length; i++)
			{
				if (!this.effects[i].draw(false, this.effectGraphic))
				{
					this.effects.splice(i, 1);
					i--;
				}
			}
			
			this.mouseover();
			if (this.targetingAbility !== null)
				this.cursor = 'CursorTarget';
				
			this.updateAbilities();
			this.updateMessages();
			if (thePlayer)
				thePlayer.updateStamina();
				
			var loc = this.im.mouse.getLocalPosition(this.guistage);
			this.toolTipContainer.x = loc.x;
			this.toolTipContainer.y = loc.y + 20;
			
			// update cursor sprite
			this.updateCursor(loc);
			
			// render at around 30 fps
			if (this.flashEffect)
			{
				this.renderer.context.setTransform(1, 0, 0, 1, 0, 0);
				this.renderer.context.fillStyle = 'white';
				this.renderer.context.fillRect(0, 0, this.renderer.width, this.renderer.height);
				this.flashEffect = false;
			}
			else
			{
				this.renderer.render(this.guistage);
			}
		}
	}
	
	export interface IOptions
	{
		gameServer: string;
		gameServerPort: number;
		proxyServer: string;
		playerName: string;
		playerClass: PlayerClass;
		gauntletMode: boolean;
	}
}